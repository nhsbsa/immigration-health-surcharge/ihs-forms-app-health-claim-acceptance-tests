# ihs-forms-app-health-claim-acceptance-tests

This project contains the acceptance tests for Health & Social Care cohort claim web form application.

The acceptance tests are built in Cucumber User Centric BDD framework using Gherkin language.

## Quick start

### Prerequisite

1. Make sure Java and Maven are installed
2. Clone the repo 

### Building the project

Install the maven dependencies

`mvn clean install`

### Executing the tests

Set the environment variable

On linux:
`export IHS_HEALTH_CLAIM=https://{url}`

On Windows:
`set IHS_HEALTH_CLAIM=https://{url}`

To run the full regression suite in test environment:    

`mvn clean verify -Dbrowser=headless -Denv=tst -Dit.test=RegressionTestRunner`

To run the smoke test suite in test environment:        

`mvn clean verify -Dbrowser=headless -Denv=tst -Dit.test=SmokeTestRunner`              

To run specific tests in test environment: 

Include the feature or scenario `@tag` in `GenericTestRunner` and run the below maven command   
                                                                                                           
`mvn clean verify -Dbrowser=headless -Denv=tst -Dit.test=GenericTestRunner`

### Viewing the reports

Cucumber reports can be found in `target` folder of the project directory

Extent reports (Spark, PDF and Json) can be found in `test-reports` folder of the project directory

## Contributing

We operate a [code of conduct](CODE_OF_CONDUCT.md) for all contributors.

See our [contributing guide](CONTRIBUTING.md) for guidance on how to contribute.

## License

Released under the [Apache 2 license](LICENSE).
