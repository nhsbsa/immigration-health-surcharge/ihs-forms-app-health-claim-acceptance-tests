package com.nhsbsa.ihs.shared;

public class SharedData {

    private SharedData() {
        throw new IllegalStateException("SharedData Utility class");
    }

    public static String environment;
    public static String browser;
}
