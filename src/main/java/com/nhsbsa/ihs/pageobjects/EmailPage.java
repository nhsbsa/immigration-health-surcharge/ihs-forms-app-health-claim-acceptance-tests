package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EmailPage extends Page {

    private By emailLocator = By.id("email");
    private By continueButtonLocator = By.id("continue-button");
    private By emailErrorMessageLocator = By.partialLinkText("Enter an");

    public EmailPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterEmailAndSubmit(String email) {
        sendTextValues(emailLocator, email);
        continueButton();
    }

    public String getEmailErrorMessage() {
        return getElementText(emailErrorMessageLocator);
    }

    public String getEnteredEmail() {
        return getElementValue(emailLocator);
    }

    public void enterEmail(String email) {
        sendTextValues(emailLocator, email);
    }
}
