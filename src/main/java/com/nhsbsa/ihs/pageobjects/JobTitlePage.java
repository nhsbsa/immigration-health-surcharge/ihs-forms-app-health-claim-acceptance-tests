package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Random;

public class JobTitlePage extends Page {

    private By jobTitleLocator = By.id("job-title");
    private By continueButtonLocator = By.id("continue-button");
    private By jobTitleErrorMessageLocator = By.partialLinkText("Enter a job title");
    private By emptyJobTitleErrorMessageLocator = By.partialLinkText("Enter the applicant's");
    private static final Random random = new Random();

    public JobTitlePage(WebDriver driver) {
        super(driver);
    }

    public void enterJobTitle(String jobTitle) {
        sendTextValues(jobTitleLocator, jobTitle);
        clickEvent(continueButtonLocator);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public String generateOverLimitJobTitle(int count) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder(71);
        for (int i = 0; i < count; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public void enterJobTitleOverLimitText(int count) {
        String jobTitleInput = generateOverLimitJobTitle(count);
        sendTextValues(jobTitleLocator, jobTitleInput);
    }

    public String getJobTitleErrorMessage() {
        return getElementText(jobTitleErrorMessageLocator);
    }

    public String getEmptyJobTitleErrorMessage() {
        return getElementText(emptyJobTitleErrorMessageLocator);
    }

    public int getEnteredJobTitleLength() {
        return getLengthOfEnteredText(jobTitleLocator);
    }

    public String getEnteredJobTitle() {
        return getElementValue(jobTitleLocator);
    }


    public void selectJobTitleFromList(String job, String jobTitle) {
        WebElement jTitle = driver.findElement(jobTitleLocator);
        jTitle.sendKeys(job);
        new WebDriverWait(driver, Duration.ofSeconds(5))
                .ignoring(StaleElementReferenceException.class)
                .until((WebDriver d) -> {
                    d.findElement(By.xpath("//div[@class='autocomplete ']/div[contains(text(), '"+jobTitle+"')]")).click();
                    return true;
                });
        clickEvent(continueButtonLocator);
    }
}
