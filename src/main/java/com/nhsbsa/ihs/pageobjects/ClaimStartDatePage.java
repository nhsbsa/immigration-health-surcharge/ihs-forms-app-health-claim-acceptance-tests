package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ClaimStartDatePage extends Page{

    private By dayLocator = By.id("claim-start-date-day");
    private By monthLocator = By.id("claim-start-date-month");
    private By yearLocator = By.id("claim-start-date-year");
    private By continueButtonLocator = By.id("continue-button");
    private By claimStartDateErrorMessageLocator = By.partialLinkText("Enter a");
    private By claimStartDateSixMonthsErrorMessageLocator = By.partialLinkText("The date entered is invalid");
    private By previousClaimDate =By.id("previousClaim-date-hint");
    private By earliestStartDate = By.id("earliest-start-date-hint");
    private By claimEarliestStartDate = By.id("claim-earliestStartDate-start-date-hint");
    Logger logger = Logger.getLogger(ClaimStartDatePage.class.getName());

    public ClaimStartDatePage(WebDriver driver)
    {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterClaimStartDateAndSubmit(String day, String month, String year) {
        sendTextValues(dayLocator, day);
        sendTextValues(monthLocator, month);
        sendTextValues(yearLocator, year);
        continueButton();
    }

    public String getClaimStartDateErrorMessage() {
        return getElementText(claimStartDateErrorMessageLocator);
    }

    public String getClaimStartDateSixMonthsErrorMessage() {
        return getElementText(claimStartDateSixMonthsErrorMessageLocator);
    }

    public String getPreviousClaimDate() {
        String claimDate = getElementText(previousClaimDate);
        logger.log(Level.INFO,"Previous claim date is: {0}", claimDate);
        return claimDate;
    }

    public String getEarliestDate() {
        String earliestDate = getElementText(earliestStartDate);
        logger.log(Level.INFO,"Earliest date applicant can start claim is: {0}", earliestDate);
        return earliestDate;
    }

    public String getEarliestStartDate(){
        String claimEarliestDate = getElementText(claimEarliestStartDate);
        logger.log(Level.INFO,"Earliest date applicant can start new claim is: {0}", claimEarliestDate);
        return claimEarliestDate;
    }

    public void enterClaimStartDate(String day, String month, String year) {
        sendTextValues(dayLocator, day);
        sendTextValues(monthLocator, month);
        sendTextValues(yearLocator, year);
    }

    public String getEnteredDate() {
        return getElementValue(dayLocator) + getElementValue(monthLocator) + getElementValue(yearLocator);
    }
}
