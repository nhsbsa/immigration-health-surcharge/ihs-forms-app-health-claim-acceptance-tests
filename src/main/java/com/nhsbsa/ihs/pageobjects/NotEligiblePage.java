package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NotEligiblePage extends Page {

    private By contactUsLocator = By.linkText("contact us");
    private By previousClaimDates = By.xpath("//p[@class='govuk-hint']");
    private By earliestSubmitDate = By.xpath("//div[@class='govuk-inset-text']");

    public NotEligiblePage(WebDriver driver) {
        super(driver);
    }

    public void navigateToContactUs () {
        clickEvent(contactUsLocator);
    }

    public String getPreviousClaimDates() {
        return getElementText(previousClaimDates);
    }

    public String getEarliestDate() {
        return getElementText(earliestSubmitDate);
    }
}
