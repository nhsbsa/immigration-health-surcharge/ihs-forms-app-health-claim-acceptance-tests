package com.nhsbsa.ihs.pageobjects;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UploadFilePage extends Page {

    private By chooseFileButtonLocator = By.id("upload-evidence");
    private By chooseFileButtonForMultiFileLocator = By.id("documents");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.xpath("//div[@class=\"govuk-error-summary__body\"]/ul/li");
    private By claimPeriodLocator = By.xpath("//*[@id=\"gov-grid-row-content\"]/div/form/div[3]/div/div/p[2]");
    private By filesAddedHeaderLocator = By.xpath("//h2[text()='Files added ']");
    private By successFileUploadLocator = By.xpath("//span[@class=\"moj-multi-file-upload__success\"]");
    private By errorFileUploadLocator = By.xpath("//span[@class=\"moj-multi-file-upload__error\"]");
    private By errorSummaryForMultiFileLocator = By.xpath("//div[@class=\"govuk-error-summary__body\"]/ul//a");
    private By deleteFileSuccessLocator = By.xpath("//div[@id=\"multi-file-notification-banner-info\"]//p");
    private By deleteAllUnsuccessfulFilesLinkLocator = By.xpath("//div[@id='bref-error-summery']//a");
    private By deleteUnsuccessfulFilesLinkLocator = By.xpath("//span[@class='moj-multi-file-upload__error']/ancestor::div[contains(@class,'moj-multi-file-upload__row')]//a");
    private By clearErrorMessageLocator = By.xpath("//*[@class=\"govuk-error-message\"]/a");
    private By inProgressFileUploadLocator = By.xpath("//span[@class=\"moj-multi-file-upload__progress\"]");

    private static final String REMOTE_PATH_FILE1 = "/data/local/tmp/Sample_1.jpg";
    private static final String REMOTE_PATH_FILE2 = "/data/local/tmp/sample_2.png";
    private static final String FILE_UPLOAD_FILEPATH = System.getProperty("user.dir") + "/sample_uploads/";
    private static final String MULTI_FILE_UPLOAD_FILEPATH = System.getProperty("user.dir") + "/sample_files/";

    WebDriverWait webDriverWait = new WebDriverWait(driver,Duration.ofSeconds(100));

    public UploadFilePage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void chooseFileAndContinue(String fileName) {
        sendTextValues(chooseFileButtonLocator, fileName);
        continueButton();
    }

    public String getErrorMessage() {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(errorMessageLocator));
        return getElementText(errorMessageLocator);
    }

    public String getClaimPeriods() {
        return getElementText(claimPeriodLocator);
    }

    public void uploadMultiFileSauceLabs() throws IOException {
        String file1 = FILE_UPLOAD_FILEPATH+"Sample_1.jpg";
        String file2 = FILE_UPLOAD_FILEPATH+"sample_2.png";
        String sPlatform = System.getProperty("platform_name");
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(chooseFileButtonForMultiFileLocator));
        WebElement inputElement = driver.findElement(chooseFileButtonForMultiFileLocator);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.left='0'", inputElement);
        if(sPlatform.contains("Win") || sPlatform.contains("mac")) {
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
            sendTextValues(chooseFileButtonForMultiFileLocator, file1+"\n"+file2);
        }
        else {
            if (sPlatform.contains("Android")) {
                ((AndroidDriver) driver).pushFile(REMOTE_PATH_FILE1, new File(file1));
                ((AndroidDriver) driver).pushFile(REMOTE_PATH_FILE2, new File(file2));
            } else {
                ((IOSDriver) driver).pushFile(REMOTE_PATH_FILE1, new File(file1));
                ((IOSDriver) driver).pushFile(REMOTE_PATH_FILE2, new File(file2));
            }
            sendTextValues(chooseFileButtonForMultiFileLocator, REMOTE_PATH_FILE1);
            sendTextValues(chooseFileButtonForMultiFileLocator, REMOTE_PATH_FILE2);
        }
    }

    public void uploadMultipleFiles(String files) {
        StringBuilder multiPath= new StringBuilder("");
        List<String> file  = new ArrayList<>(List.of(files.split(",")));
        file.replaceAll(s -> FILE_UPLOAD_FILEPATH+s);
        WebElement inputElement = driver.findElement(chooseFileButtonForMultiFileLocator);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.left='0'", inputElement);
        for(String f : file)
            multiPath.append(f).append("\n");
        multiPath = new StringBuilder(multiPath.substring(0, multiPath.length() - 1));
        sendTextValues(chooseFileButtonForMultiFileLocator, String.valueOf(multiPath));
    }

    public void uploadNewMultipleFiles(String files) {
        StringBuilder multiPath= new StringBuilder("");
        List<String> file  = new ArrayList<>(List.of(files.split(",")));
        file.replaceAll(s -> MULTI_FILE_UPLOAD_FILEPATH+s);
        WebElement inputElement = driver.findElement(chooseFileButtonForMultiFileLocator);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.left='0'", inputElement);
        for(String f : file)
            multiPath.append(f).append("\n");
        multiPath = new StringBuilder(multiPath.substring(0, multiPath.length() - 1));
        sendTextValues(chooseFileButtonForMultiFileLocator, String.valueOf(multiPath));
    }

    public String getErrorMessageMultiFile() {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(errorSummaryForMultiFileLocator));
        return getElementText(errorSummaryForMultiFileLocator);
    }

    public List<String> getErrorMessageForMaxMultiFileUpload() {
        List<String> errorMessages = new ArrayList<>();
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(errorSummaryForMultiFileLocator));
        List<WebElement> errorMessageLocators = driver.findElements(errorSummaryForMultiFileLocator);
        for(WebElement errorLocator : errorMessageLocators)
            errorMessages.add(errorLocator.getText().trim());
        return errorMessages;
    }

    public boolean isFilesAddedHeadingDisplayed() {
        return webDriverWait.until(ExpectedConditions.visibilityOf(driver.findElement(filesAddedHeaderLocator))).isDisplayed();
    }

    public boolean isMultiFileUploadSuccessMessagesDisplayedFromCheckAnswer() {
        String files = "Sample_1.jpg,sample_2.png,sample_3.pdf,Sample_4.bmp";
        return isMultiFileUploadSuccessMessagesDisplayed(files);
    }

    public boolean isMultiFileUploadSuccessMessagesDisplayed(String files) {
        List<String> expUploadedFiles = new ArrayList<>(Arrays.asList(files.split(",")));
        List<String> actualUploadedFiles = new ArrayList<>();
        expUploadedFiles.replaceAll(s -> s+" successfully uploaded");
        webDriverWait.until(ExpectedConditions.numberOfElementsToBe(successFileUploadLocator,expUploadedFiles.size()));
        List<WebElement> uploadedFilesLocator = driver.findElements(successFileUploadLocator);
        for(WebElement uploadedFileLocator : uploadedFilesLocator)
            actualUploadedFiles.add(uploadedFileLocator.getText().trim());
        return expUploadedFiles.containsAll(actualUploadedFiles);
    }

    public boolean isMultiFileUploadSuccessMessageCountCorrect(int expCount) {
        return webDriverWait.until(ExpectedConditions.numberOfElementsToBe(successFileUploadLocator,expCount)).size()==expCount;
    }

    public boolean isMultiFileUploadFailureMessagesDisplayed(String files, String error) {
        List<String> expErredFiles = new ArrayList<>(Arrays.asList(files.split(",")));
        List<String> actualErredFiles = new ArrayList<>();
        expErredFiles.replaceAll(s -> s + error);
        webDriverWait.until(ExpectedConditions.numberOfElementsToBe(errorFileUploadLocator,expErredFiles.size()));
        List<WebElement> erredFilesLocators = driver.findElements(errorFileUploadLocator);
        for(WebElement erredFilesLocator : erredFilesLocators)
            actualErredFiles.add(erredFilesLocator.getText());
        return expErredFiles.containsAll(actualErredFiles);
    }

    public boolean isErrorSummaryCorrect(String files, String error) {
        List<String> actualErrorSummary = new ArrayList<>();
        List<String> expErrorSummary = new ArrayList<>(Arrays.asList(files.split(",")));
        expErrorSummary.replaceAll(s -> s+error);
        webDriverWait.until(ExpectedConditions.visibilityOf(driver.findElement(errorSummaryForMultiFileLocator)));
        List<WebElement> errorSummaryLocators = driver.findElements(errorSummaryForMultiFileLocator);
        for(WebElement errorSummaryLocator : errorSummaryLocators)
            actualErrorSummary.add(errorSummaryLocator.getText());
        return actualErrorSummary.containsAll(expErrorSummary);
    }

    public void deleteFile(String fileName) {
        clickEvent(By.xpath("//a[contains(@data-filename,'"+fileName+"')]"));
    }

    public void deleteFile() {
        clickEvent(deleteUnsuccessfulFilesLinkLocator);
    }

    public String getDeleteFileSuccessMessage(String fileName) {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(deleteFileSuccessLocator));
        webDriverWait.until(ExpectedConditions.textToBePresentInElementLocated(deleteFileSuccessLocator,fileName));
        return getElementText(deleteFileSuccessLocator);
    }

    public void deleteAllUnsuccessfulFiles() {
        clickEvent(deleteAllUnsuccessfulFilesLinkLocator);
    }

    public String getDeleteFileSuccessMessage() {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(deleteFileSuccessLocator));
        return getElementText(deleteFileSuccessLocator);
    }

    public boolean isUnsuccessfulFileDisplayed() {
        return driver.findElements(errorFileUploadLocator).isEmpty();
    }

    public void clearErrorMessage() {
        clickEvent(clearErrorMessageLocator);
    }

    public boolean multiFileUploadProgressCount(int expCount) {
        return webDriverWait.until(ExpectedConditions.numberOfElementsToBe(inProgressFileUploadLocator,expCount)).size()==expCount;
    }

    public boolean isDeleteSuccessMessageDisplayed() {
        return driver.findElements(deleteFileSuccessLocator).isEmpty();
    }
}
