package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Random;

public class JobSettingPage extends Page {

    private By hospitalLocator = By.id("job-setting-hospital");
    private By gpPracticeLocator = By.id("job-setting-gppractice");
    private By careHomeLocator = By.id("job-setting-carehome");
    private By communityHealthcareLocator = By.id("job-setting-community");
    private By otherRadioLocator = By.id("job-setting-other");
    private By otherTextBoxLocator = By.id("other-job-setting");
    private By continueButtonLocator = By.id("continue-button");
    private By emptyJobTypeErrorMessageLocator = By.partialLinkText("Select the applicant's");
    private By otherEmptyJobTypeErrorMessageLocator = By.partialLinkText("Enter the applicant's");
    private By jobTypeErrorMessageLocator = By.partialLinkText("Enter a job setting");
    private By selectedJobSetting = By.xpath("//input[@checked='checked']");
    private static final Random random = new Random();

    public JobSettingPage(WebDriver driver) {
        super(driver);
    }

    public void selectHospitalAndContinue() {
        clickEvent(hospitalLocator);
        continueButton();
    }

    public void selectGPPracticeAndContinue() {
        clickEvent(gpPracticeLocator);
        continueButton();
    }

    public void selectCareHomeAndContinue() {
        clickEvent(careHomeLocator);
        continueButton();
    }

    public void selectCommunityHealthcareAndContinue() {
        clickEvent(communityHealthcareLocator);
        continueButton();
    }

    public void selectOther() {
        clickEvent(otherRadioLocator);
    }

    public void enterOtherJobTypeAndContinue(String otherText) {
        sendTextValues(otherTextBoxLocator, otherText);
        continueButton();
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public String generateOverLimitJobSetting(int count) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder(51);
        for (int i = 0; i < count; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public void enterJobSettingOverLimitText(int count) {
        String jobSettingInput = generateOverLimitJobSetting(count);
        sendTextValues(otherTextBoxLocator, jobSettingInput);
    }

    public String getEmptyJobTypeErrorMessage() {
        return getElementText(emptyJobTypeErrorMessageLocator);
    }

    public String getOtherEmptyJobTypeErrorMessage() {
        return getElementText(otherEmptyJobTypeErrorMessageLocator);
    }

    public String getJobTypeErrorMessage() {
        return getElementText(jobTypeErrorMessageLocator);
    }

    public String getEnteredJobSetting() {
        return driver.findElement(selectedJobSetting).getAttribute("value");
    }

    public int getEnteredOtherJobSetting() {
        return getLengthOfEnteredText(otherTextBoxLocator);
    }
}
