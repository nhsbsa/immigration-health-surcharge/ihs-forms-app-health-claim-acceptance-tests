package com.nhsbsa.ihs.pageobjects;

import com.github.javafaker.Faker;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;
import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.Random;

public class Page {

    protected WebDriver driver;
    private WebElement element;
    public static final int EXPLICIT_WAIT_TIME = 50;
    public WebDriverWait driverWait;
    private static final Random random = new Random();
    JavascriptExecutor js = (JavascriptExecutor) driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateToUrl(String url) {
        driver.get(url);
    }

    public WebDriverWait getDriverWait() {
        driverWait = new WebDriverWait(driver, Duration.ofSeconds(EXPLICIT_WAIT_TIME));
        driverWait.ignoring(NoSuchFrameException.class);
        driverWait.ignoring(StaleElementReferenceException.class);
        driverWait.ignoring(NoSuchElementException.class);
        return driverWait;
    }

    public void waitForPageLoad() {
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(EXPLICIT_WAIT_TIME));
        wait.until(d -> {((JavascriptExecutor) driver).executeScript("return document.readyState");
            return (((JavascriptExecutor) driver).executeScript("return document.readyState"))
                    .equals("complete");
        });
    }

    public void clickEvent(By by) {
            waitForPageLoad();
            driverWait = getDriverWait();
            WebElement ele = driverWait.until(ExpectedConditions.presenceOfElementLocated(by));
            ele.click();
    }

    public void sendTextValues(By by, String text) {
            waitForPageLoad();
            driverWait = getDriverWait();
            WebElement ele = driverWait.until(ExpectedConditions.presenceOfElementLocated(by));
            ele.clear();
            ele.sendKeys(text);
        }

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public String generateOverLimitText(int count) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder(20);
        for (int i = 0; i < count; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public static String generateLongIHSNumber(int count) {
        Faker faker = new Faker();
        return "IHSC" + faker.number().randomNumber(count, true);
    }

    public static Number generateLongPhoneNumber(int count) {
        Faker faker = new Faker();
        return faker.number().randomNumber(count, true);
    }

    public int getLengthOfEnteredText(By by) {
        return driver.findElement(by).getAttribute("value").length();
    }

    public boolean getElementIsDisplayed(By by) {
        return driver.findElement(by).isDisplayed();
    }

    public void click() {
        element.click();
    }

    public String getElementText(By by) {
        return driver.findElement(by).getText();
    }

    public String getPageTitles() {
        return driver.getTitle();
    }

    public void tearDownDriver() {
        driver.quit();
    }

    public String getElementText() {
        return element.getText();
    }

    public String getElementValue(By by) {
        return driver.findElement(by).getAttribute("value");
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }
}
