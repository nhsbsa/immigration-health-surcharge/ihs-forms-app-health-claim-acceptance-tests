package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class IHSNumberPage extends Page{

    private By ihsNumberLocator = By.id("immigration-health-surcharge-number");
    private By continueButtonLocator = By.id("continue-button");
    private By ihsErrorMessageLocator = By.xpath("//*[@id=\"gov-grid-row-content\"]/div/div/div/ul/li/a");


    public IHSNumberPage(WebDriver driver) {
        super(driver);
    }

    public void enterIHSNumberAndSubmit(String ihsNumber) {
        sendTextValues(ihsNumberLocator,ihsNumber);
        clickEvent(continueButtonLocator);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public String getIHSNumberErrorMessage(){
        return getElementText(ihsErrorMessageLocator);
    }

    public void enterOverLimitIHSNumber(int count) {
        sendTextValues(ihsNumberLocator, generateLongIHSNumber(count));
    }

    public int getEnteredTextCount() {
        return getLengthOfEnteredText(ihsNumberLocator);
    }

    public String getEnteredIHSNumber() {
        return getElementValue(ihsNumberLocator);
    }

    public void enterIHSNumber(String ihsNumber) {
        sendTextValues(ihsNumberLocator, ihsNumber);
    }
}
