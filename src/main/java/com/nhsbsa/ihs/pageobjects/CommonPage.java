package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommonPage extends Page {

    private By govUKLinkLocator = By.id("logo");
    private By serviceNameLocator = By.id("proposition-name");
    private By backLinkLocator = By.id("step");
    private By acceptAndSendLocator = By.id("continue-button");
    private By cookiesLocator = By.id("cookie-link");
    private By accessibilityStatementLocator = By.id("accessibility-link");
    private By helpLocator = By.id("help-link");
    private By contactUsLocator = By.id("contact-link");
    private By termsConditionsLocator = By.id("terms-and-conditions-link");
    private By privacyNoticeLocator = By.id("privacy-link");
    private By copyrightLogoLocator = By.id("copyright-logo");
    private By openLicenceLocator = By.id("open-government-licence");
    private By cookiesBannerLocator = By.id("cookie-banner");
    private By acceptCookiesLocator = By.id("accept-cookies-button");
    private By rejectCookiesLocator = By.id("reject-cookies-button");
    private By viewCookiesLocator = By.id("cookie-banner-confirmation-view-cookies");
    private By acceptCookieSettingsLocator = By.id("cookie-banner-accept-view-cookies");
    private By rejectCookieSettingsLocator = By.id("cookie-banner-reject-view-cookies");
    private By cookiesAcceptedMessageLocator = By.id("cookie-banner-accepted");
    private By cookiesRejectedMessageLocator = By.id("cookie-banner-rejected");
    private By hideAcceptMessageLocator = By.id("hide-cookie-banner");
    private By hideRejectMessageLocator = By.id("hide-cookie-banner-reject");
    private By betaBannerFeedbackLocator = By.id("feedback-link");
    private By startSnapSurveyLocator = By.className("previewWarningSubmitButton");
    private By moreInfoOnDeclarationLocator = By.partialLinkText("information you give may be");
    private By endOfSurveyLinkLocator = By.partialLinkText("What did you think");
    private By contactUsLinkLocator = By.partialLinkText("contact us");
    private By moreInfoLocator = By.partialLinkText("more");
    private By referenceLocator = By.xpath("//div/strong");
    private By claimPeriodLocator = By.xpath("//*[@id=\"gov-grid-row-content\"]/div/form/p[2]/strong[2]");
    private By visasAndImmigrationLocator = By.id("contact-ukvi-inside-outside-uk-link");
    private By applicantLostThisNumberDropdownLocator = By.xpath("//*[@id=\"qualified-health-professional-hint\"]/details/summary/span");
    Logger logger = Logger.getLogger(CommonPage.class.getName());

    public CommonPage(WebDriver driver) {
        super(driver);
    }

    public void govUKLink() {
        clickEvent(govUKLinkLocator);
    }

    public void serviceNameLink() {
        clickEvent(serviceNameLocator);
    }

    public void backLink() {
        clickEvent(backLinkLocator);
    }

    public void acceptAndSend() {
        clickEvent(acceptAndSendLocator);
    }

    public void navigateToCookies() {
        clickEvent(cookiesLocator);
    }

    public void navigateToAccessibilityStatement() {
        clickEvent(accessibilityStatementLocator);
    }

    public void navigateToHelp () {
        clickEvent(helpLocator);
    }

    public void navigateToContactUs () {
        clickEvent(contactUsLocator);
    }

    public void navigateToTermsConditions () {
        clickEvent(termsConditionsLocator);
    }

    public void navigateToPrivacyNotice () {
        clickEvent(privacyNoticeLocator);
    }

    public void navigateToCopyrightLogo () {
        clickEvent(copyrightLogoLocator);
    }

    public void navigateToOpenLicence () {
        clickEvent(openLicenceLocator);
    }

    public void acceptAnalyticsCookies () {
        clickEvent(acceptCookiesLocator);
    }

    public void rejectAnalyticsCookies () {
        clickEvent(rejectCookiesLocator);
    }

    public void navigateToViewCookies () {
        clickEvent(viewCookiesLocator);
    }

    public void navigateToAcceptCookieSettings () {
        clickEvent(acceptCookieSettingsLocator);
    }

    public void navigateToRejectCookieSettings () {
        clickEvent(rejectCookieSettingsLocator);
    }

    public void hideAcceptCookieBanner () {
        clickEvent(hideAcceptMessageLocator);
    }

    public void hideRejectCookieBanner () {
        clickEvent(hideRejectMessageLocator);
    }

    public boolean isCookieBannerDisplayed() {
        return getElementIsDisplayed(cookiesBannerLocator);
    }

    public String getFooterLinksPageTitle(){
        List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(1));
        String getFooterLinksPageTitle = getPageTitles();
        driver.close();
        driver.switchTo().window(browserTabs.get(0));
        return getFooterLinksPageTitle;
    }

    public String getFooterLinksPageURL(){
        List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(1));
        String getFooterLinksPageURL = getUrl();
        driver.close();
        driver.switchTo().window(browserTabs.get(0));
        return getFooterLinksPageURL;
    }

    public String getCurrentPageTitle(){
        return getPageTitles();
    }

    public String getCurrentURL() {
        return getUrl();
    }

    public String getCookiesAcceptedMessage() {
        return getElementText(cookiesAcceptedMessageLocator);
    }

    public String getCookiesRejectedMessage() {
        return getElementText(cookiesRejectedMessageLocator);
    }

    public void endOfServiceSurvey() { clickEvent(endOfSurveyLinkLocator); }

    public void navigateToContactUsLink () {
        clickEvent(contactUsLinkLocator);
    }

    public void declarationPrivacy() {
        clickEvent(moreInfoOnDeclarationLocator);
    }

    public void navigateToMoreInfo () {
        clickEvent(moreInfoLocator);
    }

    public void navigateToSurveyPreview () {
        clickEvent(betaBannerFeedbackLocator);
    }

    public void navigateToSnapSurvey () {
        clickEvent(startSnapSurveyLocator);
    }

    public void clickApplicantLostThisNumberDropdown() { clickEvent(applicantLostThisNumberDropdownLocator); }

    public void navigateToVisasAndImmigrationLink() { clickEvent(visasAndImmigrationLocator); }

    public String getSurveyPreviewPageTitle(){
        List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(1));
        return getPageTitles();
    }

    public String getReferenceNumber(){
        String getReferenceNumber = getElementText(referenceLocator);
        logger.log(Level.INFO,getReferenceNumber);
        return getReferenceNumber;
    }

    public String getClaimPeriods() {
        String getClaimPeriods = getElementText(claimPeriodLocator);
        logger.log(Level.INFO,getClaimPeriods);
        return getClaimPeriods;
    }
}