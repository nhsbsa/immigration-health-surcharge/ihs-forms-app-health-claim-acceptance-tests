package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class CheckYourAnswersPage extends Page{

    private By continueButtonLocator = By.id("continue-button");
    private By claimNameChangeLocator = By.id("name-change");
    private By dobChangeLocator = By.id("date-of-birth-change");
    private By ninoChangeLocator = By.id("national-insurance-number-change");
    private By ihsNumberChangeLocator = By.id("immigration-health-surcharge-number-change");
    private By mobileChangeLocator = By.id("phone-number-change");
    private By emailChangeLocator = By.id("email-change");
    private By dependentQuestionChangeLocator = By.id("dependant-ques-change");
    private By dependant1ChangeLocator = By.id("dependant-1-change");
    private By dependant2ChangeLocator = By.id("dependant-2-change");
    private By fileAddedChangeLocator = By.id("fileEvidence-change-link");
    private By extraInfoChangeLocator = By.id("extraInfo-change-link");
    private By claimStartChangeLocator = By.id("claim-starting-date-change");
    private By reminderEmailChangeLocator = By.id("reminder-email-change");
    private By employerChangeLocator = By.id("employer-name-1-change");
    private By employerChangeLocator2 = By.id("employer-name-2-change");
    private By employerChangeLocator3 = By.id("employer-name-3-change");
    private By jobTitleChangeLocator = By.id("job-title-1-change");
    private By jobTitleChangeLocator2 = By.id("job-title-2-change");
    private By jobTitleChangeLocator3 = By.id("job-title-3-change");
    private By jobSettingChangeLocator = By.id("job-setting-1-change");
    private By jobSettingChangeLocator2 = By.id("job-setting-2-change");
    private By nameLocator = By.id("name-value");
    private By dobLocator = By.id("date-of-birth-value");
    private By ninoLocator = By.id("national-insurance-number-value");
    private By ihsNumberLocator = By.id("immigration-health-surcharge-number-value");
    private By phoneNumberLocator = By.id("phone-number-value");
    private By emailLocator = By.id("email-value");
    private By claimStartingDateLocator = By.id("claim-starting-date-value");
    private By reminderEmailLocator = By.id("reminder-email-value");
    private By employerNameLocator = By.id("employer-name-1-value");
    private By jobTitleLocator = By.id("job-title-1-value");
    private By jobSettingLocator = By.id("job-setting-1-value");
    private By employerNameLocator2 = By.id("employer-name-2-value");
    private By jobTitleLocator2 = By.id("job-title-2-value");
    private By jobSettingLocator2 = By.id("job-setting-2-value");
    private By employerNameLocator3 = By.id("employer-name-3-value");
    private By jobTitleLocator3 = By.id("job-title-3-value");
    private By jobSettingLocator3 = By.id("job-setting-3-value");
    private By dependantQuestionLocator = By.id("dependant-ques-value");
    private By dependantName1Locator = By.id("dependant-name-1-value");
    private By dependantDOB1Locator = By.id("dependant-date-of-birth-1-value");
    private By dependantIHSNumber1Locator = By.id("dependant-immigration-health-surcharge-number-1-value");
    private By dependantName2Locator = By.id("dependant-name-2-value");
    private By dependantDOB2Locator = By.id("dependant-date-of-birth-2-value");
    private By dependantIHSNumber2Locator = By.id("dependant-immigration-health-surcharge-number-2-value");
    private By dependant1locator = By.id("dependant-1");
    private By dependant2Locator = By.id("dependant-2");
    private By uploadedFile1Locator = By.id("fileEvidenceName-1-value");
    private By uploadedFile12Locator = By.id("fileEvidenceName-2-value");
    private By uploadedFilesLocator = By.xpath("//p[contains(@id,'fileEvidenceName')]");
    private By extraInfoLocator = By.id("extraInfo-value");
    private By errorMessageLocator = By.xpath("//div[@class=\"govuk-error-summary__body\"]/ul/li");
    private By extraEvidenceChangeLocator = By.id("extraEvidence-change-link");
    private By extraEvidenceLocator = By.xpath("//dd[contains(@id,'ExtraEvidenceUploadedList')]/p");
    private By filesAddedLocator = By.xpath("//dd[contains(@id,'FileAddedList')]/p");
    public CheckYourAnswersPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void claimNameChangeLink() {
        clickEvent(claimNameChangeLocator);
    }

    public void claimDOBChangeLink() {
        clickEvent(dobChangeLocator);
    }

    public void claimNINOChangeLink() {
        clickEvent(ninoChangeLocator);
    }

    public void claimIHSNumberChangeLink() {
        clickEvent(ihsNumberChangeLocator);
    }

    public void claimPhoneNumberChangeLink() {
        clickEvent(mobileChangeLocator);
    }

    public void claimEmailChangeLink() {
        clickEvent(emailChangeLocator);
    }

    public void claimFileAttachedChangeLink() {
        clickEvent(fileAddedChangeLocator);
    }

    public void extraInformationChangeLink() {
        clickEvent(extraInfoChangeLocator);
    }

    public void claimDependentQuestionChangeLink() {
        clickEvent(dependentQuestionChangeLocator);
    }

    public void claimDependant1ChangeLink(){
        clickEvent(dependant1ChangeLocator);
    }

    public void claimDependant2ChangeLink(){
        clickEvent(dependant2ChangeLocator);
    }

    public void claimStartDateChangeLink() {
        clickEvent(claimStartChangeLocator);
    }

    public void reminderEmailChangeLink() {
        clickEvent(reminderEmailChangeLocator);
    }

    public void employerChangeLink() {
        clickEvent(employerChangeLocator);
    }

    public void jobTitleChangeLink() {
        clickEvent(jobTitleChangeLocator);
    }

    public void jobSettingChangeLink() {
        clickEvent(jobSettingChangeLocator);
    }

    public String getApplicantName() { return getElementText(nameLocator); }

    public String getApplicantDOB() { return getElementText(dobLocator); }

    public String getApplicantNINONumber() { return getElementText(ninoLocator); }

    public String getApplicantIHSNumber() { return getElementText(ihsNumberLocator); }

    public String getApplicantPhoneNumber() { return getElementText(phoneNumberLocator); }

    public String getApplicantEmail() { return getElementText(emailLocator); }

    public String getApplicantClaimStartingDate() { return getElementText(claimStartingDateLocator); }

    public String getApplicantReminderEmail() { return getElementText(reminderEmailLocator); }

    public String getEmployerName() {
        return getElementText(employerNameLocator);
    }

    public String getApplicantEmployerDetails() {
        String empDetail = getElementText(employerNameLocator);

        if(!driver.findElements(jobTitleLocator).isEmpty()) {
            empDetail =  empDetail + " " + getElementText(jobTitleLocator);
            if(!driver.findElements(jobSettingLocator).isEmpty()) {
                empDetail = empDetail + " " + getElementText(jobSettingLocator);
            }}

        if(!driver.findElements(employerNameLocator2).isEmpty()) {
            empDetail =  empDetail + " " + getElementText(employerNameLocator2);
            if(!driver.findElements(jobTitleLocator2).isEmpty()) {
                empDetail =  empDetail + " " + getElementText(jobTitleLocator2);
                if(!driver.findElements(jobSettingLocator2).isEmpty()) {
                    empDetail = empDetail + " " + getElementText(jobSettingLocator2);
                }}}
        if(!driver.findElements(employerNameLocator3).isEmpty()) {
            empDetail =  empDetail + " " + getElementText(employerNameLocator3);
            if(!driver.findElements(jobTitleLocator3).isEmpty()) {
                empDetail =  empDetail + " " + getElementText(jobTitleLocator3);
                if(!driver.findElements(jobSettingLocator3).isEmpty()) {
                    empDetail = empDetail + " " + getElementText(jobSettingLocator3);
                }}}
        return empDetail;
    }

    public String getDependantQues() { return getElementText(dependantQuestionLocator);}

    public String getDependantName() {return getElementText(dependantName1Locator);}

    public String getDependantDob() {return getElementText(dependantDOB1Locator);}

    public String getDependantIHSNumber() {return getElementText(dependantIHSNumber1Locator);}

    public String getDependantName2() {return getElementText(dependantName2Locator);}

    public String getDependantDob2() {return getElementText(dependantDOB2Locator);}

    public String getDependantIHSNumber2() {return getElementText(dependantIHSNumber2Locator);}

    public boolean isDependant1Visible() { return isElementPresent(dependant1locator); }

    public boolean isDependant2Visible() { return isElementPresent(dependant2Locator); }

    public void employerChangeLink2() {
        clickEvent(employerChangeLocator2);
    }

    public void employerChangeLink3() {
        clickEvent(employerChangeLocator3);
    }

    public void jobTitleChangeLink2() {
        clickEvent(jobTitleChangeLocator2);
    }

    public void jobTitleChangeLink3() {
        clickEvent(jobTitleChangeLocator3);
    }

    public void jobSettingChangeLink2() {
        clickEvent(jobSettingChangeLocator2);
    }

    public String getUploadedFile1() { return getElementText(uploadedFile1Locator); }

    public String getUploadedFile2() { return getElementText(uploadedFile12Locator); }

    public String getExtraInformationDisplayed() { return getElementText(extraInfoLocator); }

    public List<String> getUploadedFiles() {
        List<WebElement> filesLocator = driver.findElements(uploadedFilesLocator);
        List<String> filesText = new ArrayList<>();
        for(WebElement fileLocator: filesLocator)
        {
            filesText.add(fileLocator.getText().replace(" successfully uploaded",""));
        }
        return filesText;
    }

    public String getErrorMessage() {
        return getElementText(errorMessageLocator);
    }

    public void extraEvidenceChangeLink() {
        clickEvent(extraEvidenceChangeLocator);
    }

    public List<String> getExtraEvidenceUploaded() {
        List<WebElement> filesLocator = driver.findElements(extraEvidenceLocator);
        List<String> filesText = new ArrayList<>();
        for (WebElement fileLocator : filesLocator) {
            filesText.add(fileLocator.getText());
        }
        return filesText;
    }

    public int getFilesAddedCount() {
        List<WebElement> filesLocator = driver.findElements(filesAddedLocator);
        List<String> filesText = new ArrayList<>();
        for (WebElement fileLocator : filesLocator) {
            filesText.add(fileLocator.getText());
        }
        int fileCount = filesText.size();
        return (fileCount);
    }

    public int getExtraEvidenceCount() {
        List<WebElement> filesLocator = driver.findElements(extraEvidenceLocator);
        List<String> filesText = new ArrayList<>();
        for (WebElement fileLocator : filesLocator) {
            filesText.add(fileLocator.getText());
        }
        int extraEvidenceCount = filesText.size();
        return (extraEvidenceCount);
    }

}