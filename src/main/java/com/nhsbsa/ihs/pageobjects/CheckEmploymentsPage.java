package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckEmploymentsPage extends Page {

    private By deleteEmployer1Locator = By.id("delete-employer-1");
    private By deleteEmployer2Locator = By.id("delete-employer-2");
    private By changeEmployerNameLocator = By.id("employer-name-1-change");
    private By changeEmployerNameLocator2 = By.id("employer-name-2-change");
    private By changeJobTitleLocator = By.id("job-title-1-change");
    private By changeJobSettingLocator = By.id("job-setting-1-change");
    private By yesRadioButtonLocator = By.id("add-more-employer-yes");
    private By noRadioButtonLocator = By.id("add-more-employer-no");
    private By continueButtonLocator = By.id("continue-button");
    private By deleteSuccessLocator = By.id("employer-delete-success");
    private By checkEmploymentErrorMessageLocator = By.partialLinkText("Select 'Yes' if");
    private By displayedEmployerNameLocator = By.id("employer-name-1-value");
    private By displayedJobTitleLocator = By.id("job-title-1-value");
    private By displayedJobSettingLocator = By.id("job-setting-1-value");

    public CheckEmploymentsPage(WebDriver driver) {
        super(driver);
    }

    public void deleteEmployer1() {
        clickEvent(deleteEmployer1Locator);
    }

    public void deleteEmployer2() {
        clickEvent(deleteEmployer2Locator);
    }

    public void changeEmployerName() {
        clickEvent(changeEmployerNameLocator);
    }

    public void changeJobTitle() {
        clickEvent(changeJobTitleLocator);
    }

    public void changeJobSetting() {
        clickEvent(changeJobSettingLocator);
    }

    public void selectYesAndContinue() {
        clickEvent(yesRadioButtonLocator);
        continueButton();
    }

    public void selectNoAndContinue() {
        clickEvent(noRadioButtonLocator);
        continueButton();
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public String getDeleteEmployerSuccessMessage() {
        return getElementText(deleteSuccessLocator);
    }

    public String getCheckEmploymentErrorMessage() {
        return getElementText(checkEmploymentErrorMessageLocator);
    }

    public String getDisplayedEmployerName() {
        return getElementText(displayedEmployerNameLocator);
    }

    public String getDisplayedJobTitle() {
        return getElementText(displayedJobTitleLocator);
    }

    public String getDisplayedJobSetting() {
        return getElementText(displayedJobSettingLocator);
    }

    public boolean isJobTitleNotDisplayed() {
        return driver.findElements(displayedJobTitleLocator).isEmpty();
    }

    public boolean isJobSettingNotDisplayed() {
        return driver.findElements(displayedJobSettingLocator).isEmpty();
    }

    public void changeEmployerName2() {
        clickEvent(changeEmployerNameLocator2);
    }
}

