package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.util.Random;

public class EmployerNamePage extends Page {

    private By employerNameLocator = By.id("employer-name");
    private By continueButtonLocator = By.id("continue-button");
    private By employerNameErrorMessageLocator = By.partialLinkText("Enter an employer name");
    private By emptyEmployerNameErrorMessageLocator = By.partialLinkText("Enter the applicant's");
    private static final Random random = new Random();

    public EmployerNamePage(WebDriver driver) {
        super(driver);
    }

    public void enterEmployerName(String employerName) {
        sendTextValues(employerNameLocator, employerName);
    }

    public void enterEmployerAndSubmit(String employer) {
        sendTextValues(employerNameLocator, employer);
        clickEvent(continueButtonLocator);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public String generateOverLimitEmployerName(int count) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder(256);
        for (int i = 0; i < count; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public void enterEmployerOverLimitText(int count) {
        String employerInput = generateOverLimitEmployerName(count);
        sendTextValues(employerNameLocator, employerInput);
    }

    public String getEmptyEmployerNameErrorMessage() {
        return getElementText(emptyEmployerNameErrorMessageLocator);
    }

    public String getEmployerNameErrorMessage() {
        return getElementText(employerNameErrorMessageLocator);
    }

    public int getEnteredEmployerNameLength() {
        return getLengthOfEnteredText(employerNameLocator);
    }

    public String getEnteredEmployerName() {
        return getElementValue(employerNameLocator);
    }

    public void selectEmployerFromList(String emp, String employerName) {
        WebElement empName = driver.findElement(employerNameLocator);
        empName.sendKeys(emp);
        driverWait = getDriverWait();
        driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='autocomplete ']/div[(text()= '"+employerName+"')]"))).click();
        clickEvent(continueButtonLocator);
    }
}
