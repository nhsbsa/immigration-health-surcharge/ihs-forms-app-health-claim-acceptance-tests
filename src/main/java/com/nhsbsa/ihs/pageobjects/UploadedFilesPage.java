package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UploadedFilesPage extends Page {

    private By moreEvidenceYesRadioButtonLocator = By.id("add-more-files-evidence-yes");
    private By moreEvidenceNoRadioButtonLocator = By.id("add-more-files-evidence-no");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.xpath("//div[@class=\"govuk-error-summary__body\"]/ul/li");
    private By uploadedFileLocator = By.xpath("//*[@id=\"uploaded-files-evidence\"]/dl/div/dd");
    private By deleteUploadedFileSuccessLocator = By.xpath("//div[@class=\"govuk-notification-banner__content\"]//p");
    private By deleteEvidence1Locator = By.id("delete-evidence-1");
    private By deleteEvidence2Locator = By.id("delete-evidence-2");

    public UploadedFilesPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void selectYesRadioButtonAndContinue() {
        clickEvent(moreEvidenceYesRadioButtonLocator);
        continueButton();
    }

    public void selectNoRadioButtonAndContinue() {
        clickEvent(moreEvidenceNoRadioButtonLocator);
        continueButton();
    }

    public String getErrorMessage() {
        return getElementText(errorMessageLocator);
    }

    public String getUploadedFile() {
        return getElementText(uploadedFileLocator);
    }

    public boolean isNoRadioButtonSelected() {
        return driver.findElement(moreEvidenceNoRadioButtonLocator).isSelected();
    }

    public String getDeleteUploadedFileSuccessLocator() {
        return getElementText(deleteUploadedFileSuccessLocator);

    }

    public void deleteEvidence1Link() {
        clickEvent(deleteEvidence1Locator);
    }

    public void deleteEvidence2Link() {
        clickEvent(deleteEvidence2Locator);
    }
}