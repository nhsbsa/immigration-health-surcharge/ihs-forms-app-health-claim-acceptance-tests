package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DependantDetailsPage extends Page {

    private By givenNameDependantLocator = By.id("dependant-given-name");
    private By familyNameDependantLocator = By.id("dependant-family-name");
    private By dayLocator = By.id("dependant-date-of-birth-day");
    private By monthLocator = By.id("dependant-date-of-birth-month");
    private By yearLocator = By.id("dependant-date-of-birth-year");
    private By ihsNumberDependantLocator = By.id("dependant-immigration-health-surcharge-number");
    private By continueButtonLocator = By.id("continue-button");
    private By dependantErrorMessageLocator = By.partialLinkText("Enter the dependant's");
    private By dependantIHSErrorMessageLocator = By.xpath("//*[@id=\"gov-grid-row-content\"]/div/div/div/ul/li/a");

    public DependantDetailsPage(WebDriver driver){super(driver);}

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterGivenName(String givenName) {
        sendTextValues(givenNameDependantLocator,givenName);
    }

    public void enterFamilyName(String familyName) {
        sendTextValues(familyNameDependantLocator,familyName);
    }

    public void enterDateOfBirth(String day, String month, String year) {
        sendTextValues(dayLocator, day);
        sendTextValues(monthLocator, month);
        sendTextValues(yearLocator, year);
    }

    public void enterIHSNumber(String ihsNumber) {
        sendTextValues(ihsNumberDependantLocator,ihsNumber);
    }

    public void enterNameAndSubmit(String givenName, String familyName) {
        sendTextValues(givenNameDependantLocator,givenName);
        sendTextValues(familyNameDependantLocator,familyName);
        continueButton();
    }

    public void enterDateOfBirthAndSubmit(String day, String month, String year) {
        sendTextValues(dayLocator, day);
        sendTextValues(monthLocator, month);
        sendTextValues(yearLocator, year);
        continueButton();
    }

    public void enterIHSandSubmit (String ihsNumber) {
        sendTextValues(ihsNumberDependantLocator,ihsNumber);
        continueButton();
    }

    public void enterDependantDetailsAndSubmit (String givenName, String familyName, String day, String month, String year, String ihsNumber) {
        sendTextValues(givenNameDependantLocator,givenName);
        sendTextValues(familyNameDependantLocator,familyName);
        sendTextValues(dayLocator, day);
        sendTextValues(monthLocator, month);
        sendTextValues(yearLocator, year);
        sendTextValues(ihsNumberDependantLocator,ihsNumber);
        continueButton();
    }

    public String getDependantErrorMessage() {
        return getElementText(dependantErrorMessageLocator);
    }

    public String getDependantIHSErrorMessage() {
        return getElementText(dependantIHSErrorMessageLocator);
    }

    public String getEnteredGivenName() {
        return getElementValue(givenNameDependantLocator);
    }

    public String getEnteredFamilyName() {
        return getElementValue(familyNameDependantLocator);
    }

    public String getEnteredDay() {
        waitForPageLoad();
        return getElementValue(dayLocator);
    }

    public String getEnteredMonth() {
        return getElementValue(monthLocator);
    }

    public String getEnteredYear() {
        return getElementValue(yearLocator);
    }

    public String getEnteredIHSNumber() {
        return getElementValue(ihsNumberDependantLocator);
    }
}
