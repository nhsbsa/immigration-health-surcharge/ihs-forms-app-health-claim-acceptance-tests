package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NamePage extends Page {

    private By givenNameLocator = By.id("given-name");
    private By familyNameLocator = By.id("family-name");
    private By continueButtonLocator = By.id("continue-button");
    private By givenNameErrorMessageLocator = By.partialLinkText("Enter the applicant's given name");
    private By familyNameErrorMessageLocator = By.partialLinkText("Enter the applicant's family name");

    public NamePage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterNameAndSubmit(String givenName, String familyName) {
        sendTextValues(givenNameLocator, givenName);
        sendTextValues(familyNameLocator, familyName);
        continueButton();
    }

    public String getGivenNameErrorMessage() {
        return getElementText(givenNameErrorMessageLocator);
    }

    public String getFamilyNameErrorMessage() {
        return getElementText(familyNameErrorMessageLocator);
    }

    public void enterOverLimitText(int count) {
        sendTextValues(givenNameLocator, generateOverLimitText(count));
        sendTextValues(familyNameLocator, generateOverLimitText(count));
    }

    public int getEnteredTextCount() {
        return getLengthOfEnteredText(givenNameLocator);
    }

    public String getEnteredGivenName() {
        return getElementValue(givenNameLocator);
    }

    public String getEnteredFamilyName() {
        return getElementValue(familyNameLocator);
    }

    public void enterName(String givenName, String familyName) {
        sendTextValues(givenNameLocator, givenName);
        sendTextValues(familyNameLocator, familyName);
    }
}


