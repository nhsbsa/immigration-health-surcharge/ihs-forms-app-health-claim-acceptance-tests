package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SubscriptionPage extends Page {

    private By yesRadioButtonLocator = By.id("subscribe-for-reminder-yes");
    private By noRadioButtonLocator = By.id("subscribe-for-reminder-no");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select 'Yes' if");

    public SubscriptionPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void selectYesRadioButtonAndClickContinue() {
        clickEvent(yesRadioButtonLocator);
        continueButton();
    }

    public void selectNoRadioButtonAndClickContinue() {
        clickEvent(noRadioButtonLocator);
        continueButton();
    }

    public String getErrorMessageSelectNoneOption() {
        return getElementText(errorMessageLocator);
    }

    public boolean isYesRadioButtonSelected() {
        return driver.findElement(yesRadioButtonLocator).isSelected();
    }

    public void selectNoRadioButton() {
        clickEvent(noRadioButtonLocator);
    }
}
