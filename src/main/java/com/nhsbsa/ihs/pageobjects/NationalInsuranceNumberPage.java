package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NationalInsuranceNumberPage extends Page{

    private By ninoLocator = By.id("national-insurance-number");
    private By continueButtonLocator = By.id("continue-button");
    private By ninoErrorMessageLocator = By.partialLinkText("Enter the applicant's");

    public NationalInsuranceNumberPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterNINOAndSubmit(String nino) {
        sendTextValues(ninoLocator, nino);
        continueButton();
    }

    public String getNINOErrorMessage() {
        return getElementText(ninoErrorMessageLocator);

    }

    public String getEnteredNINO() {
        return getElementValue(ninoLocator);
    }

    public void enterNINO(String nino) {
        sendTextValues(ninoLocator, nino);
    }
}
