package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ExtraInformationPage extends Page {

    private By extraInformationInputLocator = By.id("extra-information");
    private By continueButtonLocator = By.id("continue-button");
    private By chooseFileButtonLocator = By.id("extra-info-upload-evidence");
    private By errorMessageLocator = By.xpath("//div[@class=\"govuk-error-summary__body\"]/ul/li/a");
    private By invalidFileErrorLocator = By.xpath("//div[@class=\"govuk-error-summary__body\"]/ul/li[2]/a");
    private By deleteUploadedFileSuccessLocator = By.xpath("//*[@id=\"multi-file-notification-banner-info\"]/div/div[2]/p");
    WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(100));

    public ExtraInformationPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterExtraInformationAndContinue(String commentInput) {
        sendTextValues(extraInformationInputLocator, commentInput);
        continueButton();
    }

    public void enterOverLimitExtraInformationText(int count) {
        sendTextValues(extraInformationInputLocator, generateOverLimitText(count));
    }

    public int getEnteredTextCount() {
        return getLengthOfEnteredText(extraInformationInputLocator);
    }

    public String getEnteredExtraInformation() { return getElementText(extraInformationInputLocator); }

    public void enterExtraInformation(String extraInformationInput) {
        sendTextValues(extraInformationInputLocator, extraInformationInput);
    }
    public void chooseFileAndContinue(String fileName) {
        sendTextValues(chooseFileButtonLocator, fileName);
        continueButton();
    }
    public String getErrorMessage() {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(errorMessageLocator));
        return getElementText(errorMessageLocator);
    }

    public String getDeleteUploadedFileSuccessLocator() {
        return getElementText(deleteUploadedFileSuccessLocator);
    }

    public String getInvalidFileErrorMessage() {
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(invalidFileErrorLocator));
        return getElementText(invalidFileErrorLocator);
    }
}
