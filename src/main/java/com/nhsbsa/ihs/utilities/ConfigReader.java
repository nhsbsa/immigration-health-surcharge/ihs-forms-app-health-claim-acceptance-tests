package com.nhsbsa.ihs.utilities;

import com.nhsbsa.ihs.shared.SharedData;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigReader {

    private static String baseURL;
    private static String apiKey;
    private static String apiURL;
    private static final String STAGE = "stage";

    private ConfigReader() {
        throw new IllegalStateException("ConfigReader Utility Class");
    }
    static Logger logger = Logger.getLogger(ConfigReader.class.getName());
    public static String getEnvironment() {
        SharedData.environment = System.getProperty("env");
        String getURL = System.getenv("IHS_HEALTH_CLAIM");

        switch (SharedData.environment) {
            case "dev", "tst", STAGE:
                baseURL = getURL.replace("{env}", SharedData.environment);
                break;
            case "prod":
                baseURL = (System.getenv("IHS_HEALTH_PROD"));
                break;
            case "local":
                baseURL = (System.getenv("IHS_HEALTH_LOCAL"));
                break;
            default:
                logger.log(Level.SEVERE,"Please provide environment details");
                break;
        }
        return baseURL;
    }

    public static String getValidationUrl(String validationDate) {
        baseURL = System.getenv("IHS_HEALTH_CLAIM").replace("{env}", System.getProperty("env"));
        return baseURL.concat("/set-test-settings?validationDate=" + validationDate);
    }

    public static String getUser() {
        return System.getenv("SAUCE_USERNAME");
    }

    public static String getAccessKey() {
        return System.getenv("SAUCE_ACCESS_KEY");
    }

    public static String getServer() {
        return System.getenv("ONDEMAND_SERVER");
    }

    public static String getTunnelName(){
        return System.getenv("SAUCE_TUNNEL_NAME");
    }

    public static String getAppiumVersion(){ return System.getenv("SAUCE_APPIUM_VERSION"); }

    public static String getApiUrl() {
        switch (SharedData.environment) {
            case "dev":
                apiURL = System.getenv("IHS_DEV_API_URL");
                break;
            case "tst":
                apiURL = System.getenv("IHS_TST_API_URL");
                break;
            case STAGE:
                apiURL = System.getenv("IHS_STG_API_URL");
                break;
            case "prod":
                apiURL = System.getenv("IHS_PRO_API_URL");
                break;
            default:
                logger.log(Level.SEVERE,"Please provide API URL");
                break;
        }
        return apiURL;
    }

    public static String getApiKey() {
        switch (SharedData.environment) {
            case "dev":
                apiKey = System.getenv("IHS_DEV_API_KEY");
                break;
            case "tst":
                apiKey = System.getenv("IHS_TST_API_KEY");
                break;
            case STAGE:
                apiKey = System.getenv("IHS_STG_API_KEY");
                break;
            case "prod":
                apiKey = System.getenv("IHS_PRO_API_KEY");
                break;
            default:
                logger.log(Level.SEVERE,"Please provide API Key");
                break;
        }
        return apiKey;
    }
}
