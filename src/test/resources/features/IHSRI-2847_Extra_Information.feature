@ExtraInformation @IHSRI-1635 @IHSRI-2682 @IHSRI-2847 @Regression

Feature: Validation of the Extra Information page on the IHS claim app to enable users to enter any extra information about their employment or evidence

  Background:
    Given I launch the IHS Health claim application

  Scenario Outline: Validate the enter extra information functionality for positive and negative scenarios
    And My details are captured until Extra Information screen - MultiFile
    When My entered extra information is <information>
    And I do not upload the file on Extra Information screen
    Then I will see the <screen>
    Examples:
      | information                                                            | screen                                 |
      #positive tests
      |                                                                        | Check your answers screen              |
      | 1                                                                      | Check your answers screen              |
      | 1. Test extra information                                              | Check your answers screen              |
      | 2-Test extra information                                               | Check your answers screen              |
      | Test extra information with £20                                        | Check your answers screen              |
      | Test extra information with £20, £50                                   | Check your answers screen              |
      | 'Testing extra information'.                                           | Check your answers screen              |
      | ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 1234567890 -',£. | Check your answers screen              |
      | Testing the enter key functionality \n Thanks, Tester                  | Check your answers screen              |
      #negative tests
      | "Testing extra information"                                            | Invalid extra information format error |
      | Testing extra information with @                                       | Invalid extra information format error |
      | Testing extra information with 20 & 50                                 | Invalid extra information format error |
      | (Testing extra information)                                            | Invalid extra information format error |
      | Testing extra information with #                                       | Invalid extra information format error |

  @JavaScriptDisabled
  Scenario Outline: Validate the extra information functionality for acceptable and non-acceptable characters length - SingleFile
    And My details are captured until Extra Information screen
    When I submit the extra information with <count> characters
    Then I will verify <charactersCount> characters in the extra information
    And I will see the <screen>
    Examples:
      | count | charactersCount | screen                    |
      | 50    | 50              | Check your answers screen |
      | 500   | 500             | Check your answers screen |
      | 1000  | 1000            | Check your answers screen |
      | 1999  | 1999            | Check your answers screen |
      | 2000  | 2000            | Check your answers screen |
       #negative tests
      | 2001  | 2000            | Check your answers screen |
      | 2500  | 2000            | Check your answers screen |
      | 3000  | 2000            | Check your answers screen |

  Scenario Outline: Validate the extra information functionality for acceptable and non-acceptable characters length - MultiFile
    And My details are captured until Extra Information screen - MultiFile
    When I submit the extra information with <count> characters
    Then I will verify <charactersCount> characters in the extra information
    And I will see the <output>

    Examples:
      | count | charactersCount | output                          |
      | 50    | 50              | Check your answers screen       |
      | 500   | 500             | Check your answers screen       |
      | 1000  | 1000            | Check your answers screen       |
      | 1999  | 1999            | Check your answers screen       |
      | 2000  | 2000            | Check your answers screen       |
      # negative tests
      | 2001  | 2001            | More than 2000 characters error |
      | 2500  | 2500            | More than 2000 characters error |
      | 3000  | 3000            | More than 2000 characters error |

  Scenario Outline: Validate the extra information and upload evidence functionality for valid details - MultiFile
    And My details are captured until Extra Information screen - MultiFile
    When My entered extra information is <information>
    And I <uploadFile> upload the file on Extra Information screen
    Then I will see the <screen>

    Examples:
      | information            | uploadFile | screen                         |
      | Test Extra Information | do not     | Check your answers screen      |
      |                        | do not     | Check your answers screen      |
      |                        | do         | Extra Evidence Uploaded screen |
      | Test Extra Information | do         | Extra Evidence Uploaded screen |

  @Retest-2770 @JavaScriptDisabled
  Scenario Outline: Validate the extra information and upload evidence functionality for valid details - SingleFile
    And My details are captured until Extra Information screen
    When My entered extra information is <information>
    And I <uploadFile> upload the file on Extra Information screen
    Then I will see the <screen>

    Examples:
      | information            | uploadFile | screen                         |
      |                        | do not     | Check your answers screen      |
      |                        | do         | Extra Evidence Uploaded screen |
      | Test Extra Information | do         | Extra Evidence Uploaded screen |
      | Test Extra Information | do not     | Check your answers screen      |

  Scenario Outline: Validate the extra evidence upload functionality for the acceptable 'File Format'
    And My details are captured until Extra Information screen - MultiFile
    When I upload a file <fileName> of format <fileFormat>
    Then I will see the <screen>
    Examples:
      | fileName | fileFormat | screen                         |
      #valid file formats
      | Sample_1 | .jpg       | Extra Evidence Uploaded screen |
      | sample_2 | .png       | Extra Evidence Uploaded screen |
      | sample_3 | .pdf       | Extra Evidence Uploaded screen |
      | Sample_4 | .bmp       | Extra Evidence Uploaded screen |
      | Sample_5 | .jpeg      | Extra Evidence Uploaded screen |

  @Retest-2818 @JavaScriptDisabled
  Scenario Outline: Validate the extra evidence upload functionality for the non-acceptable 'File Format'
    And My details are captured until Extra Information screen
    When I upload a file <fileName> of format <fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> extra evidence
    Examples:
      | fileName     | fileFormat | error                     |
      #invalid file formats
      | Sample excel | .xlsx      | Invalid file format error |
      | Sample text  | .rtf       | Invalid file format error |
      | Sample word  | .doc       | Invalid file format error |

  Scenario Outline: Validate the extra evidence upload functionality for the acceptable 'File Name'
    And My details are captured until Extra Information screen - MultiFile
    When I upload a file with name <fileName><fileFormat>
    Then I will see the <screen>
    Examples:
      | fileName           | fileFormat | screen                         |
      #valid file names
      | Payslip-June       | .png       | Extra Evidence Uploaded screen |
      | My Payslip_1       | .pdf       | Extra Evidence Uploaded screen |
      | Payslip July       | .bmp       | Extra Evidence Uploaded screen |
      | 123456             | .pdf       | Extra Evidence Uploaded screen |
      | 1-payslip_May Name | .jpg       | Extra Evidence Uploaded screen |
      | Payslip   Mar      | .pdf       | Extra Evidence Uploaded screen |

  Scenario Outline: Validate the extra evidence upload functionality for the non-acceptable 'File Name'
    And My details are captured until Extra Information screen - MultiFile
    When I upload a file with name <fileName><fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> extra evidence
    Examples:
      | fileName     | fileFormat | error                   |
      #invalid file names
      | 'My Payslip' | .jpg       | Invalid file name error |
      | Payslip(Nov) | .png       | Invalid file name error |
      | Payslip.Jan  | .pdf       | Invalid file name error |

  @Retest-2782
  Scenario Outline: Validate the extra evidence upload functionality for the 'File Name' with maximum characters
    And My details are captured until Extra Information screen - MultiFile
    When I upload a file with name <fileName><fileFormat>
    Then I will see the <output>
    Examples:
      | fileName                                                                                                                           | fileFormat | output                         |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       | Extra Evidence Uploaded screen |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890a  | .pdf       | Extra Evidence Uploaded screen |

  @Retest-2782
  Scenario Outline: Validate the extra evidence upload functionality for the 'File Name' with maximum characters
    And My details are captured until Extra Information screen - MultiFile
    When I upload a file with name <fileName><fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> extra evidence
    Examples:
      | fileName                                                                                                                             | fileFormat | error                          |
      | File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-123  | .png       | More than 130 characters error |
      | File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-abcd | .pdf       | More than 130 characters error |

  Scenario Outline: Validate the extra evidence upload functionality for the acceptable 'File Size'
    And My details are captured until Extra Information screen - MultiFile
    When I upload a file <fileName><fileFormat> of size <fileSize>
    Then I will see the <screen>
    Examples:
      | fileName               | fileFormat | fileSize | screen                         |
       #valid file sizes
      | small_size_file        | .bmp       | > 35KB   | Extra Evidence Uploaded screen |
      | SampleFile             | .jpg       | > 500KB  | Extra Evidence Uploaded screen |
      | thisIsMy_PDF_1MB       | .pdf       | > 1MB    | Extra Evidence Uploaded screen |
      | Large_size_jpg_file    | .jpg       | > 1MB    | Extra Evidence Uploaded screen |
      | Large_size_png_file    | .png       | > 1MB    | Extra Evidence Uploaded screen |
      | Sample-jpg-image-1-1mb | .jpeg      | > 1MB    | Extra Evidence Uploaded screen |
      | Sample-jpg-image-1-2mb | .jpeg      | > 1.2MB  | Extra Evidence Uploaded screen |
      | Sample-jpg-image-1-3mb | .jpeg      | > 1.3MB  | Extra Evidence Uploaded screen |
      | Sample-jpg-image-1-5mb | .jpeg      | > 1.5MB  | Extra Evidence Uploaded screen |
      | Sample-jpg-image-1-7mb | .jpeg      | > 1.7MB  | Extra Evidence Uploaded screen |
      | Sample-jpg-image-1-8mb | .jpeg      | > 1.8MB  | Extra Evidence Uploaded screen |
      | Sample-jpg-image-1-9mb | .jpeg      | > 1.9MB  | Extra Evidence Uploaded screen |
      | File_size_2MB          | .bmp       | 2MB      | Extra Evidence Uploaded screen |

  Scenario Outline: Validate the extra evidence upload functionality for the non-acceptable 'File Size'
    And My details are captured until Extra Information screen - MultiFile
    When I upload a file <fileName><fileFormat> of size <fileSize>
    Then I will be displayed <error> for <fileName><fileFormat> extra evidence
    Examples:
      | fileName       | fileFormat | fileSize | error               |
      #invalid file sizes
      | More_than_2MB  | .bmp       | > 2MB    | Max file size error |
      | More_than_2MB  | .jpeg      | > 2MB    | Max file size error |
      | More_than_2MB  | .jpg       | > 2MB    | Max file size error |
      | More_than_2MB  | .pdf       | > 2MB    | Max file size error |
      | More_than_2MB  | .png       | > 2MB    | Max file size error |
      | More_than_5MB  | .bmp       | > 5MB    | Max file size error |
      | More_than_5MB  | .pdf       | > 5MB    | Max file size error |
      | More_than_10MB | .bmp       | > 10MB   | Max file size error |
      | More_than_10MB | .jpeg      | > 10MB   | Max file size error |
      | More_than_10MB | .jpg       | > 10MB   | Max file size error |
      | More_than_10MB | .pdf       | > 10MB   | Max file size error |
      | More_than_30MB | .pdf       | > 30MB   | Max file size error |
      | More_than_30MB | .png       | > 30MB   | Max file size error |
      | More_than_50MB | .bmp       | > 50MB   | Max file size error |

  @Retest-2780
  Scenario Outline: Validate the error for invalid extra information and evidence
    And My details are captured until Extra Information screen - MultiFile
    When My entered extra information is <invalidInformation>
    And I upload a file with name <fileName><fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> extra evidence

    Examples:
      | invalidInformation       | fileName     | fileFormat | error                                                 |
      | My comments are @ (test) | Payslip(Nov) | .png       | Invalid extra information and Invalid file error      |
      | My comments are @ test & | Payslip-June | .png       | Invalid extra information and File not uploaded error |

  Scenario Outline: Validate the back link functionality for invalid extra information and evidence
    And My details are captured until Extra Information screen - MultiFile
    When My entered extra information is <invalidInformation>
    And I upload a file with name <fileName><fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> extra evidence
    And I select the Back link
    When I click on continue
    Then I will see the Extra Information screen

    Examples:
      | invalidInformation       | fileName     | fileFormat | error                                            |
      | My comments are @ test & | Payslip(Nov) | .png       | Invalid extra information and Invalid file error |

  Scenario Outline: Validate the hyperlinks on Extra Information page
    And My details are captured until Extra Information screen - MultiFile
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                 |
      | Back link         | Upload Evidence screen |
      | Service Name link | Start screen           |
      | GOV.UK link       | GOV UK screen          |