@AddDependant @IHSRI-1615 @Regression

Feature: Validation of the Dependant Question Page on the IHS claim app to enable users to choose if they have dependants to add

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Dependant Question screen

  @Smoke @Retest-1749 @Retest-1773 @Retest-1774
  Scenario Outline: Validate the user is able to add dependants to the claim application
    When I <dependantOption> have dependant to add
    Then I will see the <output>
    Examples:
      | dependantOption | output                   |
      | do              | Dependant details screen |
      | do not          | Claim start date screen  |
      |                 | Invalid selection error  |

  @Retest-1747
  Scenario Outline: Validate the hyperlinks on Dependant Question page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen              |
      | Back link         | Phone number screen |
      | Service Name link | Start screen        |
      | GOV.UK link       | GOV UK screen       |