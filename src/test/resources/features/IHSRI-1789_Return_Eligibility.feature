@ReturnEligibility @IHSRI-1789
Feature: Validation of the eligibility check for return journey on the IHS claim app when user has previously claimed IHS reimbursement

  Background:
    Given I launch the IHS Health claim application

  Scenario Outline: Validate the returning user is redirected to 'Currently Not Eligible' screen when returned too soon to submit a new claim
  (Tested without validation date parameter = Application date is today's date)
    Given My previous claim details are <StartDate> and <EndDate>
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see the <Output>
    And I will see claim dates <startDate> and <endDate> of my previous claim
    And I will see earliest date I can submit the new claim is <EarliestDate>
    Examples:
      | StartDate  | EndDate    | startDate        | endDate      | EarliestDate     | Output                        |
      | 2023-09-09 | 2024-03-08 | 9 September 2023 | 8 March 2024 | 9 September 2024 | Currently not eligible screen |
      | 2024-01-01 | 2024-06-30 | 1 January 2024   | 30 June 2024 | 1 January 2025   | Currently not eligible screen |

  Scenario Outline: Validate the returning user is redirected to 'Email address' screen when eligible to submit a new claim
  (Tested without validation date parameter = Application date is today's date)
    Given My previous claim details are <StartDate> and <EndDate>
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see the <Output>
    Examples:
      | StartDate   | EndDate     | Output               |
      | 2021-07-31  | 2022-01-30  | Email address screen |
      | 2021-08-31  | 2022-02-28  | Email address screen |
      | 2021-09-01  | 2022-02-28  | Email address screen |
      | 2021-09-08  | 2022-03-07  | Email address screen |
      | 2021-09-07  | 2022-03-06  | Email address screen |

  @Regression
  Scenario Outline: Validate the returning user is redirected to 'Currently Not Eligible' screen when returned too soon to submit a new claim
  (Tested with validation date parameter = Application date is provided validation date)
    Given My previous claim details are <StartDate> and <EndDate>
    When I submit the page with validation date <validationDate>
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see the <Output>
    And I will see claim dates <startDate> and <endDate> of my previous claim
    And I will see earliest date I can submit the new claim is <EarliestDate>
    Examples:
      | StartDate  | EndDate    | startDate        | endDate           | Output                        | EarliestDate     | validationDate  |
      | 2021-03-31 | 2021-09-30 | 31 March 2021    | 30 September 2021 | Currently not eligible screen | 1 April 2022     | 2022-03-31      |
      | 2021-04-01 | 2021-09-30 | 1 April 2021     | 30 September 2021 | Currently not eligible screen | 1 April 2022     | 2022-03-31      |
      | 2021-04-02 | 2021-10-01 | 2 April 2021     | 1 October 2021    | Currently not eligible screen | 2 April 2022     | 2022-04-01      |
      | 2021-04-09 | 2021-10-08 | 9 April 2021     | 8 October 2021    | Currently not eligible screen | 9 April 2022     | 2022-04-08      |
      | 2021-04-30 | 2021-10-29 | 30 April 2021    | 29 October 2021   | Currently not eligible screen | 30 April 2022    | 2022-04-15      |
      | 2021-05-01 | 2021-10-31 | 1 May 2021       | 31 October 2021   | Currently not eligible screen | 1 May 2022       | 2022-04-15      |
      | 2021-05-02 | 2021-11-01 | 2 May 2021       | 1 November 2021   | Currently not eligible screen | 2 May 2022       | 2022-05-01      |
      | 2021-10-31 | 2022-04-30 | 31 October 2021  | 30 April 2022     | Currently not eligible screen | 1 November 2022  | 2022-10-31      |
      | 2021-11-01 | 2022-04-30 | 1 November 2021  | 30 April 2022     | Currently not eligible screen | 1 November 2022  | 2022-10-31      |
      | 2021-11-30 | 2022-05-29 | 30 November 2021 | 29 May 2022       | Currently not eligible screen | 30 November 2022 | 2022-10-15      |
      | 2021-12-01 | 2022-05-31 | 1 December 2021  | 31 May 2022       | Currently not eligible screen | 1 December 2022  | 2022-10-15      |
      | 2021-12-31 | 2022-06-30 | 31 December 2021 | 30 June 2022      | Currently not eligible screen | 1 January 2023   | 2022-12-31      |
      | 2021-01-01 | 2021-06-30 | 1 January 2021   | 30 June 2021      | Currently not eligible screen | 1 January 2022   | 2021-12-31      |
      | 2021-01-31 | 2021-07-30 | 31 January 2021  | 30 July 2021      | Currently not eligible screen | 31 January 2022  | 2022-01-30      |
      | 2021-02-01 | 2021-07-31 | 1 February 2021  | 31 July 2021      | Currently not eligible screen | 1 February 2022  | 2022-01-31      |
      | 2023-08-30 | 2024-02-29 | 30 August 2023   | 29 February 2024  | Currently not eligible screen | 1 September 2024 | 2024-08-15      |
      | 2023-08-31 | 2024-02-29 | 31 August 2023   | 29 February 2024  | Currently not eligible screen | 1 September 2024 | 2024-08-31      |
      | 2023-09-01 | 2024-02-29 | 1 September 2023 | 29 February 2024  | Currently not eligible screen | 1 September 2024 | 2024-08-30      |

  @Regression
  Scenario Outline: Validate the returning user is redirected to 'Email address' screen when eligible to submit a new claim
  (Tested with validation date parameter = Application date is provided validation date)
    Given My previous claim details are <StartDate> and <EndDate>
    When I submit the page with validation date <validationDate>
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see the <Output>

    Examples:
      | StartDate  | EndDate    | validationDate  | Output               |
      | 2021-03-31 | 2021-09-30 | 2022-04-01      | Email address screen |
      | 2021-03-31 | 2021-09-30 | 2022-04-03      | Email address screen |
      | 2021-03-31 | 2021-09-30 | 2022-04-05      | Email address screen |
      | 2021-03-31 | 2021-09-30 | 2022-04-06      | Email address screen |
      | 2021-04-30 | 2021-10-29 | 2022-04-30      | Email address screen |
      | 2021-05-01 | 2021-10-31 | 2022-05-01      | Email address screen |
      | 2021-05-02 | 2021-11-01 | 2022-05-02      | Email address screen |

  @Regression
  Scenario Outline: Validate the hyperlinks on 'Currently Not Eligible' screen when returned too soon to submit a new claim
    When I submit the page with validation date <validationDate>
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see the <Output>
    When I select the <Hyperlink>
    Then I will see the <output>

    Examples:
      | validationDate | Output                        | Hyperlink         | output                       |
      | 2022-04-15     | Currently not eligible screen | Service Name link | Start screen                 |
      | 2022-04-15     | Currently not eligible screen | GOV.UK link       | GOV UK screen                |
      | 2022-04-15     | Currently not eligible screen | Contact Us link   | Contact Us screen            |
      | 2022-04-15     | Currently not eligible screen | More Info link    | Pay for UK healthcare screen |
