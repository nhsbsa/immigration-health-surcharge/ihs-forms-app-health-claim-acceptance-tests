@CheckDependants @IHSRI-1640 @IHSRI-3445 @Regression

Feature: Validation of the Change links in Check Dependants Page to enable users to check and change the details of their dependants

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Check Dependants screen

  Scenario Outline: Validate the change links in Check Dependants screen when answers are not changed
    When I select <changeLink> in Check Dependants screen
    Then I will see the <screen>
    And my dependant's <answer> is pre-populated
    When I do not change the details and continue
    Then I will see the Check dependants screen
    And my dependant's <answer> is not changed
    Examples:
      | changeLink                | screen                   | answer        |
      | Change Name link          | Dependant details screen | name          |
      | Change Date of Birth link | Dependant details screen | date of birth |
      | Change IHS Number link    | Dependant details screen | IHS number    |

  Scenario Outline: Validate the change links in Check Dependants screen when answers are changed
    When I select <changeLink> in Check Dependants screen
    Then I will see the <screen>
    And my dependant's <answer> is pre-populated
    When I change the dependant's <answer> and continue
    Then I will see the Check dependants screen
    And my dependant's <answer> is changed
    Examples:
      | changeLink                | screen                   | answer        |
      | Change Name link          | Dependant details screen | name          |
      | Change Date of Birth link | Dependant details screen | date of birth |
      | Change IHS Number link    | Dependant details screen | IHS number    |

  Scenario Outline: Validate the errors when incorrect answers are entered using change links
    When I select <changeLink> in Check Dependants screen
    Then I will see the Dependant details screen
    And my dependant's <answer> is pre-populated
    When I change the dependant's answer as <invalid>
    Then I will see the <output>
    Examples:
      | changeLink                | answer        | invalid                  | output                                       |
      | Change Name link          | name          | blank name               | Blank dependant given name error             |
      | Change Name link          | name          | invalid name             | Invalid dependant family name error          |
      | Change Date of Birth link | date of birth | blank date of birth      | Blank dependant date of birth error          |
      | Change Date of Birth link | date of birth | invalid date of birth    | Invalid format dependant date of birth error |
      | Change IHS Number link    | IHS number    | blank IHS number         | Blank dependant ihs number error             |
      | Change IHS Number link    | IHS number    | invalid IHS number       | Invalid dependant ihs number error           |
      | Change IHS Number link    | IHS number    | invalid range IHS number | Invalid range dependant ihs number error     |

  Scenario Outline: Validate the functionality of the Back link from the change links - Selection on Add dependant page is ignored and details remain the same
    When I select <changeLink> in Check Dependants screen
    Then I will see the <screen>
    When I select the Back link
    Then I will see the <previousScreen>
    And I will see <radioButton1> selected
    When I update my selection as <radioButton2>
    Then I will see the Check dependants screen
    And my dependant's <answer> is not changed
    Examples:
      | changeLink                | screen                   | previousScreen            | radioButton1 | radioButton2 | answer        |
      | Change Name link          | Dependant details screen | Dependant question screen | Yes          | No           | name          |
      | Change Date of Birth link | Dependant details screen | Dependant question screen | Yes          | No           | date of birth |
      | Change IHS Number link    | Dependant details screen | Dependant question screen | Yes          | No           | IHS number    |
      | Change Name link          | Dependant details screen | Dependant question screen | Yes          | Yes          | name          |
      | Change Date of Birth link | Dependant details screen | Dependant question screen | Yes          | Yes          | date of birth |
      | Change IHS Number link    | Dependant details screen | Dependant question screen | Yes          | Yes          | IHS number    |

  Scenario Outline: Validate the hyperlinks on Check Dependants page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen        |
      | Service Name link | Start screen  |
      | GOV.UK link       | GOV UK screen |