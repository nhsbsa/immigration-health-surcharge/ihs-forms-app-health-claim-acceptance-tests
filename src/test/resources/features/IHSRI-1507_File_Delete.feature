@DeleteFile @IHSRI-1507 @Regression @JavaScriptDisabled

Feature: Validation of the delete uploaded evidence functionality in Evidence page to enable users to delete their uploaded evidence

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Upload Evidence screen

  @EndToEndDelete
  Scenario Outline: Validate the user is able to delete uploaded evidence when multiple evidence are added in Evidence upload screen
  When I upload the file <fileName> of format <fileFormat>
  And I view the uploaded evidence <fileName>
  And I do want to add another evidence
  And I upload the file <fileName1> of format <fileFormat1>
  And I select <deleteLink> in Evidence uploaded screen
  Then I will see the <output>
  And I will see delete success text for file <fileName1> of format <fileFormat1>
  And I do not want to add another evidence
  And My extra information is qatest
  And My answer remains same as on Evidence Uploaded screen
  And I confirm the declaration
  Then My claim is submitted successfully
    # And Only one evidence file is present in dynamics
  Examples:
    | fileName | fileFormat | fileName1    | fileFormat1 | deleteLink         | output                   |
    | payslip4 | .png       | Payslip-June | .png        | Delete File 2 link | Evidence Uploaded screen |

  Scenario Outline:Validate the user is able to delete uploaded evidence when single evidence is added in Evidence upload screen
    When I upload the file <fileName> of format <fileFormat>
    And I view the uploaded evidence <fileName>
    And I select <deleteLink> in Evidence uploaded screen
    Then I will see the <output>
    And I will see delete success text for file <fileName> of format <fileFormat>
    Examples:
      | fileName               | fileFormat | output                 | deleteLink         |
      | Sample_4               | .bmp       | Upload Evidence screen | Delete File 1 link |
      | sample_3               | .pdf       | Upload Evidence screen | Delete File 1 link |
      | Payslip-June           | .png       | Upload Evidence screen | Delete File 1 link |
      | Sample-jpg-image-1-9mb | .jpeg      | Upload Evidence screen | Delete File 1 link |
      | Large_size_jpg_file    | .jpg       | Upload Evidence screen | Delete File 1 link |
      | Large_size_png_file    | .png       | Upload Evidence screen | Delete File 1 link |

  @IHSRI-2790
  Scenario Outline:Validate the user is able to delete uploaded evidence when 'File Name' with maximum 130 characters is added in Evidence upload screen
    When I upload the file <fileName> of format <fileFormat>
    And I view the uploaded evidence <fileName>
    And I select <deleteLink> in Evidence uploaded screen
    Then I will see the <output>
    And I will see delete success text for file <fileName> of format <fileFormat>
    Examples:
      | fileName                                                                                                                           | fileFormat | output                 | deleteLink         |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       | Upload Evidence screen | Delete File 1 link |