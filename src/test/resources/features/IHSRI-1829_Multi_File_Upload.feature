@IHSRI-1829 @IHSRI-1987 @IHSRI-1988 @MultiFileUpload @Regression @NeedJavaScript

Feature: Validation of the Multi File Upload page on the IHS claim app to enable users to upload evidence to prove their eligibility

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Upload Evidence screen

  Scenario Outline: Validate the multi file upload functionality for all the acceptable 'File' formats
    When I upload multiple files <files>
    Then I will see upload success message for the files <files>
    And I click on continue
    Then I will see the <screen>
    Examples:
      | files                                                             | screen                   |
      | Sample_1.jpg,sample_2.png,sample_3.pdf,Sample_4.bmp,Sample_5.jpeg | Extra Information screen |

  Scenario Outline: Validate the multi file upload functionality for the non-acceptable 'File' formats
    When I upload multiple files <files>
    And I will be displayed <error> message for the files <files>
    Then I will be displayed <error> summary for the files <files>
    Examples:
      | files                                             | error                     |
      | Sample excel.xlsx,Sample text.rtf,Sample word.doc | Invalid file format error |

  @Retest-2450
  Scenario Outline: Validate the multi file upload functionality for all the acceptable 'File Name' formats
    When I upload multiple files <files>
    Then I will see upload success message for the files <files>
    And I click on continue
    Then I will see the <screen>
    Examples:
      | files                                                                                | screen                   |
      | Payslip-June.png,My Payslip_1.pdf,Payslip July.bmp,123456.pdf,1-payslip_May Name.jpg | Extra Information screen |

  Scenario Outline: Validate the multi file upload functionality for all the non-acceptable 'File Name' formats
    When I upload multiple files <files>
    And I will be displayed <error> message for the files <files>
    Then I will be displayed <error> summary for the files <files>
    Examples:
      | files                                             | error                   |
      | Payslip(Nov).png,Payslip.Jan.pdf,'My Payslip'.jpg | Invalid file name error |

  Scenario Outline: Validate the multi file upload functionality for all the acceptable 'File Size' formats
    When I upload multiple files <files>
    Then I will see upload success message for the files <files>
    And I click on continue
    Then I will see the <screen>
    Examples:
      | files                                                                                                                                                                                               | screen                   |
      | Sample-jpg-image-1-1mb.jpeg,Sample-jpg-image-1-2mb.jpeg,Sample-jpg-image-1-3mb.jpeg,Sample-jpg-image-1-5mb.jpeg,Sample-jpg-image-1-7mb.jpeg,Sample-jpg-image-1-8mb.jpeg,Sample-jpg-image-1-9mb.jpeg | Extra Information screen |

  Scenario Outline: Validate the multi file upload functionality for all the non-acceptable 'File Size' formats
    When I upload multiple files <files>
    And I will be displayed <error> message for the files <files>
    Then I will be displayed <error> summary for the files <files>
    Examples:
      | files                                                                                         | error               |
      | More_than_2MB.jpg,More_than_5MB.pdf,More_than_10MB.jpeg,More_than_30MB.png,More_than_50MB.bmp | Max file size error |

  @IHSRI-2016 @IHSRI-2744 @Retest-2760
  Scenario: Validate the multi file upload functionality for more than 75 files uploaded
    When I add 76 multi files to my claim application
    Then I will see the More than 75 files error for multi file
    Then I click Clear error message link on Upload Evidence screen
    And I click on continue
    Then I will see the Extra Information screen

  Scenario Outline: Validate the multi file upload functionality when acceptable and non-acceptable files are uploaded at once
    When I upload multiple files <files>
    Then I will be displayed Max file size error summary for the files More_than_2MB.jpg
    And I will be displayed Invalid file name error summary for the files Payslip(Nov).png
    And I will be displayed Invalid file format error summary for the files Sample word.doc
    And I will see upload success message for the files Payslip July.bmp,sample_3.pdf
    Examples:
      | files                                                                            |
      | Payslip July.bmp,More_than_2MB.jpg,Payslip(Nov).png,Sample word.doc,sample_3.pdf |

  Scenario Outline: Validate the hyperlinks on Upload Evidence page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen                  |
      | Back link         | Check Employment screen |
      | Service Name link | Start screen            |
      | GOV.UK link       | GOV UK screen           |

  @Retest-2465
  Scenario: Validate when user tries to continue without uploading any file
    When I do not upload multiple file
    Then I will see the No file selected error for multi file
    When I do upload multiple file
    Then I will see the Extra Information screen

  @Retest-2449
  Scenario: Validate that user should not be allowed to continue before the files are fully uploaded
    When I upload multiple files Sample_4.bmp,Sample_5.jpeg
    And I will see File Upload in progress
    And I click on continue
    Then I will see the Continue before evidence fully uploaded error
    When I will see upload success message for the files Sample_4.bmp,Sample_5.jpeg
    And I click on continue
    Then I will see the Extra Information screen

  @IHSRI-2798 @Retest-3086
  Scenario Outline: Validate the multi file upload functionality for the 'File Name' with maximum 130 characters
    When I upload multiple files <files>
    Then I will see upload success message for the files <files>
    And I click on continue
    Then I will see the Extra Information screen
    Examples:
      | files                                                                                                                                                                                                                                                                        |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1.png,File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890a.pdf |

  @IHSRI-2798 @Retest-3088
  Scenario Outline: Validate the multi file upload functionality for the 'File Name' with more than 130 characters
    When I upload multiple files <files>
    Then I will be displayed <error> summary for the files <files>
    And I will be displayed <error> message for the files <files>
     Examples:
      | files                                                                                                                                                                                                                                                                            | error                          |
      | File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-123.png,File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-abcd.pdf | More than 130 characters error |
