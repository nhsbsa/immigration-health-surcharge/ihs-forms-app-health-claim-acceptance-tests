@CheckYourAnswers @PersonalDetails @IHSRI-1702 @IHSRI-3446 @Regression

Feature: Validation of Check Your Answers functionality for Personal Details section on Health Claim web app to enable users to change their answers before submitting the claim application

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Check your answers screen - MultiFile

  Scenario Outline: Validate the change links on Check Your Answers page - do not change the answers and verify answer remains same
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                   | screen                  | answer           |
      | Change Name link             | Name screen             | Name             |
      | Change Date of birth link    | Date of birth screen    | Date of birth    |
      | Change NI number link        | NI Number screen        | NI Number        |
      | Change IHS number link       | IHS Number screen       | IHS Number       |
      | Change Email address link    | Email Address screen    | Email            |
      | Change Phone number link     | Phone number screen     | Phone Number     |
      | Change Claim start date link | Claim start date screen | Claim start date |
      | Change Reminder Email link   | Subscriptions screen    | Subscription     |

  Scenario Outline: Validate the change links on Check Your Answers page - change the answer and verify answers are updated
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    When I change my <answer> and continue
    Then I will see the Check Your Answers screen
    And I see the updated <answer> on Check Your Answers screen

    Examples:
      | changeLink                   | screen                  | answer           |
      | Change Name link             | Name screen             | Name             |
      | Change Date of birth link    | Date of birth screen    | Date of birth    |
      | Change NI number link        | NI Number screen        | NI Number        |
      | Change IHS number link       | IHS Number screen       | IHS Number       |
      | Change Email address link    | Email Address screen    | Email            |
      | Change Phone number link     | Phone number screen     | Phone Number     |
      | Change Claim start date link | Claim start date screen | Claim start date |
      | Change Reminder Email link   | Subscriptions screen    | Subscription     |

  @Retest-1851
  Scenario Outline: Validate the back link functionality on Check Your Answers page without changing the answers
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I select the Back link
    Then I will see the <previous screen>
    When I continue without changing my answer on <previous screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                   | screen                  | previous screen           | answer                               |
      | Change Date of birth link    | Date of birth screen    | Name screen               | Date of birth                        |
      | Change NI number link        | NI Number screen        | Date of birth screen      | NI Number                            |
      | Change IHS number link       | IHS Number screen       | NI Number screen          | IHS Number                           |
      | Change Email address link    | Email Address screen    | IHS Number screen         | Email                                |
      | Change Phone number link     | Phone number screen     | Email Address screen      | Phone Number                         |
      | Change Claim start date link | Claim start date screen | Dependant question screen | Claim start date                     |
      | Change Reminder Email link   | Subscriptions screen    | Claim start date screen   | Subscription                         |
      | Change Employer link         | Employer Name screen    | Subscriptions screen      | Employer Name with title and setting |

  @Retest-1851
  Scenario Outline: Validate the back link functionality on Check Your Answers page by changing the answers
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change my <answer> and do not continue
    And I select the Back link
    Then I will see the <previous screen>
    And I continue without changing my answer on <previous screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                   | screen                  | previous screen           | answer                               |
      | Change Date of birth link    | Date of birth screen    | Name screen               | Date of birth                        |
      | Change NI number link        | NI Number screen        | Date of birth screen      | NI Number                            |
      | Change IHS number link       | IHS Number screen       | NI Number screen          | IHS Number                           |
      | Change Email address link    | Email Address screen    | IHS Number screen         | Email                                |
      | Change Phone number link     | Phone number screen     | Email Address screen      | Phone Number                         |
      | Change Claim start date link | Claim start date screen | Dependant question screen | Claim start date                     |
      | Change Reminder Email link   | Subscriptions screen    | Claim start date screen   | Subscription                         |
      | Change Employer link         | Employer Name screen    | Subscriptions screen      | Employer Name with title and setting |

  Scenario Outline: Validate the errors when incorrect answers are entered using change links on Check Your Answers page
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change the answer to <invalidAnswer>
    Then I will see the <output>

    Examples:
      | changeLink                   | screen                  | invalidAnswer            | output                                | answer                               |
      | Change Name link             | Name screen             | Blank Given Name         | Blank given name error                | Name                                 |
      | Change Name link             | Name screen             | Blank Family Name        | Blank family name error               | Name                                 |
      | Change Name link             | Name screen             | Invalid Given Name       | Invalid given name error              | Name                                 |
      | Change Date of birth link    | Date of birth screen    | Blank Date of birth      | Blank date of birth error             | Date of birth                        |
      | Change Date of birth link    | Date of birth screen    | Invalid Date of birth    | Invalid date of birth error           | Date of birth                        |
      | Change NI number link        | NI Number screen        | Blank NI Number          | Blank nino error                      | NI Number                            |
      | Change NI number link        | NI Number screen        | Invalid NI Number        | Invalid nino error                    | NI Number                            |
      | Change IHS number link       | IHS Number screen       | Blank IHS Number         | Blank ihs number error                | IHS Number                           |
      | Change IHS number link       | IHS Number screen       | Invalid IHS Number       | Invalid ihs number error              | IHS Number                           |
      | Change IHS number link       | IHS Number screen       | Invalid range IHS Number | Invalid range ihs number error        | IHS Number                           |
      | Change Email address link    | Email Address screen    | Blank Email              | Blank email address error             | Email                                |
      | Change Email address link    | Email Address screen    | Invalid Email            | Invalid email address error           | Email                                |
      | Change Phone number link     | Phone number screen     | Invalid Phone Number     | Invalid phone number format error     | Phone Number                         |
      | Change Claim start date link | Claim start date screen | Blank Claim Start Date   | Blank claim start date error          | Claim start date                     |
      | Change Claim start date link | Claim start date screen | Invalid Claim Start Date | Invalid claim start date format error | Claim start date                     |
      | Change Employer link         | Employer Name screen    | Blank Employer Name      | Blank employer name error             | Employer Name with title and setting |
      | Change Employer link         | Employer Name screen    | Invalid Employer Name    | Invalid employer name error           | Employer Name with title and setting |

  Scenario Outline: Validate the hyperlinks on Check Your Answers page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                   |
      | Back link         | Extra Information screen |
      | Service Name link | Start screen             |
      | GOV.UK link       | GOV UK screen            |