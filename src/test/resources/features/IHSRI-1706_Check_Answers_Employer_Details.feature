@CheckYourAnswers @EmploymentDetails @IHSRI-1706 @Regression @Retest-2888 @IHSRI-3198

Feature: Validation of Check Your Answers functionality for Employment Details section on Health Claim web app to enable users to change their answers before submitting the claim application

  Background:
    Given I launch the IHS Health claim application

  Scenario: Validate the change links for Employer with title and setting on Check Your Answers page - do not change the answers and verify answer remains same
    And My details are captured until Check your answers screen - MultiFile
    When I select Change Employer link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name with title and setting is pre-populated
    When I continue without changing my answer on Employer Name screen
    Then I will see the Check Employment screen
    When I continue without changing my answer on Check Employment screen
    Then I will see the Check Your Answers screen
    When I select Change Job Title link on Check Your Answers page
    Then I will see the Job Title screen
    When I continue without changing my answer on Job Title screen
    Then I will see the Job Setting screen
    When I continue without changing my answer on Job Setting screen
    Then I will see the Check Employment screen
    When I continue without changing my answer on Check Employment screen
    Then I will see the Check Your Answers screen
    And My answer remains same as on Employer Name screen

  Scenario: Validate the change links for Employer on Check Your Answers page - change to the employer without title and verify employer details are updated
    And My details are captured until Check your answers screen - Employer with mandatory Title
    When I select Change Employer link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name is pre-populated
    When I change my Employer Name and continue
    And I am navigated to the Check Employment screen-select No and continue
    Then I will see the Check Your Answers screen
    And I see the updated Employer Name on Check Your Answers screen

  Scenario: Validate the change links for Employer on Check Your Answers page - change to the employer with title and verify employer details are updated
    And My details are captured until Check your answers screen - Employer with mandatory Title
    When I select Change Employer link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name is pre-populated
    When I change my Employer to Employer with Title and continue
    And I am navigated to the Check Employment screen-select No and continue
    Then I will see the Check Your Answers screen
    When I select Change Job Title link on Check Your Answers page
    Then I am navigated to the Job Title screen-enter Job title and continue
    And I am navigated to the Check Employment screen-select No and continue
    And I see the updated Employer Name with Title on Check Your Answers screen

  Scenario: Validate the change links for Employer on Check Your Answers page - change to the employer with title and setting and verify employer details are updated
    And My details are captured until Check your answers screen - Employer with mandatory Title
    When I select Change Employer link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name is pre-populated
    When I change my Employer to Employer with Title and Setting and continue
    And I am navigated to the Check Employment screen-select No and continue
    Then I will see the Check Your Answers screen
    When I select Change Job Title link on Check Your Answers page
    Then I am navigated to the Job Title screen-enter Job title with setting and continue
    And I am navigated to the Job Setting screen-select GP and continue
    When I am navigated to the Check Employment screen-select No and continue
    Then I will see the Check Your Answers screen
    And I see the updated Employer Name with Title and Setting on Check Your Answers screen

  Scenario: Validate the change links for Employer on Check Your Answers page - change the existing employer and add another employer
    And My details are captured until Check your answers screen - Employer with mandatory Title
    When I select Change Employer link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name is pre-populated
    When I change my Employer Name and continue
    And I am navigated to the Check Employment screen-select yes and continue
    Then I will see the Employer Name screen
    When I add 1 employer to my claim application
    And I am navigated to the Check Employment screen-select No and continue
    Then I will see the Check Your Answers screen
    And I see the updated Employer and newly added employer on Check Your Answers screen

  Scenario: Validate the change links for Employer on Check Your Answers page - delete employer
    And My details are captured until Check your answers screen - Employer with mandatory Title
    When I select Change Employer link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name is pre-populated
    When I continue without changing my answer on Employer Name screen
    Then I will see the Check Employment screen
    When I select Delete Employer1 link on Check Employments screen
    Then I will see the Employer Name screen
    And I will see the Employer deleted success message
    When I add 1 employer to my claim application
    And I am navigated to the Check Employment screen-select No and continue
    Then I will see the Check Your Answers screen
    And I see the updated Employer on Check Your Answers screen

  Scenario: Validate the change links for Employer on Check Your Answers page - delete with multiple employers
    And My details are captured until Check your answers screen - Multiple Employers
    When I select Change Employer 3 link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name with title is pre-populated
    When I change my Employer Name and continue
    Then I will see the Check Employment screen
    When I select Delete Employer2 link on Check Employments screen
    Then I will see the Employer2 deleted success message
    And I am navigated to the Check Employment screen-select No and continue
    Then I will see the Check Your Answers screen
    And I see the updated Employers on Check Your Answers screen

  Scenario: Validate the different change links for multiple Employers on Check Your Answers page
    And My details are captured until Check your answers screen - Multiple Employers
    When I select Change Employer link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name is pre-populated
    When I change my Employer1 and continue
    And I am navigated to the Check Employment screen-select No and continue
    Then I will see the Check Your Answers screen
    When I select Change Job Title link on Check Your Answers page
    Then I am navigated to the Job Title screen-enter Job title and continue
    And I am navigated to the Check Employment screen-select No and continue
    And I see the updated Employer1 on Check Your Answers screen
    When I select Change Employer 3 link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name with title is pre-populated
    When I change my Employer3 and continue
    Then I am navigated to the Check Employment screen-select No and continue
    And I will see the Check Your Answers screen
    When I select Change Job Title 3 link on Check Your Answers page
    Then I am navigated to the Job Title screen-enter Job title with setting and continue
    And I am navigated to the Job Setting screen-select GP and continue
    And I am navigated to the Check Employment screen-select No and continue
    And I will see the Check Your Answers screen
    And I see the updated Employer3 on Check Your Answers screen

  Scenario: Validate the change links on Check Your Answers page by changing multiple employers on Check Employment Details page
    And My details are captured until Check your answers screen - Multiple Employers
    When I select Change Employer link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name is pre-populated
    When I change my Employer1 and continue
    And I select Change Job Title link on Check Employments screen
    Then I am navigated to the Job Title screen-enter Job title with setting and continue
    And I am navigated to the Job Setting screen-select GP and continue
    Then I will see the Check Employment screen
    When I select Change Employer Name link2 in Check Employment Details screen
    Then I will see the Employer Name screen
    And I change my Employer2 and continue
    And I am navigated to the Check Employment screen-select No and continue
    Then I will see the Check Your Answers screen
    And I see the updated multiple employers on Check Your Answers screen

  @Retest-3279
  Scenario: Validate the change links on Check Your Answers page by changing Job Setting on Check Employment Details page
    And My details are captured until Check your answers screen - Multiple Employers
    When I select Change Job Setting 2 link on Check Your Answers page
    Then I am navigated to the Job Setting screen-select GP and continue
    And I am navigated to the Check Employment screen-select No and continue
    And I see the updated Employer2 on Check Your Answers screen

  @IHSRI-2320
  Scenario: Validate the change links for Employer with title and setting on Check Your Answers page - delete the employer and verify user is notified to add one employer
    And My details are captured until Check your answers screen - MultiFile
    When I select Change Employer link on Check Your Answers page
    Then I will see the Employer Name screen
    And My claimant's Employer Name with title and setting is pre-populated
    When I continue without changing my answer on Employer Name screen
    Then I will see the Check Employment screen
    When I select Delete Employer1 link on Check Employments screen
    Then I will see the Employer Name screen
    And I will see the Employer1 deleted success message
    When I select the Back link
    Then I will see the Subscriptions screen
    When I continue without changing my answer on Subscriptions screen
    Then I will see the Check Your Answers screen
    When I continue without changing my answer on Check Your Answers screen
    Then I will see the No Employer added error
