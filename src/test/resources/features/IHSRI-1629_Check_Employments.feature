@CheckEmployments @IHSRI-1629 @Regression @Retest-1810 @Retest-1833 @Retest-2888 @IHSRI-3197

Feature: Validation of the Change links in Check Employments Page to enable users to check and change the details of their employments

  Rule: Validate the Check Employment Details screen when Employer with Job Title and Settings is added
    Background:
      Given I launch the IHS Health claim application
      And My details are captured until Check Employment Details screen with Employer Job Title and Settings

    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Job Title and Settings when answers are not changed
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's <detail> is pre-populated
      When I do not change the employment's <detail> and continue
      Then I will be navigated to the <nextScreen>
      And My employment's <detail> is not changed
      Examples:
        | changeLink                | screen               | detail                               | nextScreen              |
        | Change Employer Name link | Employer Name screen | employer name with title and setting | Check Employment screen |
        | Change Job Title link     | Job Title screen     | job title with setting               | Job Setting screen      |
        | Change Job Setting link   | Job Setting screen   | job setting                          | Check Employment screen |

    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Job Title and Settings when answers are changed to another Employer with Job Title and Settings
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's <detail> is pre-populated
      When I change the employment's <detail> to another employer with Job Title and Job Setting and continue
      Then I will see the Check Employment screen
      And My employment's <detail> is changed to another employer with Job Title and Job Setting
      Examples:
        | changeLink                | screen               | detail                               |
        | Change Employer Name link | Employer Name screen | employer name with title and setting |
        | Change Job Title link     | Job Title screen     | job title with setting               |
        | Change Job Setting link   | Job Setting screen   | job setting                          |

    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Job Title and Settings when answers are changed to Employer with Job Title
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's <detail> is pre-populated
      When I change the employment's <detail> to employer with Job title and continue
      Then I will see the Check Employment screen
      And My employment's <detail> is changed to employer with Job title
      Examples:
        | changeLink                | screen               | detail                               |
        | Change Employer Name link | Employer Name screen | employer name with title and setting |
        | Change Job Title link     | Job Title screen     | job title with setting               |

    @Retest-2888
    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Job Title and Settings when Employer name is changed
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's employer name with title and setting is pre-populated
      When I update the <detail> and continue
      Then I will see the Check Employment screen
      And My employment's <detail> is updated
      Examples:
        | changeLink                | screen               | detail        |
        | Change Employer Name link | Employer Name screen | employer name |

    Scenario Outline: Validate the errors when incorrect answers are entered using change links in Check Employment Details screen
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's <detail> is pre-populated
      When I change the employment detail's as <invalid>
      Then I will see the <output>
      Examples:
        | changeLink                | screen               | detail                               | invalid               | output                        |
        | Change Employer Name link | Employer Name screen | employer name with title and setting | blank employer name   | Blank employer name error     |
        | Change Employer Name link | Employer Name screen | employer name with title and setting | invalid employer name | Invalid employer name error   |
        | Change Job Title link     | Job Title screen     | job title with setting               | blank job title       | Blank job title error         |
        | Change Job Title link     | Job Title screen     | job title with setting               | invalid job title     | Invalid job title error       |
        | Change Job Setting link   | Job Setting screen   | job setting                          | blank job setting     | Blank other job setting error |
        | Change Job Setting link   | Job Setting screen   | job setting                          | invalid job setting   | Invalid job setting error     |

    @Retest-1884
    Scenario Outline: Validate the functionality of the Back link from the change links when employment details are updated
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      When I select the Back link
      Then I will see the <previousScreen>
      And My employment's <detail> is pre-populated
      When I update the <detail> and continue
      Then I will see the Check Employment screen
      And My employment's <detail> is updated
      Examples:
        | changeLink                | screen               | previousScreen       | detail                               |
        | Change Employer Name link | Employer Name screen | Subscriptions screen | subscription                         |
        | Change Job Title link     | Job Title screen     | Employer Name screen | employer name with title and setting |
        | Change Job Setting link   | Job Setting screen   | Job Title screen     | job title with setting               |

  Rule: Validate the Check Employment Details screen when Employer with Job Title is added
    Background:
      Given I launch the IHS Health claim application
      And My details are captured until Check Employment Details screen with Employer Name and Job Title

    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Job Title when answers are not changed
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's <detail> is pre-populated
      When I do not change the employment's <detail> and continue
      Then I will be navigated to the <nextScreen>
      And My employment's <detail> is not changed
      Examples:
        | changeLink                | screen               | detail                   | nextScreen              |
        | Change Employer Name link | Employer Name screen | employer name with title | Check Employment screen |
        | Change Job Title link     | Job Title screen     | job title                | Check Employment screen |

    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Job Title when answers are changed to another Employer with Job Title and Setting
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's <detail> is pre-populated
      When I change the employment's <detail> to another employer with Job Title and Job Setting and continue
      Then I will see the Check Employment screen
      And My employment's <detail> is changed to another employer with Job Title and Job Setting
      Examples:
        | changeLink                | screen               | detail                   |
        | Change Employer Name link | Employer Name screen | employer name with title |
        | Change Job Title link     | Job Title screen     | job title                |

    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Job Title when answers are changed to Employer with Job Title
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's <detail> is pre-populated
      When I change the employment's <detail> to employer with Job title and continue
      Then I will see the Check Employment screen
      And My employment's <detail> is changed to employer with Job title
      Examples:
        | changeLink                | screen               | detail                   |
        | Change Employer Name link | Employer Name screen | employer name with title |
        | Change Job Title link     | Job Title screen     | job title                |

    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Job Title when Employer name is changed
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's employer name with title is pre-populated
      When I update the <detail> and continue
      Then I will see the Check Employment screen
      And My employment's <detail> is updated
      Examples:
        | changeLink                | screen               | detail        |
        | Change Employer Name link | Employer Name screen | employer name |

  Rule: Validate the Check Employment Details screen when Employer with mandatory Job Title is added
    Background:
      Given I launch the IHS Health claim application
      And My details are captured until Check Employment Details screen with Employer Name and mandatory Job Title

    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Name when answers are not changed
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's <detail> is pre-populated
      When I do not change the employment's <detail> and continue
      Then I will see the Check Employment screen
      And My employment's <detail> is not changed
      Examples:
        | changeLink                | screen               | detail        |
        | Change Employer Name link | Employer Name screen | employer name |

    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Name when answers are changed to another Employer with Job Title and Setting
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's <detail> is pre-populated
      When I change the employment's <detail> to another employer with Job Title and Job Setting and continue
      Then I will see the Check Employment screen
      And My employment's <detail> is changed to another employer with Job Title and Job Setting
      Examples:
        | changeLink                | screen               | detail        |
        | Change Employer Name link | Employer Name screen | employer name |

    Scenario Outline: Validate the change links in Check Employment Details screen with Employer Name when answers are changed to Employer with Job Title
      When I select <changeLink> in Check Employment Details screen
      Then I will see the <screen>
      And My employment's <detail> is pre-populated
      When I change the employment's <detail> to employer with Job title and continue
      Then I will see the Check Employment screen
      And My employment's <detail> is changed to employer with Job title
      Examples:
        | changeLink                | screen               | detail        |
        | Change Employer Name link | Employer Name screen | employer name |

    Scenario Outline: Validate the hyperlinks on Check Employments page
      When I select the <hyperlink>
      Then I will see the <screen>
      Examples:
        | hyperlink         | screen        |
        | Service Name link | Start screen  |
        | GOV.UK link       | GOV UK screen |