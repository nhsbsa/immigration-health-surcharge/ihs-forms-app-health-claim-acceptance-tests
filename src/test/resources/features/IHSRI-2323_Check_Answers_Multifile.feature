@IHSRI-2323 @MultiFileUpload @CheckYourAnswers @Regression @NeedJavaScript

Feature: Validation of Check Your Answers functionality for Multi file on Health Claim web app to enable users to check and change their answers before submitting the claim application

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Check your answers screen - MultiFile

  Scenario Outline: Validate the change links on Check Your Answers page Evidence details - do not change the answers and verify answer remains same
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check your answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink              | screen                 | answer     |
      | Change Files Added link | Upload Evidence screen | Multi File |

  Scenario Outline: Validate the files added change link on Check Your Answers page Evidence details - change the answer and verify answers are updated
    When I select Change Files Added link on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I upload multiple files <files>
    Then I will see upload success message for the new files
    And I click on continue
    Then I will see the Check your answers screen
    And I see the updated <answer> on Check Your Answers screen

    Examples:
      | answer     | screen                 | files                     |
      | Multi File | Upload Evidence screen | sample_3.pdf,Sample_4.bmp |

  @Retest-2451
  Scenario Outline: Validate the back link from Extra Information change link on Check Your Answers page Evidence details without changing the answer
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I select the Back link
    Then I will see the <output>
    When I continue without changing my answer on <output>
    Then I will see the Check your answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                    | screen                   | answer            | output                 |
      | Change Extra Information Link | Extra Information screen | Extra Information | Upload Evidence screen |

  @Retest-2451
  Scenario Outline: Validate the back link from Extra Information change link on Check Your Answers page Evidence details by changing the answer
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I select the Back link
    Then I will see the <output>
    When I upload multiple files <files>
    Then I will see upload success message for the new files
    And I click on continue
    Then I will see the Check your answers screen
    And I see the updated Multi File on Check Your Answers screen

    Examples:
      | changeLink                    | screen                   | answer            | output                 | files                     |
      | Change Extra Information Link | Extra Information screen | Extra Information | Upload Evidence screen | sample_3.pdf,Sample_4.bmp |

  Scenario Outline: Validate the errors when incorrect file is added using change links on Check Your Answers page Evidence details
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I upload multiple files <files>
    Then I will be displayed Max file size error summary for the files More_than_2MB.jpg
    And I will be displayed Invalid file name error summary for the files Payslip(Nov).png
    And I will be displayed Invalid file format error summary for the files Sample excel.xlsx

    Examples:
      | changeLink              | answer     | screen                 | files                                                |
      | Change Files Added link | Multi File | Upload Evidence screen | Sample excel.xlsx,Payslip(Nov).png,More_than_2MB.jpg |

  @Retest-2077
  Scenario: Validate the back link from File change link on Check Your Answers page by deleting the files
    When I select Change Files Added link on Check Your Answers page
    And I will see the Upload Evidence screen
    And My claimant's Multi File is pre-populated
    And I click delete link for Sample_1.jpg on Upload Evidence screen
    And I will see delete success message for file Sample_1.jpg
    And I click delete link for sample_2.png on Upload Evidence screen
    And I will see delete success message for file sample_2.png
    And I select the Back link
    And I will see the Check Employment screen
    And I continue without changing my answer on Check Employment screen
    Then I will see the Upload Evidence screen
    And I upload multiple files 123456.pdf,Payslip July.bmp
    Then I will see upload success message for the files 123456.pdf,Payslip July.bmp
    And I click on continue
    Then I will see the Check your answers screen
    And I see the updated Files on Check Your Answers screen

  @Retest-2077
  Scenario: Validate the back link from File change link on Check Your Answers page by not deleting the files
    When I select Change Files Added link on Check Your Answers page
    And I will see the Upload Evidence screen
    And My claimant's Multi File is pre-populated
    And I select the Back link
    And I will see the Check Employment screen
    And I continue without changing my answer on Check Employment screen
    Then I will see the Check your answers screen
    And My answer remains same as on Upload Evidence screen

  @Retest-2077
  Scenario: Validate that user is not able to submit the claim without evidences
    When I select Change Files Added link on Check Your Answers page
    And I will see the Upload Evidence screen
    And My claimant's Multi File is pre-populated
    And I click delete link for Sample_1.jpg on Upload Evidence screen
    And I will see delete success message for file Sample_1.jpg
    And I click delete link for sample_2.png on Upload Evidence screen
    And I will see delete success message for file sample_2.png
    And I select the Back link
    And I will see the Check Employment screen
    And I select Change Employer Name link in Check Employment Details screen
    And I will see the Employer Name screen
    And I select the Back link
    And I continue without changing my answer on Subscriptions screen
    And I will see the Check your answers screen
    And I confirm the answers
    Then I will see the Submission without evidence error