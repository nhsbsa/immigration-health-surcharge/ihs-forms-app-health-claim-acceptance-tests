@ReturnClaimStartDate @IHSRI-1794

Feature: Validation of the claim start date for return journey on the IHS Health claim app when user has previously claimed IHS reimbursement

  Scenario Outline: Validate a new user is redirected to 'Subscriptions screen' screen when valid date is entered on 'Claim Start Date' screen
  (Tested without validation date parameter = Application date is today's date)
    Given I launch the IHS Health claim application
    And My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My national insurance number is <NINumber>
    When My IHS number is <IHSNumber>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    And I do not have dependant to add
    And I will see earliest date my new claim period can start is <earliestStartDate>
    When I enter claim start date <day>, <month>, <year>
    Then I will see the <output>

    Examples:
      | GivenName | FamilyName | Day | Month | Year | NINumber  | IHSNumber    | Email                      | PhoneNumber | earliestStartDate | day | month | year | output               |
      | Kathrine  | Lockman    | 01  | 01    | 1999 | SZ711911C | IHS123456789 | nhsbsa.ihs-testing@nhs.net | 72345678901 | 31 March 2020     | 31  | 03    | 2020 | Subscriptions screen |
      | Oliver    | Hilll      | 01  | 01    | 1989 | SZ735007C | IHS234567891 | nhsbsa.ihs-testing@nhs.net | 72345678911 | 31 March 2020     | 05  | 03    | 2022 | Subscriptions screen |

  @Regression
  Scenario Outline: Validate the returning user is redirected to 'Subscriptions screen' screen when valid date is entered on 'Claim Start Date' screen
  (Tested without validation date parameter = Application date is today's date)
    Given My previous claim details are <StartDate> and <EndDate>
    When I launch the IHS Health claim application
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see previous claim dates <startDate> and <endDate> on the claim start date screen
    And I will see earliest date I can start the new claim is <EarliestDate>
    When I enter claim start date <day>, <month>, <year>
    Then I will see the <output>

    Examples:
      | StartDate  | EndDate    | startDate        | endDate           | EarliestDate   | day | month | year | output               |
      | 2021-03-31 | 2021-09-30 | 31 March 2021    | 30 September 2021 | 1 October 2021 | 01  | 10    | 2021 | Subscriptions screen |
      | 2021-01-01 | 2021-06-30 | 1 January 2021   | 30 June 2021      | 1 July 2021    | 01  | 07    | 2021 | Subscriptions screen |
      | 2021-09-05 | 2022-03-04 | 5 September 2021 | 4 March 2022      | 5 March 2022   | 05  | 03    | 2022 | Subscriptions screen |
      | 2021-08-30 | 2022-02-28 | 30 August 2021   | 28 February 2022  | 1 March 2022   | 01  | 03    | 2022 | Subscriptions screen |

  @Regression
  Scenario Outline: Validate the new user is redirected to 'Subscriptions screen' screen when valid date is entered on 'Claim Start Date' screen
  (Tested with validation date parameter = Application date is provided validation date)
    Given I submit the page with validation date <validationDate>
    And My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My national insurance number is <NINumber>
    When My IHS number is <IHSNumber>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    And I do not have dependant to add
    And I will see earliest date my new claim period can start is <earliestStartDate>
    When I enter claim start date <day>, <month>, <year>
    Then I will see the <output>

    Examples:
      | GivenName | FamilyName | Day | Month | Year | NINumber  | IHSNumber    | Email                      | PhoneNumber | validationDate | earliestStartDate | day | month | year | output               |
      | Kathrine  | Lockman    | 01  | 01    | 1999 | SZ711111C | IHS123456789 | nhsbsa.ihs-testing@nhs.net | 72345678901 | 2023-01-01     | 31 March 2020     | 01  | 07    | 2022 | Subscriptions screen |
      | Oliver    | Hilll      | 01  | 01    | 1989 | SZ735107C | IHS234567891 | nhsbsa.ihs-testing@nhs.net | 72345678911 | 2021-08-28     | 31 March 2020     | 28  | 02    | 2021 | Subscriptions screen |
      | Keith     | Jobs       | 01  | 09    | 2000 | SA735107C | IHS234767891 | nhsbsa.ihs-testing@nhs.net | 72345678911 | 2022-09-30     | 31 March 2020     | 30  | 03    | 2021 | Subscriptions screen |
      | John      | Jobs       | 01  | 09    | 2000 | SA735107C | IHS234767891 | nhsbsa.ihs-testing@nhs.net | 78345678911 | 2023-11-01     | 31 March 2020     | 31  | 10    | 2022 | Subscriptions screen |

  @Regression
  Scenario Outline: Validate the returning user is redirected to 'Subscriptions screen' screen when valid date is entered on 'Claim Start Date' screen
  (Tested with validation date parameter = Application date is provided validation date)
    Given My previous claim details are <StartDate> and <EndDate>
    When I submit the page with validation date <validationDate>
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see previous claim dates <startDate> and <endDate> on the claim start date screen
    And I will see earliest date I can start the new claim is <EarliestDate>
    When I enter claim start date <day>, <month>, <year>
    Then I will see the <output>

    Examples:
      | StartDate  | EndDate    | startDate         | endDate           | validationDate | EarliestDate     | day | month | year | output               |
      | 2022-04-30 | 2022-10-29 | 30 April 2022     | 29 October 2022   | 2023-04-30     | 30 October 2022  | 30  | 10    | 2022 | Subscriptions screen |
      | 2023-05-01 | 2023-10-31 | 1 May 2023        | 31 October 2023   | 2024-05-01     | 1 November 2023  | 01  | 11    | 2023 | Subscriptions screen |
      | 2021-05-02 | 2021-11-01 | 2 May 2021        | 1 November 2021   | 2022-05-04     | 2 November 2021  | 03  | 11    | 2021 | Subscriptions screen |
      | 2022-05-31 | 2022-11-30 | 31 May 2022       | 30 November 2022  | 2023-06-01     | 1 December 2022  | 01  | 12    | 2022 | Subscriptions screen |
      | 2023-06-30 | 2023-12-29 | 30 June 2023      | 29 December 2023  | 2024-06-30     | 30 December 2023 | 30  | 12    | 2023 | Subscriptions screen |
      | 2022-07-01 | 2022-12-31 | 1 July 2022       | 31 December 2022  | 2023-07-01     | 1 January 2023   | 01  | 01    | 2023 | Subscriptions screen |
      | 2023-07-31 | 2024-01-30 | 31 July 2023      | 30 January 2024   | 2024-07-31     | 31 January 2024  | 31  | 01    | 2024 | Subscriptions screen |
      | 2024-08-01 | 2025-01-31 | 1 August 2024     | 31 January 2025   | 2025-08-01     | 1 February 2025  | 01  | 02    | 2025 | Subscriptions screen |
      | 2021-08-29 | 2022-02-28 | 29 August 2021    | 28 February 2022  | 2022-09-01     | 1 March 2022     | 01  | 03    | 2022 | Subscriptions screen |
      | 2023-08-30 | 2024-02-28 | 30 August 2023    | 28 February 2024  | 2024-09-02     | 29 February 2024 | 29  | 02    | 2024 | Subscriptions screen |
      | 2021-08-31 | 2022-02-28 | 31 August 2021    | 28 February 2022  | 2022-09-03     | 1 March 2022     | 01  | 03    | 2022 | Subscriptions screen |
      | 2022-09-01 | 2023-02-28 | 1 September 2022  | 28 February 2023  | 2023-09-04     | 1 March 2023     | 01  | 03    | 2023 | Subscriptions screen |
      | 2021-09-30 | 2022-03-29 | 30 September 2021 | 29 March 2022     | 2022-09-30     | 30 March 2022    | 30  | 03    | 2022 | Subscriptions screen |
      | 2022-10-01 | 2023-03-31 | 1 October 2022    | 31 March 2023     | 2023-10-01     | 1 April 2023     | 01  | 04    | 2023 | Subscriptions screen |
      | 2021-10-31 | 2022-04-30 | 31 October 2021   | 30 April 2022     | 2022-11-01     | 1 May 2022       | 01  | 05    | 2022 | Subscriptions screen |
      | 2020-11-01 | 2021-04-30 | 1 November 2020   | 30 April 2021     | 2021-11-02     | 1 May 2021       | 02  | 05    | 2021 | Subscriptions screen |
      | 2021-11-30 | 2022-05-29 | 30 November 2021  | 29 May 2022       | 2022-11-30     | 30 May 2022      | 30  | 05    | 2022 | Subscriptions screen |
      | 2021-12-01 | 2022-05-31 | 1 December 2021   | 31 May 2022       | 2022-12-01     | 1 June 2022      | 01  | 06    | 2022 | Subscriptions screen |
      | 2022-12-31 | 2023-06-30 | 31 December 2022  | 30 June 2023      | 2024-01-01     | 1 July 2023      | 01  | 07    | 2023 | Subscriptions screen |
      | 2021-01-01 | 2021-06-30 | 1 January 2021    | 30 June 2021      | 2022-01-03     | 1 July 2021      | 03  | 07    | 2021 | Subscriptions screen |
      | 2021-01-31 | 2021-07-30 | 31 January 2021   | 30 July 2021      | 2022-01-31     | 31 July 2021     | 31  | 07    | 2021 | Subscriptions screen |
      | 2021-02-01 | 2021-07-31 | 1 February 2021   | 31 July 2021      | 2022-02-01     | 1 August 2021    | 01  | 08    | 2021 | Subscriptions screen |
      | 2021-08-31 | 2022-02-28 | 31 August 2021    | 28 February 2022  | 2022-09-03     | 1 March 2022     | 03  | 03    | 2022 | Subscriptions screen |
      | 2022-04-01 | 2022-09-30 | 1 April 2022      | 30 September 2022 | 2023-04-01     | 1 October 2022   | 01  | 10    | 2022 | Subscriptions screen |
      | 2023-08-30 | 2024-02-29 | 30 August 2023    | 29 February 2024  | 2024-09-01     | 1 March 2024     | 01  | 03    | 2024 | Subscriptions screen |
      | 2023-08-31 | 2024-02-29 | 31 August 2023    | 29 February 2024  | 2024-09-02     | 1 March 2024     | 02  | 03    | 2024 | Subscriptions screen |
      | 2023-09-01 | 2024-02-29 | 1 September 2023  | 29 February 2024  | 2024-09-03     | 1 March 2024     | 03  | 03    | 2024 | Subscriptions screen |

  Scenario Outline: Validate the error for a new user in 'Claim Start Date' screen when the claim start date is less than six months
  (Tested without validation date parameter = Application date is today's date)
    Given I launch the IHS Health claim application
    And My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My national insurance number is <NINumber>
    When My IHS number is <IHSNumber>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    And I do not have dependant to add
    And I will see earliest date my new claim period can start is <earliestStartDate>
    When I enter claim start date <day>, <month>, <year>
    Then I will see the <output>

    Examples:
      | GivenName | FamilyName | Day | Month | Year | NINumber  | IHSNumber    | Email                      | PhoneNumber | earliestStartDate | day | month | year | output                        |
      | Kathrine  | Dawis      | 01  | 01    | 1999 | SZ711910C | IHS123156789 | nhsbsa.ihs-testing@nhs.net | 72345678901 | 31 March 2020     | 01  | 10    | 2022 | Six months before today error |
      | Oliver    | Walker     | 01  | 01    | 1989 | SZ735017C | IHS231567891 | nhsbsa.ihs-testing@nhs.net | 72345678911 | 31 March 2020     | 30  | 11    | 2022 | Six months before today error |

  @Regression
  Scenario Outline: Validate the error for a new user in 'Claim Start Date' screen when the claim start date is less than six months
  (Tested with validation date parameter = Application date is provided validation date)
    Given I launch the IHS Health claim application
    And I submit the page with validation date <validationDate>
    And My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My national insurance number is <NINumber>
    When My IHS number is <IHSNumber>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    And I do not have dependant to add
    And I will see earliest date my new claim period can start is <earliestStartDate>
    When I enter claim start date <day>, <month>, <year>
    Then I will see the <output>

    Examples:
      | validationDate | GivenName | FamilyName | Day | Month | Year | NINumber  | IHSNumber    | Email                      | PhoneNumber | earliestStartDate | day | month | year | output                        |
      | 2023-01-31     | Kathrine  | Wills      | 01  | 01    | 1999 | SZ111911C | IHS133456789 | nhsbsa.ihs-testing@nhs.net | 72345678901 | 31 March 2020     | 01  | 08    | 2022 | Six months before today error |
      | 2022-12-28     | Oliver    | Williams   | 01  | 01    | 1989 | SZ135007C | IHS134567891 | nhsbsa.ihs-testing@nhs.net | 72345678911 | 31 March 2020     | 29  | 06    | 2022 | Six months before today error |
      | 2022-09-29     | Keith     | Jobs       | 01  | 09    | 2000 | SA735007C | IHS334767891 | nhsbsa.ihs-testing@nhs.net | 72345678911 | 31 March 2020     | 01  | 04    | 2022 | Six months before today error |

  @Regression
  Scenario Outline: Validate the error for a returning user in 'Claim Start Date' screen when the claim start date overlaps previous claim dates
  (Tested without validation date parameter = Application date is today's date)
    Given My previous claim details are <StartDate> and <EndDate>
    When I launch the IHS Health claim application
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see previous claim dates <startDate> and <endDate> on the claim start date screen
    And I will see earliest date I can start the new claim is <EarliestDate>
    When I enter claim start date <day>, <month>, <year>
    Then I will see the <output>

    Examples:
      | StartDate  | EndDate    | startDate      | endDate           | EarliestDate   | day | month | year | output              |
      | 2021-03-31 | 2021-09-30 | 31 March 2021  | 30 September 2021 | 1 October 2021 | 29  | 09    | 2021 | Claim overlap error |
      | 2021-07-01 | 2021-12-31 | 1 July 2021    | 31 December 2021  | 1 January 2022 | 30  | 12    | 2021 | Claim overlap error |
      | 2021-04-02 | 2021-10-01 | 2 April 2021   | 1 October 2021    | 2 October 2021 | 01  | 10    | 2021 | Claim overlap error |
      | 2021-08-31 | 2022-02-28 | 31 August 2021 | 28 February 2022  | 1 March 2022   | 28  | 02    | 2022 | Claim overlap error |

  @Regression
  Scenario Outline: Validate the error for a returning user in 'Claim Start Date' screen when the claim start date overlaps previous claim dates
  (Tested with validation date parameter = Application date is provided validation date)
    Given My previous claim details are <StartDate> and <EndDate>
    When I submit the page with validation date <validationDate>
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see previous claim dates <startDate> and <endDate> on the claim start date screen
    And I will see earliest date I can start the new claim is <EarliestDate>
    When I enter claim start date <day>, <month>, <year>
    Then I will see the <output>

    Examples:
      | StartDate  | EndDate    | startDate        | endDate           | validationDate | EarliestDate     | day | month | year | output              |
      | 2022-03-31 | 2022-09-30 | 31 March 2022    | 30 September 2022 | 2023-10-01     | 1 October 2022   | 29  | 09    | 2022 | Claim overlap error |
      | 2022-03-01 | 2022-08-31 | 1 March 2022     | 31 August 2022    | 2023-03-01     | 1 September 2022 | 30  | 08    | 2022 | Claim overlap error |
      | 2022-04-30 | 2022-10-29 | 30 April 2022    | 29 October 2022   | 2023-04-30     | 30 October 2022  | 29  | 10    | 2022 | Claim overlap error |
      | 2021-12-31 | 2022-06-30 | 31 December 2021 | 30 June 2022      | 2023-01-01     | 1 July 2022      | 30  | 06    | 2022 | Claim overlap error |
      | 2022-05-02 | 2022-11-01 | 2 May 2022       | 1 November 2022   | 2023-05-04     | 2 November 2022  | 30  | 10    | 2022 | Claim overlap error |
      | 2021-05-31 | 2021-11-30 | 31 May 2021      | 30 November 2021  | 2022-06-01     | 1 December 2021  | 30  | 11    | 2021 | Claim overlap error |
      | 2022-06-30 | 2022-12-29 | 30 June 2022     | 29 December 2022  | 2023-06-30     | 30 December 2022 | 28  | 12    | 2022 | Claim overlap error |
      | 2022-07-01 | 2022-12-31 | 1 July 2022      | 31 December 2022  | 2023-07-01     | 1 January 2023   | 31  | 12    | 2022 | Claim overlap error |
      | 2023-07-31 | 2024-01-30 | 31 July 2023     | 30 January 2024   | 2024-07-31     | 31 January 2024  | 29  | 01    | 2024 | Claim overlap error |
      | 2021-08-01 | 2022-01-31 | 1 August 2021    | 31 January 2022   | 2022-08-01     | 1 February 2022  | 31  | 01    | 2022 | Claim overlap error |
      | 2022-08-29 | 2023-02-28 | 29 August 2022   | 28 February 2023  | 2023-09-01     | 1 March 2023     | 28  | 02    | 2023 | Claim overlap error |
      | 2023-08-30 | 2024-02-28 | 30 August 2023   | 28 February 2024  | 2024-09-02     | 29 February 2024 | 27  | 02    | 2024 | Claim overlap error |
      | 2021-08-31 | 2022-02-28 | 31 August 2021   | 28 February 2022  | 2022-09-03     | 1 March 2022     | 06  | 02    | 2022 | Claim overlap error |
      | 2021-09-01 | 2022-02-28 | 1 September 2021 | 28 February 2022  | 2022-09-04     | 1 March 2022     | 01  | 09    | 2021 | Claim overlap error |

  Scenario Outline: Validate the error for a returning user in 'Claim Start Date' screen when the claim start date is less than six months
  (Tested without validation date parameter = Application date is today's date)
    Given My previous claim details are <StartDate> and <EndDate>
    When I launch the IHS Health claim application
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see previous claim dates <startDate> and <endDate> on the claim start date screen
    And I will see earliest date I can start the new claim is <EarliestDate>
    When I enter claim start date <day>, <month>, <year>
    Then I will see the <output>

    Examples:
      | StartDate  | EndDate    | startDate        | endDate           | EarliestDate   | day | month | year | output                        |
      | 2021-03-31 | 2021-09-30 | 31 March 2021    | 30 September 2021 | 1 October 2021 | 31  | 03    | 2023 | Six months before today error |
      | 2021-01-06 | 2021-07-05 | 6 January 2021   | 05 July 2021      | 6 July 2021    | 04  | 12    | 2022 | Six months before today error |
      | 2021-09-01 | 2022-02-28 | 1 September 2021 | 28 February 2022  | 1 March 2022   | 01  | 04    | 2023 | Six months before today error |
      | 2021-02-28 | 2021-08-27 | 28 February 2021 | 27 August 2021    | 28 August 2021 | 28  | 11    | 2022 | Six months before today error |

  @Regression
  Scenario Outline: Validate the error for a returning user in 'Claim Start Date' screen when the claim start date is less than six months
  (Tested with validation date parameter = Application date is provided validation date)
    Given My previous claim details are <StartDate> and <EndDate>
    When I submit the page with validation date <validationDate>
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see previous claim dates <startDate> and <endDate> on the claim start date screen
    And I will see earliest date I can start the new claim is <EarliestDate>
    When I enter claim start date <day>, <month>, <year>
    Then I will see the <output>

    Examples:
      | StartDate  | EndDate    | startDate        | endDate           | validationDate | EarliestDate     | day | month | year | output                        |
      | 2022-03-31 | 2022-09-30 | 31 March 2022    | 30 September 2022 | 2023-04-01     | 1 October 2022   | 01  | 01    | 2023 | Six months before today error |
      | 2022-03-01 | 2022-08-31 | 1 March 2022     | 31 August 2022    | 2023-03-01     | 1 September 2022 | 01  | 03    | 2023 | Six months before today error |
      | 2021-04-30 | 2021-10-29 | 30 April 2021    | 29 October 2021   | 2022-04-30     | 30 October 2021  | 29  | 04    | 2022 | Six months before today error |
      | 2023-05-01 | 2023-10-31 | 1 May 2023       | 31 October 2023   | 2024-05-01     | 1 November 2023  | 11  | 11    | 2023 | Six months before today error |
      | 2021-05-02 | 2021-11-01 | 2 May 2021       | 1 November 2021   | 2022-05-02     | 2 November 2021  | 01  | 05    | 2022 | Six months before today error |
      | 2022-05-31 | 2022-11-30 | 31 May 2022      | 30 November 2022  | 2023-06-01     | 1 December 2022  | 31  | 05    | 2023 | Six months before today error |
      | 2020-06-30 | 2020-12-29 | 30 June 2020     | 29 December 2020  | 2021-06-30     | 30 December 2020 | 29  | 06    | 2021 | Six months before today error |
      | 2022-07-01 | 2022-12-31 | 1 July 2022      | 31 December 2022  | 2023-07-01     | 1 January 2023   | 01  | 07    | 2023 | Six months before today error |
      | 2020-07-31 | 2021-01-30 | 31 July 2020     | 30 January 2021   | 2021-08-01     | 31 January 2021  | 31  | 07    | 2021 | Six months before today error |
      | 2021-08-01 | 2022-01-31 | 1 August 2021    | 31 January 2022   | 2022-08-01     | 1 February 2022  | 01  | 07    | 2022 | Six months before today error |
      | 2021-08-29 | 2022-02-28 | 29 August 2021   | 28 February 2022  | 2022-09-01     | 1 March 2022     | 01  | 09    | 2022 | Six months before today error |
      | 2022-08-31 | 2023-02-28 | 31 August 2022   | 28 February 2023  | 2023-09-05     | 1 March 2023     | 02  | 09    | 2023 | Six months before today error |
      | 2022-10-01 | 2023-03-31 | 01 October 2022  | 31 March 2023     | 2024-10-01     | 1 April 2023     | 30  | 09    | 2024 | Six months before today error |
      | 2021-09-01 | 2022-02-28 | 1 September 2021 | 28 February 2022  | 2022-09-01     | 1 March 2022     | 31  | 08    | 2022 | Six months before today error |