@Name @IHSRI-1609 @Regression

Feature: Validation of the Name Page on the IHS claim app to enable users to enter their given name and family name for the claim application

  Background:
    Given I launch the IHS Health claim application

  @Retest-1736 @Retest-1737 @Retest-1738 @Retest-3500
  Scenario Outline: Validate the enter name functionality for positive and negative scenarios
    When My firstname is <GivenName> and surname is <FamilyName>
    Then I will see the <output>
    Examples:
      | GivenName            | FamilyName          | output                    |
      #positive tests
      | firstName            | lastName            | Date of birth screen      |
      | firstName            | O'hara              | Date of birth screen      |
      | firstName-middleName | lastName            | Date of birth screen      |
      | FirstName middleName | LastName            | Date of birth screen      |
      | firstName            | middleName lastName | Date of birth screen      |
      | A O' John            | D' O-Wills          | Date of birth screen      |
      | A' D'anil-John       | D-Wilson            | Date of birth screen      |
      | O'-John              | È-Wilson            | Date of birth screen      |
      | O'John               | E'wilson            | Date of birth screen      |
      | È Romà John          | È Wilson            | Date of birth screen      |
      | D' John              | D                   | Date of birth screen      |
      | D' John              | D' Wilson           | Date of birth screen      |
      | d                    | john                | Date of birth screen      |
      | d o                  | john                | Date of birth screen      |
      | JOHN                 | D                   | Date of birth screen      |
      | j                    | t                   | Date of birth screen      |
      | A                    | à t                 | Date of birth screen      |
      | J T                  | T S                 | Date of birth screen      |
      #negative tests
      |                      | lastName            | Blank given name error    |
      | firstName            |                     | Blank family name error   |
      | (first name)         | last name           | Invalid given name error  |
      | first name 1         | last name           | Invalid given name error  |
      | first name           | "last name"         | Invalid family name error |
      | first name           | last name 123       | Invalid family name error |
      | '-                   | last name           | Invalid given name error  |
      | first name           | -                   | Invalid family name error |
      | 1                    | last name           | Invalid given name error  |
      | first name           | 123                 | Invalid family name error |
      | first:%name           | last name           | Invalid given name error  |
      | first name           | last:%name           | Invalid family name error |

  Scenario Outline: Validate user is not allowed to enter name more than 50 characters
    When I attempt to enter <count> characters name
    Then I am restricted to enter <maximum> characters name
    And I will see the <output>
    Examples:
      | count | maximum | output               |
      | 51    | 50      | Date of birth screen |

  Scenario Outline: Validate the hyperlinks on Name page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen        |
      | Service Name link | Start screen  |
      | GOV.UK link       | GOV UK screen |