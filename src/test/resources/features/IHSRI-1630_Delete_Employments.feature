@DeleteEmployer @IHSRI-1630 @Regression @Retest-1811 @Retest-1814

Feature: Validation of the employment delete functionality in Check Employments Page to enable users to delete their employments

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Employer Name screen

  Scenario Outline: Validate the delete employer functionality on Check Employment Details page for Single Employer
    When I add <count> employer to my claim application
    And I select <link> on Check Employments screen
    Then I will see the <screen>
    And I will see the <output>
    When I select the Back link
    Then I will see the <previousScreen>
    Examples:
      | count | link                  | screen                  | output                            | previousScreen       |
      | 1     | Delete Employer1 link | Employer Name screen    | Employer1 deleted success message | Subscriptions screen |

  Scenario Outline: Validate the delete employer functionality on Check Employment Details page for Multiple Employers
    When I add <count> employer to my claim application
    And I select <link> on Check Employments screen
    Then I will see the <screen>
    And I will see the <output>
    Examples:
      | count | link                  | screen                  | output                            |
      | 3     | Delete Employer2 link | Check Employment screen | Employer2 deleted success message |