@Compatibility
Feature: Validation of the IHS Health Claim end-to-end functionality in various SauceLabs devices to meet the compatibility test criteria

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Dependant Question screen

  Scenario Outline: Verify that the user is able to submit a claim with Dependant in the IHS Health claim application
    And I do have dependant to add
    And My dependant's firstname is <depGivenName> and surname is <depFamilyName>
    And My dependant's date of birth is <depDay> <depMonth> <depYear>
    And My dependant's IHS number is <depIHSNumber>
    And I do not have another dependant to add
    When My claim start date is <claimDay> <claimMonth> <claimYear>
    And I do choose to subscribe
    And My employer name is <employerName>
    And My job title is <jobTitle>
    And My job setting is <jobSetting>
    And I Do not have more employers to add
    And I upload multiple payslips in SauceLabs
    And I will see upload success message for the files <files>
    And I click on continue
    And My entered extra information is <information>
    And I do upload the file on Extra Information screen
    And I do not want to upload another extra evidence
    And I confirm the declaration
    And I validate the output

    Examples:
      | claimDay | claimMonth | claimYear | depGivenName | depFamilyName | depDay | depMonth | depYear | depIHSNumber | employerName                    | jobTitle         | jobSetting | files                     | information                        |
      | 02       | 04         | 2020      | Oliver       | Smith         | 01     | 05       | 1999    | IHS876545678 | NHS Business Services Authority | Family Therapist | Hospital   | Sample_1.jpg,sample_2.png | Compatibility Testing in SauceLabs |

  Scenario Outline: Verify that the user is able to submit a claim without Dependant in the IHS Health claim application
    And I do not have dependant to add
    When My claim start date is <claimDay> <claimMonth> <claimYear>
    And I do choose to subscribe
    And My employer name is <employerName>
    And My job title is <jobTitle>
    And I Do not have more employers to add
    And I upload multiple payslips in SauceLabs
    And I will see upload success message for the files <files>
    And I click on continue
    And My extra information is <information>
    And I confirm the declaration
    And I validate the output

    Examples:
      | claimDay | claimMonth | claimYear | employerName                    | jobTitle         | files                     | information                        |
      | 02       | 04         | 2020      | NHS Business Services Authority | medical director | Sample_1.jpg,sample_2.png | Compatibility Testing in SauceLabs |