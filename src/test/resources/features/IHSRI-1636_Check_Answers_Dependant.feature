@IHSRI-1636 @CheckYourAnswers @DependantSection @Regression

Feature: Validation of Check Your Answers functionality for Dependant section on Health Claim web app to enable users to check and change their answers before submitting the claim application

  Background:
    Given I launch the IHS Health claim application

  Scenario Outline: Validate the change link on Check Your Answers page Dependant section - do not change the answers and verify answer remains same
    And My details are captured until Check your answers screen with Dependant - Multi File
    When I select Change Dependant 1 link on Check Your Answers page
    Then I will see the Dependant details screen
    And my dependant's <answer> is pre-populated
    When I continue without changing my answer on Dependant details screen
    Then I will see the Check dependants screen
    When I continue without changing my answer on Check dependants screen
    Then I will see the Check Your Answers screen
    And My dependant <answer> remains same as on Dependant details screen

    Examples:
      | answer        |
      | name          |
      | date of birth |
      | IHS number    |

  Scenario Outline: Validate the change link on Check Your Answers page Dependant section for single dependant - change the answer and verify answers are updated
    And My details are captured until Check your answers screen with Dependant - Multi File
    When I select Change Dependant 1 link on Check Your Answers page
    Then I will see the Dependant details screen
    And my dependant's <answer> is pre-populated
    When I change the dependant <answer> and continue
    Then I will see the Check dependants screen
    When I continue without changing my answer on Check dependants screen
    Then I will see the Check Your Answers screen
    And I see the updated <details> on Check Your Answers screen

    Examples:
      | answer        | details                 |
      | name          | Dependant Name          |
      | date of birth | Dependant Date of Birth |
      | IHS number    | Dependant IHS number    |

  Scenario Outline: Validate the change link functionality without changing answer on Check Your Answers page Dependant section - verify No value is shown when no dependant is added
    And My details are captured until Check your answers screen - MultiFile
    And I see No value under dependant section for dependants
    When I select Change Dependant Question link on Check Your Answers page
    Then I will see the <screen>
    And My dependant answer is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | screen                    |
      | Dependant question screen |

  Scenario Outline: Validate the change link functionality on Check Your Answers page Dependant section - change the answer and verify dependant gets added
    And My details are captured until Check your answers screen - MultiFile
    When I select Change Dependant Question link on Check Your Answers page
    Then I will see the Dependant question screen
    And My dependant answer is pre-populated
    When I change my Dependant Details and continue
    Then I will see the Dependant details screen
    When I add a dependant and continue
    Then I will see the Check dependants screen
    When I do not have another dependant to add
    Then I will see the Check Your Answers screen
    And I will see updated dependant <answer> on Check your answers page

    Examples:
      | answer        |
      | name          |
      | date of birth |
      | IHS number    |

  Scenario Outline: Validate the errors when incorrect answers are entered using change links on Check Your Answers page Dependant section
    And My details are captured until Check your answers screen with Dependant - Multi File
    When I select Change Dependant 1 link on Check Your Answers page
    Then I will see the Dependant details screen
    And my dependant's <answer> is pre-populated
    When I change the dependant's answer as <invalid>
    Then I will see the <output>

    Examples:
      | answer        | invalid               | output                                       |
      | name          | blank name            | Blank dependant given name error             |
      | name          | invalid name          | Invalid dependant family name error          |
      | date of birth | blank date of birth   | Blank dependant date of birth error          |
      | date of birth | invalid date of birth | Invalid format dependant date of birth error |
      | IHS number    | blank IHS number      | Blank dependant ihs number error             |
      | IHS number    | invalid IHS number    | Invalid dependant ihs number error           |

  Scenario Outline: Validate the functionality of the Back link from the change links on Check Your Answers page Dependant section - Selection on Add dependant page is ignored and details remain the same
    And My details are captured until Check your answers screen with Dependant - Multi File
    When I select Change Dependant 1 link on Check Your Answers page
    Then I will see the Dependant details screen
    When I select the Back link
    Then I will see the Dependant question screen
    And I will see <radioButton1> selected
    When I update my selection as <radioButton2>
    Then I will see the Check dependants screen
    And my dependant's <answer> is not changed
    When I continue without changing my answer on Check dependants screen
    Then I will see the Check Your Answers screen
    And My dependant <answer> remains same as on Dependant details screen

    Examples:
      | radioButton1 | radioButton2 | answer        |
      | Yes          | No           | name          |
      | Yes          | No           | date of birth |
      | Yes          | No           | IHS number    |

  Scenario Outline: Validate the functionality of change links on Check Your Answers page Dependant section - add another dependant and verify dependant is added
    And My details are captured until Check your answers screen with Dependant - Multi File
    When I select Change Dependant 1 link on Check Your Answers page
    Then I will see the Dependant details screen
    When I continue without changing my answer on Dependant details screen
    Then I will see the Check dependants screen
    When I add another dependant and continue
    Then I will see the Check dependants screen
    When I do not have another dependant to add
    Then I will see the Check Your Answers screen
    And I will see updated dependant <answer> on Check your answers page
    And I select Change Dependant 2 link on Check Your Answers page
    When I change the dependant <answer> and continue
    Then I will see the Check dependants screen
    When I continue without changing my answer on Check dependants screen
    Then I will see the Check Your Answers screen
    And I will see updated dependant <details> on Check your answers page

    Examples:
      | answer        | details                 |
      | name          | updated name 2          |
      | date of birth | updated date of birth 2 |
      | IHS number    | updated IHS number 2    |

  Scenario Outline: Validate the functionality of change links on Check Your Answers page for multiple dependants - delete dependant and verify dependant is deleted
    And My details are captured until Check Your Answer screen with multiple dependants
    When I select Change Dependant 2 link on Check Your Answers page
    Then I will see the <screen>
    When I continue without changing my answer on <screen>
    Then I will see the <output1>
    When I select Delete Dependant 2 link in Check Dependants screen
    Then I will see the <output1>
    When I do not have another dependant to add
    Then I will see the <output2>
    And I will see Dependant 2 is deleted on Check your answers page
    When I select Change Dependant 1 link on Check Your Answers page
    Then I will see the <screen>
    When I continue without changing my answer on <screen>
    Then I will see the <output1>
    When I select Delete Dependant 1 link in Check Dependants screen
    Then I will see the Dependant question screen
    When I do not have dependant to add
    Then I will see the <output2>
    And I will see Dependant 1 is deleted on Check your answers page
    And I see No value under dependant section for dependants

    Examples:
      | screen                   | output1                 | output2                  |
      | Dependant details screen | Check dependants screen | Check Your Answer screen |