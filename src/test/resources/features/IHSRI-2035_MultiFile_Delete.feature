@MultiFileDelete @IHSRI-2035 @IHSRI-150 @IHSRI-2351 @Regression @NeedJavaScript

Feature: Validation of the multi file delete functionality on Upload Evidence screen to enable users to delete their uploaded evidences

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Upload Evidence screen

  Scenario Outline: Validate that user is able to delete the unsuccessful files
    When I upload multiple files <files>
    And I will be displayed Max file size error summary for the files More_than_2MB.jpg
    And I will be displayed Invalid file name error summary for the files Payslip(Nov).png
    And I will be displayed Invalid file format error summary for the files Sample word.doc
    Then I click delete link for More_than_2MB.jpg on Upload Evidence screen
    And I will see delete success message for file More_than_2MB.jpg
    And I will be displayed Invalid file name error summary for the files Payslip(Nov).png
    And I will be displayed Invalid file format error summary for the files Sample word.doc
    And I click delete link for Payslip(Nov).png on Upload Evidence screen
    And I will see delete success message for file Payslip(Nov).png
    And I will be displayed Invalid file format error summary for the files Sample word.doc
    And I click delete link for Sample word.doc on Upload Evidence screen
    And I will see delete success message for file Sample word.doc
    And I will not see any unsuccessful files in the Files added section
    Examples:
      | files                                              |
      | More_than_2MB.jpg,Payslip(Nov).png,Sample word.doc |

  Scenario Outline: Validate that user is able to delete all the unsuccessful files at once
    When I upload multiple files <files>
    And I will be displayed Max file size error summary for the files More_than_2MB.jpg
    And I will be displayed Invalid file name error summary for the files Payslip(Nov).png
    And I will be displayed Invalid file format error summary for the files Sample word.doc
    Then I will see upload success message for the files Payslip July.bmp,sample_3.pdf
    And I click Delete all unsuccessful files link on Upload Evidence screen
    Then I will see delete success message
    And I will not see any unsuccessful files in the Files added section
    Examples:
      | files                                                                            |
      | Payslip July.bmp,More_than_2MB.jpg,Payslip(Nov).png,Sample word.doc,sample_3.pdf |

  @EndToEndMultiFileDelete
  Scenario Outline: Validate that the deleted files are not submitted as part of the claim
    When I upload multiple files <files>
    And I will see upload success message for the files <files>
    And I click delete link for sample_3.pdf on Upload Evidence screen
    And I will see delete success message for file sample_3.pdf
    And I click delete link for Sample_4.bmp on Upload Evidence screen
    And I will see delete success message for file Sample_4.bmp
    And I click delete link for Sample_5.jpeg on Upload Evidence screen
    And I will see delete success message for file Sample_5.jpeg
    And I click on continue
    And My extra information is qatest
    And My answer remains same as on Upload Evidence screen
    And I confirm the declaration
    Then My claim is submitted successfully
    # And Only two evidence files are present in dynamics
  Examples:
    | files                                                             |
    | Sample_1.jpg,sample_2.png,sample_3.pdf,Sample_4.bmp,Sample_5.jpeg |

    Scenario Outline: Validate that Delete File Success Message disappears when an Error Message is displayed
      When I upload multiple files <files>
      And I will see upload success message for the files <files>
      And I click delete link for Sample_1.jpg on Upload Evidence screen
      And I will see delete success message for file Sample_1.jpg
      And I upload multiple files More_than_2MB.jpg,Payslip(Nov).png
      And I will be displayed Max file size error summary for the files More_than_2MB.jpg
      And I will be displayed Invalid file name error summary for the files Payslip(Nov).png
      Then I will not see the delete success message
      Examples:
        | files                     |
        | Sample_1.jpg,sample_2.png |

  @IHSRI-2798
  Scenario Outline: Validate the multi file delete functionality for the 'File Name' with maximum 130 characters
    When I upload multiple files <files>
    And I will see upload success message for the files <files>
    And I click delete link for File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1.png on Upload Evidence screen
    And I will see delete success message for file File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1.png
    And I click delete link for File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890a.pdf on Upload Evidence screen
    And I will see delete success message for file File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890a.pdf

    Examples:
      | files                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1.png,File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890a.pdf |

  @IHSRI-2798
  Scenario Outline: Validate the multi file delete functionality for the 'File Name' with more than 130 characters
    When I upload multiple files <files>
    Then I will be displayed <error> message for the files <files>
    And I click delete link for File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-123.png on Upload Evidence screen
    Then I will see delete success message for file File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-123.png
    And I click delete link for File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-abcd.pdf on Upload Evidence screen
    Then I will see delete success message for file File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-abcd.pdf
    Examples:
      | files                                                                                                                                                                                                                                                                            | error                          |
      | File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-123.png,File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-abcd.pdf | More than 130 characters error |
