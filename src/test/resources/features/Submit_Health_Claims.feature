@EndToEnd @SubmitClaim @Regression @Smoke

Feature: Validation of the end to end claim submissions in the IHS Health claim application

  @JavaScriptDisabled
  Scenario Outline: Verify that the user is able to submit a new health claim with 1 dependant, 1 employment and 1 file
    Given I launch the IHS Health claim application
    When My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My national insurance number is <NINumber>
    And My IHS number is <IHSNumber>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    When I do have dependant to add
    And My dependant's firstname is <depGivenName> and surname is <depFamilyName>
    And My dependant's date of birth is <depDay> <depMonth> <depYear>
    And My dependant's IHS number is <depIHSNumber>
    And I do not have another dependant to add
    When My claim start date is <ClaimDay> <ClaimMonth> <ClaimYear>
    And I do choose to subscribe
    And My employer name is <employerName>
    And My job title is <jobTitle>
    And I Do not have more employers to add
    And I upload the file <fileName> of format <fileFormat>
    And I do not want to add another evidence
    And My extra information is <information>
    And I confirm the declaration
    Then My claim is submitted successfully
    Examples:
      | GivenName | FamilyName | Day | Month | Year | NINumber  | IHSNumber    | Email                     | PhoneNumber | ClaimDay | ClaimMonth | ClaimYear | depGivenName | depFamilyName | depDay | depMonth | depYear | depIHSNumber | employerName                    | jobTitle         | fileName | fileFormat | information                                     |
      | Sally     | O'hara     | 07  | 04    | 1988 | SZ098765C | IHS098765432 | ihs-testing@nhsbsa.nhs.uk | 07123456789 | 20       | 04         | 2020      | Abigail      | O'hara        | 01     | 05       | 1999    | IHS876545678 | NHS Business Services Authority | medical director | sample_3 | .pdf       | Claim with 1 Dependant, 1 Employment and 1 File |

  @JavaScriptDisabled
  Scenario Outline: Verify that the user is able to submit a new health claim with NO dependants, 2 employments and 5 files
    Given I launch the IHS Health claim application
    When My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My national insurance number is <NINumber>
    And My IHS number is <IHSNumber>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    When I do not have dependant to add
    When My claim start date is <ClaimDay> <ClaimMonth> <ClaimYear>
    And I do not choose to subscribe
    And I add 2 employer to my claim application
    And I Do not have more employers to add
    And I add 5 same files to my claim application
    And I do not want to add another evidence
    And My entered extra information is <information>
    And I do upload the file on Extra Information screen
    And I do not want to upload another extra evidence
    And I confirm the declaration
    Then My claim is submitted successfully
    Examples:
      | GivenName | FamilyName | Day | Month | Year | NINumber      | IHSNumber    | Email                     | PhoneNumber | ClaimDay | ClaimMonth | ClaimYear | information                                       |
      | Nick S    | M D        | 7   | 9     | 1988 | sz 09 87 65 c | ihs098765432 | ihs-testing@nhsbsa.nhs.uk | 07123456789 | 20       | 04         | 2020      | Claim with 0 dependants, 2 Employment and 5 files |

  @NeedJavaScript
  Scenario Outline: Verify that the user is able to submit a new health claim with 10 dependants, 10 employments and 75 multi files
    Given I launch the IHS Health claim application
    When My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My national insurance number is <NINumber>
    And My IHS number is <IHSNumber>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    And I add 10 dependants to my claim application
    And I do not have another dependant to add
    When My claim start date is <ClaimDay> <ClaimMonth> <ClaimYear>
    And I do choose to subscribe
    And I add 10 employer to my claim application
    And I Do not have more employers to add
    And I add 75 multi files to my claim application
    And I click on continue
    And My extra information is <information>
    And I confirm the declaration
    Then My claim is submitted successfully
    Examples:
      | GivenName | FamilyName | Day | Month | Year | NINumber      | IHSNumber    | Email                     | PhoneNumber | ClaimDay | ClaimMonth | ClaimYear | information                                           |
      | Maximum   | test data  | 07  | 04    | 1988 | SZ 09 87 65 C | IHS098765432 | ihs-testing@nhsbsa.nhs.uk | 07123456789 | 20       | 04         | 2020      | Claim with 10 Dependants, 10 Employments and 75 Files |

  @NeedJavaScript
  Scenario Outline: Verify that the user is able to submit a new health claim with 10 dependants, 10 employments, 75 multi files and 3 additional evidences
    Given I launch the IHS Health claim application
    When My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My national insurance number is <NINumber>
    And My IHS number is <IHSNumber>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    And I add 10 dependants to my claim application
    And I do not have another dependant to add
    When My claim start date is <ClaimDay> <ClaimMonth> <ClaimYear>
    And I do choose to subscribe
    And I add 10 employer to my claim application
    And I Do not have more employers to add
    And I upload 75 new multi files to my claim application
    And I click on continue
    And My entered extra information is <information>
    And I upload 3 different extra files to my claim application
    And I do not want to upload another extra evidence
    And I confirm the declaration
    Then My claim is submitted successfully
    Examples:
      | GivenName | FamilyName | Day | Month | Year | NINumber      | IHSNumber    | Email                     | PhoneNumber | ClaimDay | ClaimMonth | ClaimYear | information                                           |
      | Maximum   | test data  | 07  | 04    | 1988 | SZ 09 87 65 C | IHS098765432 | ihs-testing@nhsbsa.nhs.uk | 07123456789 | 20       | 04         | 2020      | Claim with 10 Dependants, 10 Employments and 78 Files |