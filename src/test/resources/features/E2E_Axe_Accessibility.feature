@Accessibility

Feature: Validation of the accessibility of IHS Health claim app using Axe tool

  Background:
    Given I launch the IHS Health claim application

  Scenario Outline: Validate the end to end accessibility of IHS health claim app
    When My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My national insurance number is <NINumber>
    And My IHS number is <IHSNumber>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    When I do have dependant to add
    And My dependant's firstname is <depGivenName> and surname is <depFamilyName>
    And My dependant's date of birth is <depDay> <depMonth> <depYear>
    And My dependant's IHS number is <depIHSNumber>
    And I do not have another dependant to add
    When My claim start date is <ClaimDay> <ClaimMonth> <ClaimYear>
    And I do choose to subscribe
    And My employer name is <employerName>
    And My job title is <jobTitle>
    And My job setting is <jobSetting>
    And I Do not have more employers to add
    And I upload multiple payslips in SauceLabs
    And I will see upload success message for the files <files>
    And I click on continue
    And My entered extra information is <information>
    And I do upload the file on Extra Information screen
    And I do not want to upload another extra evidence
    And I confirm the answers
    And I submit the claim application
    Then My claim is submitted successfully

    Examples:
      | GivenName | FamilyName | Day | Month | Year | NINumber  | IHSNumber    | Email                     | PhoneNumber | ClaimDay | ClaimMonth | ClaimYear | depGivenName | depFamilyName | depDay | depMonth | depYear | depIHSNumber | employerName            | jobTitle         | jobSetting | files                     | information                          |
      | D' John   | D' Wilson  | 30  | 12    | 1983 | SH123456D | IHS123456789 | ihs-testing@nhsbsa.nhs.uk | 09876543245 | 20       | 04         | 2020      | Abigail      | O'hara        | 01     | 05       | 1999    | IHS876545678 | Horwich Dental Practice | Family Therapist | Hospital   | Sample_1.jpg,sample_2.png | Accessibility Testing using Axe tool |

  Scenario Outline: Validate the accessibility of Currently Not Eligible screen
    When My previous claim details are <StartDate> and <EndDate>
    And I enter my first name and family name
    And I enter my date of birth
    And I enter my National Insurance Number
    And I enter my IHS number
    Then I will see the <Output>
    Examples:
      | StartDate  | EndDate    | Output                        |
      | 2022-06-09 | 2022-12-08 | Currently not eligible screen |