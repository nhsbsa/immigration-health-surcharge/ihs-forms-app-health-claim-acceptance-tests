@NationalInsurance @IHSRI-1611 @Regression

Feature: Validation of National Insurance Page on the IHS claim app to enable users to enter their national insurance number for the claim application

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until NI Number screen

  @Retest-1750 @Retest-1751 @Retest-1752
  Scenario Outline: Validate the enter national insurance number functionality for positive and negative scenarios
    When My national insurance number is <NINumber>
    Then I will see the <output>
    Examples:
      | NINumber      | output             |
      #Valid NINO
      | SH123456D     | IHS Number screen  |
      | Sz654567C     | IHS Number screen  |
      | LA654567B     | IHS Number screen  |
      | mn654567a     | IHS Number screen  |
      | sz 123456 D   | IHS Number screen  |
      | SZ 123456D    | IHS Number screen  |
      | SZ123456 D    | IHS Number screen  |
      | HB 12 34 56 A | IHS Number screen  |
      | NN 12 34 56 B | IHS Number screen  |
      #invalid format
      |               | Blank nino error   |
      | SA1234C       | Invalid nino error |
      | SA12345C      | Invalid nino error |
      | SH123456      | Invalid nino error |
      | SH1234567     | Invalid nino error |
      | SH1234567C    | Invalid nino error |
      | SH-123456-C   | Invalid nino error |
      | 'SH123456C'   | Invalid nino error |
      | SH:12:34:56:C | Invalid nino error |
      #invalid prefixes
      | S1234567C     | Invalid nino error |
      | 1H123456C     | Invalid nino error |
      | 123456789     | Invalid nino error |
      #invalid numbers
      | SHABC456C     | Invalid nino error |
      | SZ654ABCD     | Invalid nino error |
      | SZ65456AB     | Invalid nino error |
      #invalid suffix
      | SZ654567E     | Invalid nino error |
      | SZ654567F     | Invalid nino error |
      | SZ654567H     | Invalid nino error |
      | SZ654567N     | Invalid nino error |
      | SZ654567Z     | Invalid nino error |
      #D,F,I,Q,U and V are not used as the first letter of a NINO prefix
      | DH123456A     | Invalid nino error |
      | FA123456B     | Invalid nino error |
      | IN123456C     | Invalid nino error |
      | QM123456D     | Invalid nino error |
      | UB123456A     | Invalid nino error |
      | VC123456B     | Invalid nino error |
      #D,F,I,Q,U,V and O are not used as the second letter of a NINO prefix
      | AD123456C     | Invalid nino error |
      | BF123456D     | Invalid nino error |
      | CI123456A     | Invalid nino error |
      | EQ123456B     | Invalid nino error |
      | GU123456C     | Invalid nino error |
      | HV123456D     | Invalid nino error |
      | JO123456C     | Invalid nino error |
      #Prefixes BG,GB,KN,NK,NT,TN and ZZ are not used
      | TN123456A     | Invalid nino error |
      | NT123456C     | Invalid nino error |
      | GB123456B     | Invalid nino error |
      | BG123456A     | Invalid nino error |
      | NK123456C     | Invalid nino error |
      | KN123456B     | Invalid nino error |
      | ZZ123456D     | Invalid nino error |

  Scenario Outline: Validate the hyperlinks on NI Number page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen               |
      | Back link         | Date of birth screen |
      | Service Name link | Start screen         |
      | GOV.UK link       | GOV UK screen        |