@ValidationDate @IHSRI-1650 @Regression

Feature: Test the date of birth validation relatively to validation date

  Background:
    Given I launch the IHS Health claim application

  Scenario Outline: Validate that date of birth is validated relatively to the validation date - feature flag is enabled and correct validation date is specified
    When I submit the page with validation date <validationDate>
    And My firstname is sample and surname is test
    And My date of birth is <Day> <Month> <Year>
    Then I will see the <output>

    Examples:
      | validationDate | Day | Month | Year | output                 |
      #past validation date
      | 2022-08-15     | 15  | 08    | 2006 | NI Number screen       |
      | 2022-08-15     | 14  | 08    | 2006 | NI Number screen       |
      | 2022-08-15     | 16  | 08    | 2006 | Minimum 16 years error |
      | 2022-08-15     | 17  | 08    | 2006 | Minimum 16 years error |
      #future validation date
      | 2023-03-05     | 5   | 3     | 2007 | NI Number screen       |
      | 2023-03-05     | 4   | 3     | 2007 | NI Number screen       |
      | 2023-03-05     | 6   | 3     | 2007 | Minimum 16 years error |
      | 2023-03-05     | 7   | 3     | 2007 | Minimum 16 years error |

  Scenario Outline: Validate that date of birth is validated relatively to today's date - feature flag is enabled and validation date is NOT specified
    When My firstname is sample and surname is test
    And My date of birth is <Day> <Month> <Year>
    Then I will see the <output>

    Examples:
      | Day | Month | Year | output                 |
      | 07  | 07    | 2006 | NI Number screen       |
      | 8   | 8     | 2006 | NI Number screen       |
      | 5   | 12    | 2023 | Minimum 16 years error |
      | 12  | 8     | 2014 | Minimum 16 years error |

  Scenario Outline: Validate that date of birth is validated relatively to today's date - feature flag is enabled and incorrect validation date is specified
    When I submit the page with validation date <validationDate>
    And My firstname is sample and surname is test
    And My date of birth is <Day> <Month> <Year>
    Then I will see the <output>

    Examples:
      | validationDate | Day | Month | Year | output                 |
      | 05-03-2023     | 5   | 3     | 2011 | Minimum 16 years error |
#      | 23-03-05       | 5   | 3     | 2007 | Minimum 16 years error |
#      | 2023-3-5       | 5   | 3     | 2007 | Minimum 16 years error |
#      | 2023-02-30     | 15  | 2     | 2007 | Minimum 16 years error |
#      | 0000-00-00     | 15  | 2     | 2007 | Minimum 16 years error |