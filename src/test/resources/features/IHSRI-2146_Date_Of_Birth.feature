@DateOfBirth @IHSRI-2146 @Regression

Feature: Validation of the Date of Birth Page on the IHS claim app to enable users to enter their date of birth for the claim application

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Date of birth screen

  @Retest-2180 @Retest-2187 @Retest-2209
  Scenario Outline: Validate the enter date of birth functionality for positive and negative scenarios
    When My date of birth is <Day> <Month> <Year>
    Then I will see the <output>
    Examples:
      | Day | Month | Year | output                                |
      #positive tests
      | 30  | 12    | 1983 | NI Number screen                      |
      | 1   | 1     | 1989 | NI Number screen                      |
      | 02  | 8     | 1990 | NI Number screen                      |
      | 31  | 12    | 1999 | NI Number screen                      |
      | 29  | 02    | 2000 | NI Number screen                      |
      | 16  | 12    | 2006 | NI Number screen                      |
      #negative tests
      |     |       |      | Blank date of birth error             |
      |     | 8     | 2004 | Blank day error                       |
      | 31  |       | 2000 | Blank month error                     |
      | 13  | 12    |      | Blank year error                      |
      |     | 08    | 99   | Blank day error                       |
      | 07  |       | 89   | Blank month error                     |
      |     |       | 89   | Incomplete date of birth error        |
      |     | 7     |      | Incomplete date of birth error        |
      |     |       | 1989 | Incomplete date of birth error        |
      | 19  |       |      | Incomplete date of birth error        |
      | 12  | 03    | 2015 | Minimum 16 years error                |
      | 31  | 03    | 2030 | Minimum 16 years error                |
      | 32  | 12    | 1989 | Invalid date of birth error           |
      | 30  | 02    | 1989 | Invalid date of birth error           |
      | 29  | 02    | 2001 | Invalid date of birth error           |
      | 00  | 00    | 0000 | Invalid date of birth error           |
      | 11  | 00    | 2006 | Invalid date of birth error           |
      | 00  | 01    | 2000 | Invalid date of birth error           |
      | 31  | 04    | 1955 | Invalid date of birth error           |
      | 01  | 13    | 1970 | Invalid date of birth error           |
      | a   | 8     | 1988 | Invalid character day error           |
      | 11  | "8    | 1987 | Invalid character month error         |
      | 11  | b     | 1987 | Invalid character month error         |
      | 11  | 08    | {987 | Invalid character year error          |
      | 11  | 08    | aaaa | Invalid character year error          |
      | DD  | MM    | YYYY | Invalid character date of birth error |
      | !   | @3    | 2@@1 | Invalid character date of birth error |
      | -1  | 8     | 1988 | Invalid format date of birth error    |
      | 1-  | 8     | 1998 | Invalid format date of birth error    |
      | 11  | 9-    | 1987 | Invalid format date of birth error    |
      | 11  | 08    | 19-1 | Invalid format date of birth error    |
      | 11  | -1    | 199- | Invalid format date of birth error    |
      | 1   | 1     | 87   | Invalid format date of birth error    |

  Scenario Outline: Validate the hyperlinks on Date of Birth page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen        |
      | Back link         | Name screen   |
      | Service Name link | Start screen  |
      | GOV.UK link       | GOV UK screen |