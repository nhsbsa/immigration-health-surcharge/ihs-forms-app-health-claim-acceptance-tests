@JobTitle @IHSRI-1625 @Claim @Regression @IHSRI-3195

Feature: Validation of the Job Title Page on the IHS claim app to enable users to add details of their job title

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Job Title screen

  @Retest-1822 @Retest-1834 @Retest-1855
  Scenario Outline: Validate the job title functionality for positive and negative scenarios
    When My job title is <jobTitle>
    Then I will see the <output>

    Examples:
      | jobTitle                                                        | output                  |
      #Job title requires a job setting
      | Family Therapist                                                | Job Setting screen      |
      | Employment Support Worker Or Advisor                            | Job Setting screen      |
      | Control Assistant                                               | Job Setting screen      |
      | Clinical Supervisor                                             | Job Setting screen      |
      | Trainee Children's Wellbeing Practitioner                       | Job Setting screen      |
      | Child And Adolescent Psychological Therapist Or Psychotherapist | Job Setting screen      |
      | Work Analyst                                                    | Job Setting screen      |
      | Support,Time, Recovery Worker                                   | Job Setting screen      |
      | Clinical Supervisor                                             | Job Setting screen      |
      | Trainee High Intensity Therapist                                | Job Setting screen      |
      | Non-Emergency Medical Dispatcher                                | Job Setting screen      |
      | a bc                                                            | Job Setting screen      |
      | a b c                                                           | Job Setting screen      |
      | abc/                                                            | Job Setting screen      |
      | ab/c                                                            | Job Setting screen      |
      | ab1                                                             | Job Setting screen      |
      | ab 1                                                            | Job Setting screen      |
      #Job title doesn't require a job setting
      | Medical Director                                                | Check Employment screen |
      | Student District Nurse                                          | Check Employment screen |
      | Trust Grade Doctor - Specialty Registrar                        | Check Employment screen |
      | Trainee Health Psychologist                                     | Check Employment screen |
      | Orthotist Specialist Practitioner                               | Check Employment screen |
      | Music Therapist Specialist Practitioner                         | Check Employment screen |
      | Chiropodist/Podiatrist Specialist Practitioner                  | Check Employment screen |
      | Ambulance Care Assistant/Patient Transport Service Driver       | Check Employment screen |
      | Radiographer - Therapeutic, Specialist Practitioner             | Check Employment screen |
      | Physician Associate                                             | Check Employment screen |
      | Dental Surgeon Acting As Hospital Consultant                    | Check Employment screen |
      | Student Nurse - Learning Disabilities Branch                    | Check Employment screen |
      | GP Senior Partner                                               | Check Employment screen |
      | Student Nurse - Adult Branch                                    | Check Employment screen |
      | Foundation Year 1                                               | Check Employment screen |
      #negative tests
      |                                                                 | Blank job title error   |
      | ab                                                              | Invalid job title length error |
      | a b                                                             | Invalid job title length error |
      | ab/                                                             | Invalid job title length error |
      | a-b                                                             | Invalid job title length error |
      | é, â, ï                                                         | Invalid job title length error |
      | abc*                                                            | Invalid job title format error |
      | abc@test                                                        | Invalid job title format error |
      | abc %                                                           | Invalid job title format error |
      | public ^                                                        | Invalid job title format error |
      | 101 "                                                           | Invalid job title format error |
      | 99+                                                             | Invalid job title length error |
      | a1@                                                             | Invalid job title length error |
      | 1a \                                                            | Invalid job title length error |
      | a()                                                             | Invalid job title length error |
      | a]b[c                                                           | Invalid job title format error |
      | nurse{ }                                                        | Invalid job title format error |
      | abc@                                                            | Invalid job title format error |
      | abc ï                                                           | Invalid job title format error |
      | j#ob                                                            | Invalid job title format error |

  @Retest-1799
  Scenario Outline: Validate job title list is displayed on entering 3 or more characters
    When I enter <job> and select <jobTitle> from the job title list
    Then I will see the <output>

    Examples:
      | job     | jobTitle                                                        | output                  |
      | con     | midwife - consultant                                            | Check Employment screen |
      | chi     | child and adolescent psychological therapist or psychotherapist | Job Setting screen      |
      | trainee | trainee high intensity therapist                                | Job Setting screen      |
      | dra     | drama therapist specialist practitioner                         | Check Employment screen |
      | nur     | student nurse - adult branch                                    | Check Employment screen |

  @Retest-1824
  Scenario Outline: Validate maximum character limit is 70 characters in job title
    When I enter <count> characters in job title
    Then I am restricted to enter <maximum> characters job title
    And I will see the <output>
    Examples:
      | count | maximum | output             |
      | 72    | 70      | Job Setting screen |

  Scenario Outline: Validate the hyperlinks on Job Title page
    When I select the <hyperlink>
    Then I will see the <output>

    Examples:
      | hyperlink         | output               |
      | Service Name link | Start screen         |
      | Back link         | Employer Name screen |
      | GOV.UK link       | GOV UK screen        |
