@PhoneNumber @IHSRI-1614 @IHSRI-3071 @Regression

Feature: Validation of Phone Number Page on the IHS claim app to enable users to enter their phone number for the claim application

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Phone Number screen

  @Retest-1741 @Retest-1742 @Retest-1748 @Retest-3107
  Scenario Outline: Validate the enter phone number functionality for positive and negative scenarios
    When My Phone number is <PhoneNumber>
    Then I will see the <output>
    Examples:
      | PhoneNumber     | output                            |
      #positive tests
      | 09876543245     | Dependant question screen         |
      | 01234567890     | Dependant question screen         |
      |                 | Dependant question screen         |
      | 01632 960002    | Dependant question screen         |
      | 01632 960 001   | Dependant question screen         |
      | 07700 900 982   | Dependant question screen         |
      | (07700) 900 982 | Dependant question screen         |
      #negative tests
      | 7653456789      | Invalid phone number length error |
      | 07700 900 98    | Invalid phone number length error |
      | 0771 6541 3245  | Invalid phone number length error |
      | 016321 960002   | Invalid phone number length error |
      | 077001900198222 | Invalid phone number length error |
      | +44 20 7946 095 | Invalid phone number format error |
      | +6545678965     | Invalid phone number format error |
      | (07700) 9@0 982 | Invalid phone number format error |
      | +447700900982   | Invalid phone number format error |

  Scenario Outline: Validate user is not allowed to enter phone number more than 15 characters
    When I attempt to enter <count> characters phone number
    Then I am restricted to enter <maximum> characters phone number
    And I will see the <output>
    Examples:
      | count | maximum | output                            |
      | 16    | 15      | Invalid phone number length error |

  Scenario Outline: Validate the hyperlinks on Phone Number page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen               |
      | Back link         | Email address screen |
      | Service Name link | Start screen         |
      | GOV.UK link       | GOV UK screen        |