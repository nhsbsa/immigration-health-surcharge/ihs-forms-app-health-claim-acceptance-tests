@IHSRI-1637 @IHSRI-1638 @Declaration @Confirmation @Claim @Regression

Feature: Validation of Declaration and Confirmation page on Claim web app to enable the users to submit their claim application

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Check your answers screen - MultiFile

  Scenario Outline: Validate the hyperlinks on Declaration page
    When I confirm the answers
    And I will see the Declaration screen
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen                    |
      | Back link         | Check your answers screen |
      | Privacy Info link | Privacy Notice screen     |
      | Service Name link | Start screen              |
      | GOV.UK link       | GOV UK screen             |

  Scenario Outline: Validate the hyperlinks on Application Complete page
    When I confirm the declaration
    And I will see the Application Complete screen
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink                  | screen                        |
      | End of service survey link | Gov UK Health Feedback screen |
      | Contact Us link            | Contact Us screen             |
      | Service Name link          | Start screen                  |
      | GOV.UK link                | GOV UK screen                 |