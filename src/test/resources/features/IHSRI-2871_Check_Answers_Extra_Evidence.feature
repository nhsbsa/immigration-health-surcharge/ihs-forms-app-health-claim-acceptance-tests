@IHSRI-2684 @IHSRI-2871 @CheckYourAnswers @ExtraEvidence @Regression

Feature: Validation of Check Your Answers functionality for Extra Evidence Uploaded on Health Claim web app to enable users to check and change their answers before submitting the claim application

  Background:
    Given I launch the IHS Health claim application

   @IHSRI-2803
  Scenario Outline: Validate the change links for Extra Evidence Uploaded and Extra Information on Check Your Answers page - do not change the answers and verify answer remains same
    And My details are captured until Check your answers screen with extra evidence - MultiFile
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check your answers screen
    And My answer remains same as on <screen>
    #And Verify the extra evidence uploaded is displayed without "extra-info-" prefix on Check Your Answers page

    Examples:
      | changeLink                          | screen                         | answer            |
      | Change Extra Evidence Uploaded Link | Extra Evidence Uploaded screen | Extra Evidence    |
      | Change Extra Information Link       | Extra Information screen       | Extra Information |

   @IHSRI-2803 @Retest-2867
  Scenario Outline: Validate the change links for Extra Evidence on Check Your Answers page - change the answers and verify answers are updated
    And My details are captured until Check your answers screen with extra evidence - MultiFile
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change my <answer> and continue
    Then I will see the Extra Information screen
    When I upload a file with name <fileName><fileFormat>
    Then I will see the <screen>
    When I do not want to upload another extra evidence
    Then I will see the Check your answers screen
    And I see the updated Extra Evidences on Check Your Answers screen
    #And Verify the extra evidence uploaded is displayed without "extra-info-" prefix on Check Your Answers page

    Examples:
      | changeLink                          | screen                         | answer         | fileName     | fileFormat |
      | Change Extra Evidence Uploaded Link | Extra Evidence Uploaded screen | Extra Evidence | Payslip-June | .png       |

  
  Scenario Outline: Validate the change links for Extra Information on Check Your Answers page - without extra evidence
    And My details are captured until Check your answers screen - MultiFile
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change my <answer> and do not continue
    And I upload a file with name <fileName><fileFormat>
    And I do not want to upload another extra evidence
    Then I will see the Check your answers screen
    And I see the updated Extra Evidence on Check Your Answers screen

    Examples:
      | changeLink                    | screen                   | answer            | fileName     | fileFormat |
      | Change Extra Information Link | Extra Information screen | Extra Information | Payslip-June | .png       |

   @Retest-2814
  Scenario Outline: Validate that files are deleted using Extra Evidence Uploaded change links on Check Your Answers page
    And My details are captured until Check your answers screen with extra evidence - MultiFile
    When I select <changeLink> on Check Your Answers page
    Then I will see the Extra Evidence Uploaded screen
    And My claimant's <answer> is pre-populated
    When I select Delete File 1 link on Extra Evidence Uploaded screen
    Then I will see the <screen>
    When I continue without changing my answer on Extra Information screen
    Then I will see the Check your answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>

    Examples:
      | changeLink                          | screen                   | answer         |
      | Change Extra Evidence Uploaded Link | Extra Information screen | Extra Evidence |

  
  Scenario Outline: Validate the error when more than 3 files are uploaded using Extra Evidence Uploaded change links on Check Your Answers page
    And My details are captured until Extra Information screen - MultiFile
    And My entered extra information is <information>
    When I upload 3 different extra files to my claim application
    And I do not want to upload another extra evidence
    Then I will see the Check your answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I do want to upload another extra evidence
    Then I will see the <error>

    Examples:
      | information       | changeLink                          | screen                         | answer          | error                   |
      | extra information | Change Extra Evidence Uploaded Link | Extra Evidence Uploaded screen | Extra Evidences | More than 3 files error |

  @Retest-2863 @JavaScriptDisabled
  Scenario Outline: Validate the error when extra information is changed and more than 3 files are uploaded using Extra Information change links on Check Your Answers page
    And My details are captured until Extra Information screen
    And My entered extra information is <information>
    When I upload 3 different extra files to my claim application
    And I do not want to upload another extra evidence
    Then I will see the Check your answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    And I change my <answer> and do not continue
    When I upload a file with name <fileName><fileFormat>
    Then I will see the <output>

    Examples:
      | information       | changeLink                    | screen                   | answer  | fileName     | fileFormat | output                  |
      | extra information | Change Extra Information Link | Extra Information screen | Extra Information | Payslip-June | .png       | More than 3 files error |

  @Retest-2805 @JavaScriptDisabled
  Scenario Outline: Validate the errors when invalid extra information and more than 3 files are uploaded using Extra Information change links on Check Your Answers page
    And My details are captured until Extra Information screen
    And My entered extra information is <information>
    When I upload 3 different extra files to my claim application
    And I do not want to upload another extra evidence
    Then I will see the Check your answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When My entered extra information is <invalidInformation>
    And I upload a file with name <fileName><fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> extra evidence

    Examples:
      | information       | changeLink                    | screen                   | answer            | invalidInformation | fileName     | fileFormat | error                                            |
      | extra information | Change Extra Information Link | Extra Information screen | Extra Information | Tes! Extra Information$      | 'My Payslip' | .jpg       | Invalid extra information and Invalid file error |

  @Retest-2818 @JavaScriptDisabled
  Scenario Outline: Validate the errors when invalid extra information and file is uploaded using Extra Evidence Uploaded change links on Check Your Answers page
    And My details are captured until Check your answers screen with extra evidence
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And I do want to upload another extra evidence
    When My entered extra information is <invalidInformation>
    And I upload a file with name <fileName><fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> extra evidence

    Examples:
      | invalidInformation | changeLink                          | screen                         | fileName     | fileFormat | error                                            |
      | Tes! Extra Information$      | Change Extra Evidence Uploaded Link | Extra Evidence Uploaded screen | 'My Payslip' | .jpg       | Invalid extra information and Invalid file error |
  
  @JavaScriptDisabled
  Scenario Outline: Validate the back link functionality using Extra Evidence Uploaded change links on Check Your Answers page - SingleFile
    And My details are captured until Check your answers screen with extra evidence
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And I do want to upload another extra evidence
    And My claimant's Extra Information is pre-populated
    When I select the Back link
    Then I will see the Evidence Uploaded screen
    When I click on continue
    Then I will see the Check your answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                          | screen                         |
      | Change Extra Evidence Uploaded Link | Extra Evidence Uploaded screen |

  Scenario Outline: Validate the back link functionality using Extra Evidence Uploaded change links on Check Your Answers page - MultiFile
    And My details are captured until Check your answers screen with extra evidence - MultiFile
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And I do want to upload another extra evidence
    And My claimant's Extra Information is pre-populated
    When I select the Back link
    Then I will see the Upload Evidence screen
    When I click on continue
    Then I will see the Check your answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                          | screen                         |
      | Change Extra Evidence Uploaded Link | Extra Evidence Uploaded screen |

   @Retest-2811
  Scenario Outline: Validate the back link functionality for invalid extra information using Extra Information change links on Check Your Answers page
    And My details are captured until Check your answers screen - MultiFile
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change the answer to <invalidAnswer>
    Then I will see the <error>
    When I select the Back link
    Then I will see the Upload Evidence screen
    When I continue without changing my answer on Upload Evidence screen
    Then I will see the Check your answers screen
    And My answer remains same as on Extra Information screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>

    Examples:
      | changeLink                    | screen                   | answer  | invalidAnswer             | error                                  |
      | Change Extra Information Link | Extra Information screen | Extra Information | Invalid Extra Information | Invalid extra information format error |

   @Retest-2833 @Retest-2872
  Scenario Outline: Validate the back link functionality for invalid extra information and file using Extra Evidence Uploaded change links on Check Your Answers page
    And My details are captured until Check your answers screen - MultiFile
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change the answer to <invalidAnswer>
    And I upload a file with name <fileName><fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> extra evidence
    When I select the Back link
    Then I will see the Upload Evidence screen
    When I continue without changing my answer on Upload Evidence screen
    Then I will see the Check your answers screen
    And My answer remains same as on Extra Information screen

    Examples:
      | changeLink                          | screen                   | answer  | invalidAnswer   | fileName     | fileFormat | error                                            |
      | Change Extra Evidence Uploaded Link | Extra Information screen | Extra Information | Invalid Extra Information | Payslip(Nov) | .png       | Invalid extra information and Invalid file error |

  @IHSRI-2801 @JavaScriptDisabled
  Scenario Outline: Validate the files added on Evidence uploaded page using Files Added change links on Check Your Answers page
    And My details are captured until Check your answers screen with extra evidence
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the <screen1>
    And My answer remains same as on <screen>

    Examples:
      | changeLink              | screen                   | answer           | screen1                   |
      | Change Files Added link | Evidence Uploaded screen | Evidence details | Check your answers screen |

   @IHSRI-2801
  Scenario Outline: Validate the files added on Upload Evidence page using Files Added change links on Check Your Answers page
    And My details are captured until Check your answers screen with extra evidence - MultiFile
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the <screen1>
    And My answer remains same as on <screen>
    Examples:
      | changeLink              | screen                 | answer     | screen1                   |
      | Change Files Added link | Upload Evidence screen | Multi File | Check your answers screen |

   @IHSRI-2767 @IHSRI-2775 @IHSRI-2800
  Scenario Outline: Validate the files added and extra evidence uploaded on Check Your Answers page with maximum different files uploaded using multi-file upload
    And My details are captured until Upload Evidence screen
    When I upload 75 new multi files to my claim application
    And I click on continue
    Then I will see the Extra Information screen
    And My entered extra information is <information>
    When I upload 3 different extra files to my claim application
    And I do not want to upload another extra evidence
    Then I will see the <screen>
    And I will see 75 files under <section1> section
    And I will see 3 files under <section2> section
    When I confirm the declaration
    Then My claim is submitted successfully
    #And Verify 78 files are added in Dynamics and extra evidence have prefix "extra-info-"

    Examples:
      | information       | screen                    | section1    | section2       |
      | extra information | Check your answers screen | Files Added | Extra Evidence |

   @IHSRI-2767 @IHSRI-2800 @Retest-2817
  Scenario Outline: Validate the files added and extra evidence on Check Your Answers page with maximum same files uploaded using multi-file upload
    And My details are captured until Upload Evidence screen
    When I add 75 multi files to my claim application
    And I click on continue
    Then I will see the Extra Information screen
    And My entered extra information is <information>
    When I upload 3 same extra files to my claim application
    And I do not want to upload another extra evidence
    Then I will see the <screen>
    And I will see 75 files under <section1> section
    And I will see 3 files under <section2> section
    When I confirm the declaration
    Then My claim is submitted successfully
    #And Verify 78 files are added in Dynamics and extra evidence have prefix "extra-info-"

    Examples:
      | information       | screen                    | section1    | section2       |
      | extra information | Check your answers screen | Files Added | Extra Evidence |

  @IHSRI-2767 @IHSRI-2775 @IHSRI-2800 @JavaScriptDisabled
  Scenario Outline: Validate the files added and extra evidence on Check Your Answers page with maximum files uploaded using single file upload
    And My details are captured until Upload Evidence screen
    When I add 75 new files to my claim application
    And I do not want to add another evidence
    Then I will see the Extra Information screen
    And My entered extra information is <information>
    When I upload 3 different extra files to my claim application
    And I do not want to upload another extra evidence
    Then I will see the <screen>
    And I will see 75 files under <section1> section
    And I will see 3 files under <section2> section
    When I confirm the declaration
    Then My claim is submitted successfully
    #And Verify 78 files are added in Dynamics and additional evidence have prefix "extra-info-"

    Examples:
      | information       | screen                    | section1    | section2       |
      | extra information | Check your answers screen | Files Added | Extra Evidence |