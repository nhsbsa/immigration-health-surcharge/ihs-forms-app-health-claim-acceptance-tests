@MoreDependants @IHSRI-1641 @Regression

Feature: Validation of the add more dependants functionality on the IHS claim app to enable users to add more dependants

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Dependant Question screen

  @Retest-1767
  Scenario Outline: Validate the user is able to add multiple dependants to the claim application
    When I add a dependant to my claim application
    And I <dependantMoreOption> have another dependant to add
    Then I will see the <output>
    Examples:
      | dependantMoreOption | output                   |
      | do                  | Dependant details screen |
      | do not              | Claim start date screen  |
      |                     | Select Yes or No error   |

  @Retest-1769
  Scenario Outline: Validate the error when user attempts to add more than 10 dependants
    When I add <count> dependants to my claim application
    And I <dependantMoreOption> have another dependant to add
    Then I will see the <output>
    And I will see the <screen>
    Examples:
      | count | dependantMoreOption | output                        | screen                        |
      | 10    | do                  | More than 10 dependants error | Check dependants error screen |