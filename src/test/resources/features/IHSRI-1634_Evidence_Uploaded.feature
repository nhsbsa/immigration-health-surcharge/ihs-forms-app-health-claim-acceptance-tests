@ViewFiles @IHSRI-1634 @Regression @JavaScriptDisabled

Feature: Validation of the Attachments page on the IHS claim app to enable users to view their uploaded evidence

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Upload Evidence screen

  @Smoke
  Scenario Outline: Validate the user is able to view the uploaded evidence in Evidence screen
    When I upload the file <fileName> of format <fileFormat>
    And I view the uploaded evidence <fileName>
    And I do not want to add another evidence
    Then I will see the <output>
    Examples:
      | fileName               | fileFormat | output                   |
      | Sample_4               | .bmp       | Extra Information screen |
      | sample_3               | .pdf       | Extra Information screen |
      | Payslip-June           | .png       | Extra Information screen |
      | Sample-jpg-image-1-9mb | .jpeg      | Extra Information screen |
      | Large_size_jpg_file    | .jpg       | Extra Information screen |
      | Large_size_png_file    | .png       | Extra Information screen |

  @Retest-1801
  Scenario Outline: Validate the options user can select in Evidence screen to add multiple evidence
    When I upload a valid file in Upload Evidence screen
    And I <addMoreEvidence> want to add another evidence
    Then I will see the <output>
    Examples:
      | addMoreEvidence | output                   |
      | do              | Upload Evidence screen   |
      | do not          | Extra Information screen |
      |                 | Make a selection error   |

  @Retest-1815 @Retest-2760
  Scenario Outline: Validate the error when user attempts to add more than 75 same files
    When I add <count> same files to my claim application
    And I <addMoreEvidence> want to add another evidence
    Then I will see the <output>
    And I will see the <screen>
    Examples:
      | count | addMoreEvidence | output                   | screen                         |
      | 75    | do              | More than 75 files error | Evidence uploaded error screen |

  @Retest-1800
  Scenario Outline: Validate the hyperlinks on Evidence screen
    When I upload a valid file in Upload Evidence screen
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen        |
      | Service Name link | Start screen  |
      | GOV.UK link       | GOV UK screen |