@JobSetting @IHSRI-1626 @Claim @Regression @IHSRI-3196

Feature: Validation of the Job Setting Page on the IHS claim app to enable users to add details of their job type

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Job Setting screen

  Scenario Outline: Validate the job setting functionality for various job types
    When My job setting is <jobSetting>
    Then I will see the <output>

    Examples:
      | jobSetting                   | output                  |
      #positive tests
      | Hospital                     | Check Employment screen |
      | GP Practice                  | Check Employment screen |
      | Care Home                    | Check Employment screen |
      | Community Healthcare Setting | Check Employment screen |
      #negative tests
      |                              | Blank job setting error |

  @Retest-1796
  Scenario Outline: Validate the job setting functionality for other job types
    When My other job setting is <otherJobSetting>
    Then I will see the <output>

    Examples:
      | otherJobSetting                                 | output                           |
      #positive tests
      | Carers                                          | Check Employment screen          |
      | Practitioner nurse                              | Check Employment screen          |
      | Cardiac sonographer                             | Check Employment screen          |
      | Patient Care Counsellor                         | Check Employment screen          |
      | Health and safety instructor                    | Check Employment screen          |
      | Dialysis Nurse                                  | Check Employment screen          |
      | Mental Health Psychiatric and Addictions Nurse  | Check Employment screen          |
      | Consultant ENT                                  | Check Employment screen          |
      | Home Care                                       | Check Employment screen          |
      | a bc                                            | Check Employment screen          |
      | a b c                                           | Check Employment screen          |
      #negative tests
      |                                                 | Blank other job setting error    |
      | ab                                              | Invalid job setting length error |
      | a b                                             | Invalid job setting length error |
      | a/b                                             | Invalid job setting length error |
      | ab/                                             | Invalid job setting length error |
      | a-b                                             | Invalid job setting length error |
      | a ! b                                           | Invalid job setting length error |
      | abc*                                            | Invalid job setting format error |
      | abc@test                                        | Invalid job setting format error |
      | ab \                                            | Invalid job setting length error |
      | a^b                                             | Invalid job setting length error |
      | a+b                                             | Invalid job setting length error |
      | é, â, ï                                         | Invalid job setting length error |
      | abc()                                           | Invalid job setting format error |
      | a]b[c                                           | Invalid job setting format error |
      | nurse{ }                                        | Invalid job setting format error |
      | ab-tes't test_test tes! T@$T                    | Invalid job setting format error |
      | a b 1                                           | Invalid job setting format error |
      | abc ï                                           | Invalid job setting format error |
      | # abc                                           | Invalid job setting format error |

  Scenario Outline: Validate maximum character limit is 50 characters in job setting when Javascript enabled
    When I enter <count> characters in job setting
    Then I am restricted to enter <maximum> characters job setting
    And I will see the <output>
    Examples:
      | count | maximum | output                  |
      | 52    | 50      | Check Employment screen |

  Scenario Outline: Validate the hyperlinks on Job Setting page
    When I select the <hyperlink>
    Then I will see the <output>

    Examples:
      | hyperlink         | output           |
      | Service Name link | Start screen     |
      | Back link         | Job Title screen |
      | GOV.UK link       | GOV UK screen    |
