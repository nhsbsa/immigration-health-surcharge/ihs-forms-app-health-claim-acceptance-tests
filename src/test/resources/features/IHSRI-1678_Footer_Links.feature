@FooterLinks @IHSRI-1593 @IHSRI-1599 @IHSRI-1598 @IHSRI-1602 @IHSRI-1601 @IHSRI-1600 @Regression

Feature: Validation of the footer links in IHS Health reimbursement service

  Background:
    Given I launch the IHS Health claim application

  @Smoke @Retest-1678
  Scenario Outline: Validate the functionality of the footer links on IHS Health claim form
    When I select the <hyperlink>
    Then I will see the <screen>

    Examples:
      | hyperlink                    | screen                         |
      | Help link                    | Help screen                    |
      | Cookies link                 | Cookies Policy screen          |
      | Accessibility Statement link | Accessibility Statement screen |
      | Contact link                 | Contact Us screen              |
      | Terms and Conditions link    | Terms and Conditions screen    |
      | Privacy link                 | Privacy Notice screen          |
      | Open Government Licence link | Open Government Licence screen |
      | Crown Copyright link         | Crown Copyright screen         |