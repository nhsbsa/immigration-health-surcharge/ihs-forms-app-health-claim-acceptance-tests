@EmailAddress @IHSRI-1613 @Regression

Feature: Validation of Email Page on the IHS claim app to enable users to enter their email address for the claim application

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Email Address screen

  @Retest-1740 @Retest-1744
  Scenario Outline: Validate the enter Email address functionality for positive and negative scenarios
    When My Email address is <Email>
    Then I will see the <output>
    Examples:
      | Email                      | output                      |
      #positive tests
      | test@test.com              | Phone number screen         |
      | Nhsbsa.ihs-testing@nhs.net | Phone number screen         |
      #negative tests
      |                            | Blank email address error   |
      | plainaddress               | Invalid email address error |
      | @domain.com                | Invalid email address error |
      | a@test                     | Invalid email address error |

  Scenario Outline: Validate the hyperlinks on Email page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen            |
      | Back link         | IHS Number screen |
      | Service Name link | Start screen      |
      | GOV.UK link       | GOV UK screen     |