@EmploymentDetails @IHSRI-1618 @Retest-2888 @Claim @Regression @IHSRI-3194

Feature: Validation of the Employer Name Page on the IHS claim app to enable users to add details of their employer name

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Employer Name screen

  @Retest-1803 @Retest-1802 @Retest-1805 @Retest-1807 @Retest-1820 @Retest-1835 @Retest-1855 @IHSRI-2067 @Retest-3018
  Scenario Outline: Validate the employer name functionality for positive and negative scenarios
    When My employer name is <employerName>
    Then I will see the <output>

    Examples:
      | employerName                                                                   | output                             |
      #positive tests
      | Horwich Dental Practice                                                        | Job Title screen                   |
      | Heaton-Medical-Centre                                                          | Job Title screen                   |
      | ALCESTER SPECSAVERS                                                            | Job Title screen                   |
      | 770 OPTICS LTD                                                                 | Job Title screen                   |
      | ASDA FFORDD Y MILENIWM BARRY VALE OF GLAMORGAN                                 | Job Title screen                   |
      | 247 TIME LIMITED                                                               | Job Title screen                   |
      | AAH PHARMACEUTICALS                                                            | Job Title screen                   |
      | ABICA LIMITED                                                                  | Job Title screen                   |
      | ACCESS UK LTD                                                                  | Job Title screen                   |
      | ACTIPATH                                                                       | Job Title screen                   |
      | ACUO TECHNOLOGIES                                                              | Job Title screen                   |
      | ADAM HTT LTD                                                                   | Job Title screen                   |
      | ADVANCED COMPUTER SOFTWARE GROUP PLC                                           | Job Title screen                   |
      | 3 COUNTIES CANCER NETWORK                                                      | Job Title screen                   |
      | HANGING HEATON CHURCH OF ENGLAND VOLUNTARY CONTROLLED JUNIOR AND INFANT SCHOOL | Job Title screen                   |
      | A A BAKER OPTOMETRISTS                                                         | Job Title screen                   |
      | ALPHA EYECARE LTD T/A PW CHURMS (WHITCHURCH)                                   | Job Title screen                   |
      | HAMPSHIRE, ISLE OF WIGHT AND THAMES VALLEY COMMISSIONING HUB                   | Job Title screen                   |
      | 4PCC (BNSSG) PCN                                                               | Job Title screen                   |
      | abc_test                                                                       | Job Title screen                   |
      | abc:test                                                                       | Job Title screen                   |
      | abc [test]                                                                     | Job Title screen                   |
      | abc                                                                            | Job Title screen                   |
      | a bc                                                                           | Job Title screen                   |
      | a b c                                                                          | Job Title screen                   |
      | abc/                                                                           | Job Title screen                   |
      | ab/c                                                                           | Job Title screen                   |
      | ab1                                                                            | Job Title screen                   |
      | a@bc                                                                           | Job Title screen                   |
      | ab 1                                                                           | Job Title screen                   |
      #IHSRI-3194
      | NHS Business Services Authority                                                | Job Title screen                   |
      | LONDON REGION AND CENTRE                                                       | Job Title screen                   |
      | COMMUNITY HEALTH PARTNERSHIPS LTD                                              | Job Title screen                   |
      | BARTS AND THE LONDON NHS TRUST                                                 | Job Title screen                   |
      | DSCRO YORKSHIRE                                                                | Job Title screen                   |
      | PUBLIC HEALTH ENGLAND COLINDALE                                                | Job Title screen                   |
      | CHESHIRE AND MERSEYSIDE COMMISSIONING HUB                                      | Job Title screen                   |
      | YORKSHIRE AND HUMBER PUBLIC HEALTH OBSERVATORY                                 | Job Title screen                   |
      | NHS ENGLAND EAST OF ENGLAND (EAST)                                             | Job Title screen                   |
      | BARNET, ENFIELD, HARINGEY SHARED FINANCIAL SERVICES                            | Job Title screen                   |
      | PRINCESS MARY'S RAF HOSPITAL                                                   | Job Title screen                   |
      | NORTH EAST AND YORKSHIRE COMMISSIONING REGION                                  | Job Title screen                   |
      | BARKING, HAVERING AND REDBRIDGE UNIVERSITY HOSPITALS NHS TRUST                 | Job Title screen                   |
      | EAST OF ENGLAND COMMISSIONING REGION                                           | Job Title screen                   |
      | 4689 YORK                                                                      | Job Title screen                   |
      #negative tests
      |                                                                                | Blank employer name error          |
      | ab                                                                             | Invalid employer name length error |
      | a b                                                                            | Invalid employer name length error |
      | ab/                                                                            | Invalid employer name length error |
      | a-b                                                                            | Invalid employer name length error |
      | abc*                                                                           | Invalid employer name format error |
      | abc ?123                                                                       | Invalid employer name format error |
      | abc %                                                                          | Invalid employer name format error |
      | public ^                                                                       | Invalid employer name format error |
      | 101 "                                                                          | Invalid employer name format error |
      | 99+                                                                            | Invalid employer name length error |
      | é, â, ï                                                                        | Invalid employer name length error |
      | abc ï                                                                          | Invalid employer name format error |
      | nh@                                                                            | Invalid employer name length error |
      | A!B                                                                            | Invalid employer name length error |
      | abc;test                                                                       | Invalid employer name format error |
      | \ a12                                                                          | Invalid employer name format error |
      | a12 {a}                                                                        | Invalid employer name format error |
      | # emp                                                                          | Invalid employer name format error |

  Scenario Outline: Validate the hyperlinks on Employer Name page
    When I select the <hyperlink>
    Then I will see the <output>

    Examples:
      | hyperlink         | output               |
      | Service Name link | Start screen         |
      | Back link         | Subscriptions screen |
      | GOV.UK link       | GOV UK screen        |

  @Retest-1795 @Retest-1825 @Retest-1840 @Retest-3150 @Retest-3018
  Scenario Outline: Validate employer name list is displayed on entering 3 or more characters
    When I enter <emp> and select <employerName> from the list
    Then I will see the <output>

    Examples:
      | emp      | employerName                                                   | output           |
      | abc      | ABC CENTRE                                                     | Job Title screen |
      | 101      | 101 HEALTHCARE LTD                                             | Job Title screen |
      | a a b    | A A BAKER OPTOMETRISTS                                         | Job Title screen |
      | 3 ch     | 3 CHAPEL STREET                                                | Job Title screen |
      | 1a S     | 1A STOCKWELL ROAD (CARE HOME)                                  | Job Title screen |
      | raf l    | RAF LAKENHEATH HOSPITAL                                        | Job Title screen |
      | A31      | A31 GROUP PCN                                                  | Job Title screen |
      | YEW      | YEW TREE CARE HOME                                             | Job Title screen |
      | airedale | AIREDALE NHS FOUNDATION TRUST                                  | Job Title screen |
      | barking, | BARKING, HAVERING AND REDBRIDGE UNIVERSITY HOSPITALS NHS TRUST | Job Title screen |

  @Retest-1823
  Scenario Outline: Validate maximum character limit is 255 characters in employer name
    When I enter <count> characters in employer name
    Then I am restricted to enter <maximum> characters employer name
    And I will see the <output>
    Examples:
      | count | maximum | output           |
      | 257   | 255     | Job Title screen |