@ClaimStartDate @IHSRI-1616 @Regression

Feature: Validation of the Claim Start Date Page on the IHS claim app to enable users to enter the start date for their claim application

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Claim start date screen

  @Retest-1775 @Retest-1776 @IHSRI-1777 @Retest-1778
  Scenario Outline: Validate the enter claim start date functionality for positive and negative scenarios
    When My claim start date is <Day> <Month> <Year>
    Then I will see the <output>
    Examples:
      | Day | Month | Year | output                                |
      #positive tests
      | 31  | 03    | 2020 | Subscriptions screen                  |
      | 01  | 04    | 2020 | Subscriptions screen                  |
      | 11  | 05    | 2021 | Subscriptions screen                  |
      | 02  | 10    | 2021 | Subscriptions screen                  |
      | 1   | 2     | 2023 | Subscriptions screen                  |
      #negative tests
      |     |       |      | Blank claim start date error          |
      | 1   |       | 2020 | Invalid claim start date format error |
      | 31  | 3     |      | Invalid claim start date format error |
      |     | 3     | 2020 | Invalid claim start date format error |
      | 31  | 02    | 2020 | Invalid claim start date format error |
      | 1   | 1     | 21   | Invalid claim start date format error |
      | ##  | ∞§    | #€   | Invalid claim start date format error |
      | ab  | er    | 2021 | Invalid claim start date format error |
      | 30  | 03    | 2020 | Before 31 March 2020 error            |
      | 15  | 02    | 2020 | Before 31 March 2020 error            |
      | 24  | 06    | 2024 | Six months before today error         |

  Scenario Outline: Validate the hyperlinks on Claim Start Date page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen                    |
      | Back link         | Dependant question screen |
      | Service Name link | Start screen              |
      | GOV.UK link       | GOV UK screen             |