@DeleteDependant @IHSRI-1640 @Regression

Feature: Validation of the dependant delete functionality in Check Dependants Page to enable users to delete their dependants

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Dependant Question screen

  @Retest-1761 @Retest-1748
  Scenario Outline: Add single dependant - Validate the delete dependant functionality on Check dependants page
    When I add a dependant to my claim application
    And I select <deleteLink> in Check Dependants screen
    Then I will see the <screen>
    And I will see <radioButton> selected
    And I will see the <output>
    Examples:
      | deleteLink              | screen                    | radioButton | output                              |
      | Delete Dependant 1 link | Dependant question screen | none        | Dependant 1 deleted success message |

  @Retest-1766
  Scenario Outline: Add multiple dependants - Validate the delete dependant functionality on Check dependants page
    When I add <count> dependants to my claim application
    And I select <deleteLink> in Check Dependants screen
    Then I will see the <screen>
    And I will see the <output>
    Examples:
      | count | deleteLink              | screen                  | output                              |
      | 2     | Delete Dependant 2 link | Check dependants screen | Dependant 2 deleted success message |
      | 3     | Delete Dependant 1 link | Check dependants screen | Dependant 1 deleted success message |