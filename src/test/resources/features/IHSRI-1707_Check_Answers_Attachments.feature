@IHSRI-1707 @CheckYourAnswers @AttachmentDetails @Regression @JavaScriptDisabled

Feature: Validation of Check Your Answers functionality for Evidence Details on Health Claim web app to enable users to check and change their answers before submitting the claim application

  Background:
    Given I launch the IHS Health claim application

  Scenario Outline: Validate the change links on Check Your Answers page Evidence details - do not change the answers and verify answer remains same
    And My details are captured until Check your answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check your answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                    | screen                   | answer            |
      | Change Files Added link       | Evidence Uploaded screen | Evidence details  |
      | Change Extra Information Link | Extra Information screen | Extra Information |

  Scenario Outline: Validate the files added change link on Check Your Answers page Evidence details - change the answer and verify answers are updated
    And My details are captured until Check your answers screen with Dependant
    When I select Change Files Added link on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change my <answer> and continue
    Then I will see the <output>
    When I upload the file with name <fileName><fileFormat>
    Then I will see the <screen>
    When I do not want to add another evidence
    Then I will see the Check your answers screen
    And I see the updated <answer> on Check Your Answers screen

    Examples:
      | answer           | screen                   | output                 | fileName     | fileFormat |
      | Evidence details | Evidence Uploaded screen | Upload Evidence screen | Payslip-June | .png       |

  Scenario Outline: Validate the extra information change link on Check Your Answers page Evidence details - change the answer and verify answers are updated
    And My details are captured until Check your answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change my <answer> and continue
    Then I will see the Check your answers screen
    And I see the updated <answer> on Check Your Answers screen

    Examples:
      | changeLink                    | screen                   | answer            |
      | Change Extra Information Link | Extra Information screen | Extra Information |

  Scenario Outline: Validate the back link from extra information change link on Check Your Answers page Evidence details without changing the answer
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I select the Back link
    Then I will see the <output>
    When I continue without changing my answer on <output>
    Then I will see the Check your answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                    | screen                   | answer            | output                   |
      | Change Extra Information Link | Extra Information screen | Extra Information | Evidence Uploaded screen |

  Scenario Outline: Validate the back link from extra information change link on Check Your Answers page Evidence details by changing the answer
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I select the Back link
    Then I will see the <output>
    When I change my Evidence details and continue
    Then I will see the Upload Evidence screen
    When I upload the file with name <fileName><fileFormat>
    Then I will see the <output>
    When I do not want to add another evidence
    Then I will see the Check your answers screen
    And I see the updated Evidence details on Check Your Answers screen

    Examples:
      | changeLink                    | screen                   | answer            | output                   | fileName     | fileFormat |
      | Change Extra Information Link | Extra Information screen | Extra Information | Evidence Uploaded screen | Payslip-June | .png       |

  Scenario Outline: Validate the errors when incorrect extra information is entered using change links on Check Your Answers page Evidence details
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change the answer to <invalidAnswer>
    Then I will see the <output>

    Examples:
      | changeLink                    | screen                   | invalidAnswer             | answer            | output                                 |
      | Change Extra Information Link | Extra Information screen | Invalid Extra Information | Extra Information | Invalid Extra Information format error |

  Scenario Outline: Validate the errors when incorrect file is added using change links on Check Your Answers page Evidence details
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change my <answer> and continue
    Then I will see the Upload Evidence screen
    When I upload the file with name <fileName><fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> file

    Examples:
      | changeLink              | answer           | screen                   | fileName     | fileFormat | error                     |
      | Change Files Added link | Evidence details | Evidence Uploaded screen | Sample excel | .xlsx      | Invalid file format error |
      | Change Files Added link | Evidence details | Evidence Uploaded screen | Payslip(Nov) | .png       | Invalid file name error   |

  @Retest-2077
  Scenario: Validate the back link from File change link on Check Your Answers page by deleting the files
    When My details are captured until Check your answers screen
    And I select Change Files Added link on Check Your Answers page
    And I will see the Evidence Uploaded screen
    And My claimant's Evidence details is pre-populated
    And I select Delete File 1 link in Evidence uploaded screen
    And I will see the Upload Evidence screen
    And I will see delete success text for file payslip4 of format .png
    And I select the Back link
    And I will see the Check Employment screen
    And I continue without changing my answer on Check Employment screen
    And I will see the Upload Evidence screen
    And I upload the file with name Payslip-June.png
    And I will see the Evidence Uploaded screen
    And I do not want to add another evidence
    Then I will see the Check your answers screen
    And I see the updated Evidence on Check Your Answers screen

  @Retest-2077
  Scenario: Validate that user is not able to submit the claim without evidences
    And My details are captured until Check your answers screen
    When I select Change Files Added link on Check Your Answers page
    And I will see the Evidence Uploaded screen
    And My claimant's Evidence details is pre-populated
    And I select Delete File 1 link in Evidence uploaded screen
    Then I will see the Upload Evidence screen
    And I will see delete success text for file payslip4 of format .png
    And I select the Back link
    And I will see the Check Employment screen
    And I select Change Employer Name link in Check Employment Details screen
    And I will see the Employer Name screen
    And I select the Back link
    And I continue without changing my answer on Subscriptions screen
    And I will see the Check your answers screen
    And I confirm the answers
    Then I will see the Submission without evidence error