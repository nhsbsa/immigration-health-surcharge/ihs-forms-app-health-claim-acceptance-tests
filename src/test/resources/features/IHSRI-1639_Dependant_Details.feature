@DependantDetails @IHSRI-1639 @IHSRI-3444 @Regression

Feature: Validation of the Dependant Details Page on the IHS claim app to enable users to add details of their dependants

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Dependant Details screen

  @Retest-1759 @Retest-1760 @Retest-1762 @Retest-1763 @Retest-3500
  Scenario Outline: Validate the dependant details functionality for positive and negative scenarios
    When My dependant's firstname is <depGivenName> and surname is <depFamilyName>
    And My dependant's date of birth is <depDay> <depMonth> <depYear>
    And My dependant's IHS number is <depIHSNumber>
    Then I will see the <output>
    Examples:
      | depGivenName         | depFamilyName       | depDay | depMonth | depYear | depIHSNumber  | output                                       |
      #positive tests
      | firstName            | lastName            | 30     | 12       | 1983    | IHS876545678  | Check dependants screen                      |
      | firstName            | O'hara              | 1      | 1        | 1989    | ihs876545678  | Check dependants screen                      |
      | firstName-middleName | lastName            | 02     | 8        | 1990    | IHS123456789  | Check dependants screen                      |
      | FirstName middleName | LastName            | 31     | 12       | 1999    | IHS789654321  | Check dependants screen                      |
      | firstName            | middleName lastName | 28     | 02       | 1989    | IHS876545678  | Check dependants screen                      |
      | fn                   | ln                  | 21     | 04       | 2022    | IHS876545678  | Check dependants screen                      |
      | F name               | L name              | 21     | 04       | 2022    | IHS876545678  | Check dependants screen                      |
      | Name D               | name S M            | 21     | 04       | 2022    | IHS876545678  | Check dependants screen                      |
      | Name s name          | name g name         | 21     | 04       | 2022    | IHS876545678  | Check dependants screen                      |
      | A                    | last name           | 21     | 04       | 2022    | IHS876545678  | Check dependants screen                      |
      | first name           | m                   | 21     | 04       | 2022    | IHS876545678  | Check dependants screen                      |
      | fn'                  | l-n                 | 21     | 04       | 2022    | IHS876545678  | Check dependants screen                      |
      | Oliver               | Wills               | 11     | 01       | 2002    | IHSC123456789 | Check dependants screen                      |
      | Diana                | Wills               | 10     | 03       | 2021    | IHSC906545678 | Check dependants screen                      |
      | Roma                 | Williams            | 21     | 05       | 2014    | IHSC976545678 | Check dependants screen                      |
      #negative tests
      |                      | Clooney             | 09     | 03       | 1989    | IHS985678945  | Blank dependant given name error             |
      | George               |                     | 21     | 09       | 1988    | IHS871344444  | Blank dependant family name error            |
      | Heidi                | Melissa             |        |          |         | IHS888888888  | Blank dependant date of birth error          |
      | Saina                | Nehwal              | 06     | 09       | 1998    |               | Blank dependant ihs number error             |
      | Sam&                 | Miranda             | 07     | 06       | 2020    | IHS987654321  | Invalid dependant given name error           |
      | George               | Dave123             | 9      | 6        | 1980    | IHS765435678  | Invalid dependant family name error          |
      | '-                   | last name           | 07     | 06       | 2020    | IHS987654321  | Invalid dependant given name error           |
      | first name           | -                   | 9      | 6        | 1980    | IHS765435678  | Invalid dependant family name error          |
      | Sam:%test             | Miranda test        | 07     | 06       | 2020    | IHS987654321  | Invalid dependant given name error           |
      | George               | Dave :% test         | 9      | 6        | 1980    | IHS765435678  | Invalid dependant family name error          |
      | Sam                  | Miranda             | 31     | 02       | 1999    | IHS876545678  | Invalid format dependant date of birth error |
      | Sam                  | Miranda             | 00     | 02       | 1989    | IHS876545678  | Invalid format dependant date of birth error |
      | Heidi                | Melissa             | 01     | 05       | 1999    | 987654324     | Invalid range dependant ihs number error     |
      | Sam                  | Miranda             | 01     | 05       | 1999    | LHS876545678  | Invalid dependant ihs number error           |
      | Stacy                | Jones               | 11     | 01       | 2002    | IHSC12345678  | Invalid dependant ihs number error           |
      | Kathrine             | Jonas               | 10     | 03       | 2021    | 1IHSC90654567 | Invalid dependant ihs number error           |
      | Steve                | Miranda             | 01     | 05       | 1999    | LHS876545678  | Invalid dependant ihs number error           |
      | Stacy                | Jones               | 11     | 01       | 2002    | IHSC12345678  | Invalid dependant ihs number error           |
      | Kathrine             | Jonas               | 10     | 03       | 2021    | 1IHSC90654567 | Invalid dependant ihs number error           |
      | Ellie                | Jonas               | 10     | 03       | 2021    | @IHSC90654567 | Invalid dependant ihs number error           |
      | Jamie                | Davis               | 21     | 05       | 2014    | IHSC9765456@  | Invalid dependant ihs number error           |
      | Rose                 | Davis               | 21     | 05       | 2014    | IHSC9765456   | Invalid range dependant ihs number error     |
      | Stacy                | Williams            | 11     | 01       | 2002    | IHS23456789   | Invalid range dependant ihs number error     |

  Scenario Outline: Validate the hyperlinks on Dependant Details page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink                            | screen                                  |
      | Back link                            | Dependant question screen               |
      | Service Name link                    | Start screen                            |
      | GOV.UK link                          | GOV UK screen                           |
      | Dependant Visas and Immigration link | Contact UK Visas and Immigration screen |