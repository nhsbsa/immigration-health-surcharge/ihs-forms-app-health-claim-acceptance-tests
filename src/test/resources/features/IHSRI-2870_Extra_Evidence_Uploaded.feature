@ExtraEvidenceUploaded @IHSRI-2683 @IHSRI-2870 @Regression

Feature: Validation of the Extra Evidence Uploaded page on the IHS claim app to enable users to view their uploaded extra evidence

  Background:
    Given I launch the IHS Health claim application

  @Smoke @IHSRI-2802
  Scenario Outline: Validate the user is able to view the uploaded evidence on Extra Evidence Uploaded screen
    And My details are captured until Extra Information screen - MultiFile
    When I upload a file <fileName> of format <fileFormat>
    Then I will see the <screen>
    And I view the extra evidence <fileName> is uploaded successfully
    Examples:
      | fileName            | fileFormat | screen                         |
      | Sample_4            | .bmp       | Extra Evidence Uploaded screen |
      | sample_3            | .pdf       | Extra Evidence Uploaded screen |
      | Payslip-June        | .png       | Extra Evidence Uploaded screen |
      | Large_size_jpg_file | .jpg       | Extra Evidence Uploaded screen |
      | Large_size_png_file | .png       | Extra Evidence Uploaded screen |

  Scenario Outline: Validate the options user can select on Extra Evidence Uploaded screen
    And My details are captured until Extra Information screen - MultiFile
    When I upload a valid extra evidence
    And I <addMoreEvidence> want to upload another extra evidence
    Then I will see the <output>
    Examples:
      | addMoreEvidence | output                                    |
      | do              | Extra Information screen                  |
      | do not          | Check your answers screen                 |
      |                 | Selection error for upload extra evidence |

  Scenario Outline: Validate the extra evidence delete functionality when single file is uploaded and deleted
    And My details are captured until Extra Information screen - MultiFile
    When I upload a file <fileName> of format <fileFormat>
    And I view the extra evidence <fileName> is uploaded successfully
    And I select <deleteLink> on Extra Evidence Uploaded screen
    Then I will see the <output>
    And I will see delete success message on Extra Information screen for file <fileName><fileFormat>
    Examples:
      | fileName               | fileFormat | output                   | deleteLink         |
      | Sample_4               | .bmp       | Extra Information screen | Delete File 1 link |
      | sample_3               | .pdf       | Extra Information screen | Delete File 1 link |
      | Payslip-June           | .png       | Extra Information screen | Delete File 1 link |
      | Sample-jpg-image-1-9mb | .jpeg      | Extra Information screen | Delete File 1 link |
      | Large_size_jpg_file    | .jpg       | Extra Information screen | Delete File 1 link |
      | Large_size_png_file    | .png       | Extra Information screen | Delete File 1 link |

  @EndToEndExtraEvidenceDelete
  Scenario Outline: Validate that deleted extra evidence is not submitted with the claim
    And My details are captured until Extra Information screen - MultiFile
    And My entered extra information is delete extra evidence
    When I upload a file <fileName> of format <fileFormat>
    And I view the extra evidence <fileName> is uploaded successfully
    And I do want to upload another extra evidence
    Then I will see the <screen>
    When I upload a file <fileName1> of format <fileFormat1>
    And I select <deleteLink> on Extra Evidence Uploaded screen
    Then I will see delete success message for the file <fileName1><fileFormat1>
    And I do not want to upload another extra evidence
    And I will see the Check your answers screen
    When I confirm the declaration
    Then My claim is submitted successfully
    # And Only one additional evidence is added in dynamics
    Examples:
      | fileName | fileFormat | fileName1    | fileFormat1 | deleteLink         | screen                   |
      | Sample_1 | .jpg       | Payslip-June | .png        | Delete File 2 link | Extra Information screen |

  @ExtraEvidenceDeleteAll @Retest-2773 @JavaScriptDisabled
  Scenario Outline: Validate the additional evidence delete functionality when all files are deleted - SingleFile
    And My details are captured until Extra Information screen
    And My entered extra information is <information>
    When I upload a file <fileName> of format <fileFormat>
    And I view the extra evidence <fileName> is uploaded successfully
    And I do want to upload another extra evidence
    Then I will see the Extra Information screen
    When I upload a file <fileName1> of format <fileFormat1>
    And I select Delete File 2 link on Extra Evidence Uploaded screen
    Then I will see delete success message for the file <fileName1><fileFormat1>
    When I select Delete File 1 link on Extra Evidence Uploaded screen
    Then I will see the Extra Information screen
    And My claimant's Extra Information is pre-populated
    Examples:
      | information       | fileName | fileFormat | fileName1    | fileFormat1 |
      | extra information | payslip4 | .png       | Payslip-June | .png        |

  @ExtraEvidenceDeleteAll @Retest-2804
  Scenario Outline: Validate the additional evidence delete functionality when all files are deleted - MultiFile
    And My details are captured until Extra Information screen - MultiFile
    And My entered extra information is <information>
    When I upload a file <fileName> of format <fileFormat>
    And I view the extra evidence <fileName> is uploaded successfully
    And I do want to upload another extra evidence
    Then I will see the Extra Information screen
    When I upload a file <fileName1> of format <fileFormat1>
    And I select Delete File 2 link on Extra Evidence Uploaded screen
    Then I will see delete success message for the file <fileName1><fileFormat1>
    When I select Delete File 1 link on Extra Evidence Uploaded screen
    Then I will see the Extra Information screen
    And My claimant's Extra Information is pre-populated
    Examples:
      | information       | fileName | fileFormat | fileName1    | fileFormat1 |
      | extra information | payslip4 | .png       | Payslip-June | .png        |

  @Retest-2836 @JavaScriptDisabled
  Scenario Outline: Validate the back link functionality for adding another invalid evidence and extra information
    And My details are captured until Extra Information screen
    And My entered extra information is <information>
    When I upload a file <fileName> of format <fileFormat>
    And I view the extra evidence <fileName> is uploaded successfully
    And I do want to upload another extra evidence
    Then I will see the Extra Information screen
    And My entered extra information is <invalidInformation>
    When I upload a file <fileName1> of format <fileFormat1>
    Then I will be displayed <error> for <fileName1><fileFormat1> extra evidence
    When I select the Back link
    And I click on continue
    Then I will see the Extra Information screen
    And My claimant's Extra Information is pre-populated
    Examples:
      | information       | fileName | fileFormat | fileName1    | fileFormat1 | invalidInformation              | error                                            |
      | extra information | payslip4 | .png       | Payslip(Nov) | .png        | test Invalid information with $ | Invalid extra information and Invalid file error |

  @Retest-2782
  Scenario Outline: Validate the extra evidence uploaded functionality when 'File Name' with 130 characters is deleted
    And My details are captured until Extra Information screen - MultiFile
    And My entered extra information is <information>
    When I upload a file <fileName> of format <fileFormat>
    And I view the extra evidence <fileName> is uploaded successfully
    And I do want to upload another extra evidence
    Then I will see the Extra Information screen
    When I upload a file <fileName> of format <fileFormat>
    And I select Delete File 2 link on Extra Evidence Uploaded screen
    Then I will see delete success message for the file <fileName><fileFormat>
    When I select Delete File 1 link on Extra Evidence Uploaded screen
    Then I will see the Extra Information screen
    And I will see delete success message on Extra Information screen for file <fileName><fileFormat>
    Examples:
      | information      | fileName                                                                                                                           | fileFormat |
      | Test information | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       |

  @IHSRI-2802
  Scenario Outline: Validate the extra evidence uploaded functionality with maximum files uploaded - MultiFile
    And My details are captured until Extra Information screen - MultiFile
    And My entered extra information is <information>
    When I upload 3 <files> extra files to my claim application
    Then I will see extra files are added having <fileName>
    When I <addMoreEvidence> want to upload another extra evidence
    Then I will see the <output>
    Examples:
      | information      | files     | fileName           | addMoreEvidence | output                    |
      #positive - upload 3 files
      | test information | different | different filename | do not          | Check your answers screen |
      | test information | same      | same filename      | do not          | Check your answers screen |
      #negative - upload more than 3 files
      | test information | different | different filename | do              | More than 3 files error   |
      | test information | same      | same filename      | do              | More than 3 files error   |

  @IHSRI-2802 @JavaScriptDisabled
  Scenario Outline: Validate the extra evidence uploaded functionality with maximum files uploaded - SingleFile
    And My details are captured until Extra Information screen
    And My entered extra information is <information>
    When I upload 3 <files> extra files to my claim application
    Then I will see extra files are added having <fileName>
    When I <addMoreEvidence> want to upload another extra evidence
    Then I will see the <output>
    Examples:
      | information      | files     | fileName           | addMoreEvidence | output                    |
      #positive - upload 3 files
      | test information | different | different filename | do not          | Check your answers screen |
      | test information | same      | same filename      | do not          | Check your answers screen |
      #negative - upload more than 3 files
      | test information | different | different filename | do              | More than 3 files error   |
      | test information | same      | same filename      | do              | More than 3 files error   |

  Scenario Outline: Validate the hyperlinks on Extra Evidence Uploaded page
    And My details are captured until Extra Information screen - MultiFile
    When I upload a valid extra evidence
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen        |
      | Service Name link | Start screen  |
      | GOV.UK link       | GOV UK screen |