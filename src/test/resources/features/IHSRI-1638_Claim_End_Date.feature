@ClaimEndDate @IHSRI-1632 @IHSRI-1638 @Regression

Feature: Validation of Claim period end dates in the Evidence screen and Confirmation screen

  Background:
    Given I launch the IHS Health claim application


  Scenario Outline: Validate the correct claim end dates are displayed in the Evidence screen and Confirmation screen
    When My details are captured until Claim start date screen
    And I enter claim start on <day> <month> <year>
    Then I will see claim end on <output> in Evidence screen - MultiFile
    And I will see claim end on <output> in Confirmation screen

    Examples:
      | day | month | year | output            |
      | 31  | 03    | 2020 | 30 September 2020 |
      | 01  | 04    | 2020 | 30 September 2020 |
      | 01  | 08    | 2020 | 31 January 2021   |
      | 02  | 10    | 2020 | 01 April 2021     |
      | 30  | 08    | 2020 | 28 February 2021  |

  @JavaScriptDisabled
  Scenario Outline: Validate the correct claim end dates are displayed in the Evidence screen and Confirmation screen
    When My details are captured until Claim start date screen
    And I enter claim start on <day> <month> <year>
    Then I will see claim end on <output> in Evidence screen
    And I will see claim end on <output> in Confirmation screen

    Examples:
      | day | month | year | output            |
      | 31  | 03    | 2020 | 30 September 2020 |
      | 01  | 04    | 2020 | 30 September 2020 |
      | 01  | 08    | 2020 | 31 January 2021   |
      | 02  | 10    | 2020 | 01 April 2021     |
      | 30  | 08    | 2020 | 28 February 2021  |

  Scenario Outline: Validate the correct claim end dates are displayed in the Evidence screen and Confirmation screen
  (Tested without query date parameter)
    When My details are captured until Claim start date screen
    And I enter claim start on <day> <month> <year>
    Then I will see claim end on <output> in Evidence screen - MultiFile
    And I will see claim end on <output> in Confirmation screen

    Examples:
      | day | month | year | output            |
      #1st Period
      | 31  | 03    | 2020 | 30 September 2020 |
      | 01  | 04    | 2020 | 30 September 2020 |
      | 02  | 04    | 2020 | 01 October 2020   |
      | 30  | 04    | 2020 | 29 October 2020   |
      | 01  | 05    | 2020 | 31 October 2020   |
      | 02  | 05    | 2020 | 01 November 2020  |
      | 31  | 05    | 2020 | 30 November 2020  |
      | 01  | 06    | 2020 | 30 November 2020  |
      | 02  | 06    | 2020 | 01 December 2020  |
      | 30  | 06    | 2020 | 29 December 2020  |
      | 01  | 07    | 2020 | 31 December 2020  |
      | 02  | 07    | 2020 | 01 January 2021   |
      | 31  | 07    | 2020 | 30 January 2021   |
      | 01  | 08    | 2020 | 31 January 2021   |
      | 02  | 08    | 2020 | 01 February 2021  |
      | 28  | 08    | 2020 | 27 February 2021  |
      | 29  | 08    | 2020 | 28 February 2021  |
      | 01  | 09    | 2020 | 28 February 2021  |
      | 02  | 09    | 2020 | 01 March 2021     |
      | 30  | 09    | 2020 | 29 March 2021     |
      | 02  | 10    | 2020 | 01 April 2021     |
      #2nd Period
      | 01  | 10    | 2020 | 31 March 2021     |
      #February Exceptions
      | 30  | 08    | 2020 | 28 February 2021  |
      | 31  | 08    | 2020 | 28 February 2021  |

  Scenario Outline: Validate the correct claim end dates are displayed in the Evidence screen and Confirmation screen
  (Tested with query date parameter)
    When I submit the page with validation date <validationDate>
    And My details are captured until Claim start date screen
    And I enter claim start on <day> <month> <year>
    Then I will see claim end on <output> in Evidence screen - MultiFile
    And I will see claim end on <output> in Confirmation screen

    Examples:
      | validationDate | day | month | year | output            |
      #1st Period
      | 2021-05-15     | 30  | 10    | 2020 | 29 April 2021     |
      | 2021-05-15     | 31  | 10    | 2020 | 30 April 2021     |
      | 2021-05-15     | 01  | 11    | 2020 | 30 April 2021     |
      | 2021-05-15     | 02  | 11    | 2020 | 01 May 2021       |
      | 2021-06-15     | 30  | 11    | 2020 | 29 May 2021       |
      | 2021-06-15     | 01  | 12    | 2020 | 31 May 2021       |
      | 2021-06-15     | 02  | 12    | 2020 | 01 June 2021      |
      | 2021-07-15     | 30  | 12    | 2020 | 29 June 2021      |
      | 2021-07-15     | 31  | 12    | 2020 | 30 June 2021      |
      | 2021-07-15     | 01  | 01    | 2021 | 30 June 2021      |
      | 2021-08-15     | 31  | 01    | 2021 | 30 July 2021      |
      | 2021-08-15     | 01  | 02    | 2021 | 31 July 2021      |
      | 2021-08-15     | 02  | 02    | 2021 | 01 August 2021    |
      | 2021-09-15     | 28  | 02    | 2021 | 27 August 2021    |
      | 2021-09-15     | 02  | 03    | 2021 | 01 September 2021 |
      | 2021-10-15     | 30  | 03    | 2021 | 29 September 2021 |
      | 2021-10-15     | 31  | 03    | 2021 | 30 September 2021 |
      | 2021-10-15     | 01  | 04    | 2021 | 30 September 2021 |
      | 2022-02-15     | 31  | 07    | 2021 | 30 January 2022   |
      | 2022-03-15     | 29  | 08    | 2021 | 28 February 2022  |
      | 2022-08-15     | 31  | 01    | 2022 | 30 July 2022      |
      | 2023-08-15     | 31  | 01    | 2023 | 30 July 2023      |
      | 2024-03-15     | 29  | 08    | 2023 | 28 February 2024  |
      | 2024-03-15     | 01  | 09    | 2023 | 29 February 2024  |
      | 2024-03-15     | 02  | 09    | 2023 | 01 March 2024     |
      | 2024-08-15     | 31  | 01    | 2024 | 30 July 2024      |
      | 2025-03-19     | 19  | 09    | 2024 | 18 March 2025     |
      | 2024-08-15     | 01  | 02    | 2024 | 31 July 2024      |
      | 2024-08-15     | 02  | 02    | 2024 | 01 August 2024    |
      #February Exceptions
      | 2022-03-15     | 30  | 08    | 2021 | 28 February 2022  |
      | 2022-03-15     | 31  | 08    | 2021 | 28 February 2022  |
      | 2024-03-15     | 30  | 08    | 2023 | 29 February 2024  |
      | 2024-03-15     | 31  | 08    | 2023 | 29 February 2024  |
      #2nd Period
      | 2021-09-15     | 01  | 03    | 2021 | 31 August 2021    |
      #3rd Period
      | 2021-10-01     | 01  | 04    | 2021 | 30 September 2021 |
      | 2022-03-15     | 01  | 09    | 2021 | 28 February 2022  |
      #4th Period
      | 2022-04-01     | 01  | 10    | 2021 | 31 March 2022     |
      | 2022-09-01     | 01  | 03    | 2022 | 31 August 2022    |
      #5th Period
      | 2022-10-01     | 01  | 04    | 2022 | 30 September 2022 |
      | 2023-03-01     | 01  | 09    | 2022 | 28 February 2023  |
      #6th Period
      | 2023-04-01     | 01  | 10    | 2022 | 31 March 2023     |
      | 2023-09-01     | 01  | 03    | 2023 | 31 August 2023    |
      #7th Period
      | 2023-10-01     | 01  | 04    | 2023 | 30 September 2023 |
      | 2024-03-01     | 01  | 09    | 2023 | 29 February 2024  |
      #8th Period
      | 2024-04-01     | 01  | 10    | 2023 | 31 March 2024     |
      | 2024-09-01     | 01  | 03    | 2024 | 31 August 2024    |