@SingleFileUpload @IHSRI-1828 @Regression @JavaScriptDisabled

Feature: Validation of the Single File Upload page on the IHS claim app to enable users to upload evidence to prove their eligibility

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Upload Evidence screen

  Scenario Outline: Validate the single file upload functionality for positive and negative scenarios
    When I <uploadFile> upload the file
    Then I will see the <output>
    Examples:
      | uploadFile | output                   |
      | do         | Evidence Uploaded screen |
      | do not     | No file selected error   |

  @Smoke
  Scenario Outline: Validate the single file upload functionality for all the acceptable 'File' formats
    When I upload the file <fileName> of format <fileFormat>
    Then I will see the <output>
    Examples:
      | fileName | fileFormat | output                   |
      #valid file formats
      | Sample_1 | .jpg       | Evidence Uploaded screen |
      | sample_2 | .png       | Evidence Uploaded screen |
      | sample_3 | .pdf       | Evidence Uploaded screen |
      | Sample_4 | .bmp       | Evidence Uploaded screen |
      | Sample_5 | .jpeg      | Evidence Uploaded screen |

  Scenario Outline: Validate the single file upload functionality for the non-acceptable 'File' formats
    When I upload the file <fileName> of format <fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> file
    Examples:
      | fileName     | fileFormat | error                     |
      #invalid file formats
      | Sample excel | .xlsx      | Invalid file format error |
      | Sample text  | .rtf       | Invalid file format error |
      | Sample word  | .doc       | Invalid file format error |

  Scenario Outline: Validate the single file upload functionality for all the acceptable 'File Name' formats
    When I upload the file with name <fileName><fileFormat>
    Then I will see the <output>
    Examples:
      | fileName           | fileFormat | output                   |
      #valid file names
      | Payslip-June       | .png       | Evidence Uploaded screen |
      | My Payslip_1       | .pdf       | Evidence Uploaded screen |
      | Payslip July       | .bmp       | Evidence Uploaded screen |
      | 123456             | .pdf       | Evidence Uploaded screen |
      | 1-payslip_May Name | .jpg       | Evidence Uploaded screen |
      | Payslip   Mar      | .pdf       | Evidence Uploaded screen |

  Scenario Outline: Validate the single file upload functionality for the non-acceptable 'File Name' formats
    When I upload the file with name <fileName><fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> file
    Examples:
      | fileName     | fileFormat | error                   |
      #invalid file names
      | 'My Payslip' | .jpg       | Invalid file name error |
      | Payslip(Nov) | .png       | Invalid file name error |
      | Payslip.Jan  | .pdf       | Invalid file name error |

  Scenario Outline: Validate the single file upload functionality for all the acceptable 'File Size' formats
    When I upload the file <fileName><fileFormat> of size <fileSize>
    Then I will see the <output>
    Examples:
      | fileName               | fileFormat | fileSize | output                   |
       #valid file sizes
      | small_size_file        | .bmp       | > 35KB   | Evidence Uploaded screen |
      | SampleFile             | .jpg       | > 500KB  | Evidence Uploaded screen |
      | thisIsMy_PDF_1MB       | .pdf       | > 1MB    | Evidence Uploaded screen |
      | Large_size_jpg_file    | .jpg       | > 1MB    | Evidence Uploaded screen |
      | Large_size_png_file    | .png       | > 1MB    | Evidence Uploaded screen |
      | Sample-jpg-image-1-1mb | .jpeg      | > 1MB    | Evidence Uploaded screen |
      | Sample-jpg-image-1-2mb | .jpeg      | > 1.2MB  | Evidence Uploaded screen |
      | Sample-jpg-image-1-3mb | .jpeg      | > 1.3MB  | Evidence Uploaded screen |
      | Sample-jpg-image-1-5mb | .jpeg      | > 1.5MB  | Evidence Uploaded screen |
      | Sample-jpg-image-1-7mb | .jpeg      | > 1.7MB  | Evidence Uploaded screen |
      | Sample-jpg-image-1-8mb | .jpeg      | > 1.8MB  | Evidence Uploaded screen |
      | Sample-jpg-image-1-9mb | .jpeg      | > 1.9MB  | Evidence Uploaded screen |

  Scenario Outline: Validate the single file upload functionality for the non-acceptable 'File Size' formats
    When I upload the file <fileName><fileFormat> of size <fileSize>
    Then I will be displayed <error> for <fileName><fileFormat> file
    Examples:
      | fileName       | fileFormat | fileSize | error               |
      #invalid file sizes
      | More_than_2MB  | .bmp       | > 2MB    | Max file size error |
      | More_than_2MB  | .jpeg      | > 2MB    | Max file size error |
      | More_than_2MB  | .jpg       | > 2MB    | Max file size error |
      | More_than_2MB  | .pdf       | > 2MB    | Max file size error |
      | More_than_2MB  | .png       | > 2MB    | Max file size error |
      | More_than_5MB  | .bmp       | > 5MB    | Max file size error |
      | More_than_5MB  | .pdf       | > 5MB    | Max file size error |
      | More_than_10MB | .bmp       | > 10MB   | Max file size error |
      | More_than_10MB | .jpeg      | > 10MB   | Max file size error |
      | More_than_10MB | .jpg       | > 10MB   | Max file size error |
      | More_than_10MB | .pdf       | > 10MB   | Max file size error |
      | More_than_30MB | .pdf       | > 30MB   | Max file size error |
      | More_than_30MB | .png       | > 30MB   | Max file size error |
      | More_than_50MB | .bmp       | > 50MB   | Max file size error |

  Scenario Outline: Validate the single file upload functionality for more than 75 files uploaded
    When I add 75 new files to my claim application
    And I do want to add another evidence
    Then I will see the <error>

    Examples:
      | error                    |
      | More than 75 files error |

  @IHSRI-2790
  Scenario Outline: Validate the single file upload functionality for the 'File Name' with maximum 130 characters
    When I upload the file with name <fileName><fileFormat>
    Then I will see the <output>
    Examples:
      | fileName                                                                                                                           | fileFormat | output                   |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1 | .png       | Evidence Uploaded screen |
      | File-name-with-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890a  | .pdf       | Evidence Uploaded screen |

  @IHSRI-2790
  Scenario Outline: Validate the single file upload functionality for the 'File Name' with more than 130 characters
    When I upload the file with name <fileName><fileFormat>
    Then I will be displayed <error> for <fileName><fileFormat> file
    Examples:
      | fileName                                                                                                                             | fileFormat | error                          |
      | File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-123  | .png       | More than 130 characters error |
      | File-name-with-more-than-130-characters-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-1234567890-abcd | .pdf       | More than 130 characters error |

  Scenario Outline: Validate the hyperlinks on Upload Evidence page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen                  |
      | Back link         | Check Employment screen |
      | Service Name link | Start screen            |
      | GOV.UK link       | GOV UK screen           |