@Subscription @IHSRI-1617 @Regression

Feature: Validation of the Subscription Page on the IHS claim app to enable users to choose if they want to subscribe for reminders

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Subscription screen

  Scenario Outline: Validate the user is able to subscribe to email notifications
    When I <subscribeOption> choose to subscribe
    Then I will see the <output>
    Examples:
      | subscribeOption | output                     |
      | do              | Employer Name screen       |
      | do not          | Employer Name screen       |
      |                 | Invalid subscription error |

  Scenario Outline: Validate the hyperlinks on Subscription page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen                  |
      | Back link         | Claim start date screen |
      | Service Name link | Start screen            |
      | GOV.UK link       | GOV UK screen           |