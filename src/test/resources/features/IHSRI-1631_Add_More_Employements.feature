@MoreEmployers @IHSRI-1631 @Regression

Feature: Validation of the add more employers functionality on the IHS claim app to enable users to add more employers

  Background:
    Given I launch the IHS Health claim application
    And My details are captured until Employer Name screen

  @Retest-1817
  Scenario Outline: Validate the user is able to add multiple employers to the claim application
    When I add 1 employer to my claim application
    And I <employmentMoreOption> have more employers to add
    Then I will see the <output>
    Examples:
      | employmentMoreOption | output                   |
      |                      | Select an option error   |
      | Do                   | Employer Name screen     |
      | Do not               | Upload Evidence screen   |

  @Retest-1816
  Scenario: Validate the error when user attempts to add more than 10 employers
    When I add 10 employer to my claim application
    And I Do have more employers to add
    Then I will see the More than 10 employers error
    And I will see the Error Check Employment screen