package com.nhsbsa.ihs.stepdefs;

import com.deque.html.axecore.results.Results;
import com.deque.html.axecore.results.Rule;
import com.deque.html.axecore.selenium.AxeBuilder;
import com.nhsbsa.ihs.driver.Config;
import io.cucumber.java.AfterStep;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.json.JSONObject;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Accessibility {

    private WebDriver driver;
    private static String reportPath = System.getProperty("user.dir") + File.separator + "target" + File.separator;
    String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new java.util.Date());
    String filePath = reportPath + "axeReport_" + timeStamp + ".csv";
    private static ArrayList<JSONObject> axeIssuesList = new ArrayList<>();

    public Accessibility() {
        driver = Config.getDriver();
    }

    @AfterStep(value="@Accessibility")
    public void afterStep() throws Exception {
        AxeBuilder axeBuilder = new AxeBuilder();
        Results results = axeBuilder.analyze(driver);
        String url = results.getUrl();
        List<Rule> axeIssues = results.getViolations();
        axeIssues.addAll(results.getIncomplete());
        if (axeIssues.size() == 0) {
            Assert.assertTrue("No violations found", true);
        } else {
            for (Rule rule :axeIssues) {
                JSONObject axeIssuesJson = new JSONObject();
                axeIssuesJson.put("URL", url);
                axeIssuesJson.put("Impact",rule.getImpact());
                axeIssuesJson.put("ID",rule.getId());
                axeIssuesJson.put("Description",rule.getDescription());
                axeIssuesList.add(axeIssuesJson);
            }
        }
    }

    public void createCSVReport() {
        CSVPrinter csv = null;
        try {
            csv = new CSVPrinter(new FileWriter(filePath,true), CSVFormat.EXCEL);
            csv.printRecord("URL","Impact","ID","Description");
            for(int i=0;i<axeIssuesList.size();i++) {
                ArrayList<String> nextLine = new ArrayList<>();
                nextLine.add(axeIssuesList.get(i).get("URL").toString());
                nextLine.add(axeIssuesList.get(i).get("Impact").toString());
                nextLine.add(axeIssuesList.get(i).get("ID").toString());
                nextLine.add(axeIssuesList.get(i).get("Description").toString());
                csv.printRecord(nextLine);
            }
            csv.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}