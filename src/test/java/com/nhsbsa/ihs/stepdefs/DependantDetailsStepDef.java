package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class DependantDetailsStepDef {

    private WebDriver driver;
    private DependantQuesPage dependantQuesPage;
    private DependantDetailsPage dependantDetailsPage;
    private CheckDependantsPage checkDependantsPage;

    public DependantDetailsStepDef() {
        driver = Config.getDriver();
        dependantQuesPage = new DependantQuesPage(driver);
        dependantDetailsPage = new DependantDetailsPage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
    }

    @And("^I (.*) have dependant to add$")
    public void iDependantOptionHaveDependantToAdd(String dependantOption) {
        switch (dependantOption) {
            case "do":
                dependantQuesPage.selectYesRadioButtonAndClickContinue();
                break;
            case "do not":
                dependantQuesPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                dependantQuesPage.continueButton();
                break;
        }
    }

    @And("^I will see (.*) selected$")
    public void iWillSeeAnswerSelected(String radioButton) {
        switch (radioButton) {
            case "Yes":
                Assert.assertTrue(dependantQuesPage.isYesRadioButtonSelected());
                break;
            case "none":
                Assert.assertFalse(dependantQuesPage.isYesRadioButtonSelected());
                Assert.assertFalse(dependantQuesPage.isNoRadioButtonSelected());
                break;
        }
    }

    @When("^I update my selection as (.*)$")
    public void iUpdateMySelectionAsRadioButton(String radioButton) {
        switch (radioButton) {
            case "Yes":
                dependantQuesPage.continueButton();
                break;
            case "No":
                dependantQuesPage.selectNoRadioButtonAndClickContinue();
                break;
        }
    }

    @When("^My dependant's firstname is (.*) and surname is (.*)$")
    public void myDependantSFirstnameIsGivenNameAndSurnameIsFamilyName(String depGivenName, String depFamilyName) {
        dependantDetailsPage.enterGivenName(depGivenName);
        dependantDetailsPage.enterFamilyName(depFamilyName);
    }

    @And("^My dependant's date of birth is (.*) (.*) (.*)$")
    public void myDependantSDateOfBirthIsDayMonthYear(String depDay, String depMonth, String depYear) {
        dependantDetailsPage.enterDateOfBirth(depDay, depMonth, depYear);
    }

    @And("^My dependant's IHS number is (.*)$")
    public void myDependantSIHSNumberIsIHSNumber(String depIHSNumber) {
        dependantDetailsPage.enterIHSandSubmit(depIHSNumber);
    }

    @When("^I add a dependant to my claim application$")
    public void iAddADependantToMyClaimApplication() {
        dependantQuesPage.selectYesRadioButtonAndClickContinue();
        dependantDetailsPage.enterDependantDetailsAndSubmit(DEPENDANT_GIVEN_NAME, DEPENDANT_FAMILY_NAME, DEPENDANT_DOB_DAY, DEPENDANT_DOB_MONTH, DEPENDANT_DOB_YEAR, DEPENDANT_IHS_NUMBER);
    }

    @When("^I add (.*) dependants to my claim application$")
    public void iAddCountDependantsToMyClaimApplication(int count) {
        dependantQuesPage.selectYesRadioButtonAndClickContinue();
        for (int i = 0; i < (count - 1); i++) {
            dependantDetailsPage.enterDependantDetailsAndSubmit(DEPENDANT_GIVEN_NAME, DEPENDANT_FAMILY_NAME, DEPENDANT_DOB_DAY, DEPENDANT_DOB_MONTH, DEPENDANT_DOB_YEAR, DEPENDANT_IHS_NUMBER);
            checkDependantsPage.selectYesRadioButtonAndClickContinue();
        }
        dependantDetailsPage.enterDependantDetailsAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME, UPDATED_DEPENDANT_FAMILY_NAME, DEPENDANT_DOB_DAY, DEPENDANT_DOB_MONTH, DEPENDANT_DOB_YEAR, DEPENDANT_IHS_NUMBER);
    }

    @And("^I select (.*) in Check Dependants screen$")
    public void iSelectChangeLinkInCheckDependantsScreen(String changeLink) {
        switch (changeLink) {
            case "Change Name link":
                checkDependantsPage.changeDependantName();
                break;
            case "Change Date of Birth link":
                checkDependantsPage.changeDependantDOB();
                break;
            case "Change IHS Number link":
                checkDependantsPage.changeDependantIHSNumber();
                break;
            case "Delete Dependant 1 link":
                checkDependantsPage.deleteDependant1Link();
                break;
            case "Delete Dependant 2 link":
                checkDependantsPage.deleteDependant2Link();
                break;
        }
    }

    @And("^my dependant's (.*) is pre-populated$")
    public void myDependantSAnswerIsPrePopulated(String answer) {
        switch (answer) {
            case "name":
                Assert.assertEquals(DEPENDANT_GIVEN_NAME, dependantDetailsPage.getEnteredGivenName());
                Assert.assertEquals(DEPENDANT_FAMILY_NAME, dependantDetailsPage.getEnteredFamilyName());
                break;
            case "date of birth":
                Assert.assertEquals(DEPENDANT_DOB_DAY, dependantDetailsPage.getEnteredDay());
                Assert.assertEquals(DEPENDANT_DOB_MONTH, dependantDetailsPage.getEnteredMonth());
                Assert.assertEquals(DEPENDANT_DOB_YEAR, dependantDetailsPage.getEnteredYear());
                break;
            case "IHS number":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER, dependantDetailsPage.getEnteredIHSNumber());
                break;
        }
    }

    @When("^I do not change the details and continue$")
    public void iDoNotChangeTheDetailsAndContinue() {
        dependantDetailsPage.continueButton();
    }

    @And("^my dependant's (.*) is not changed$")
    public void myDependantSParameterIsNotChanged(String parameter) {
        switch (parameter) {
            case "name":
                Assert.assertEquals(checkDependantsPage.getDisplayedName(), DEPENDANT_GIVEN_NAME + " " + DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                Assert.assertEquals(checkDependantsPage.getDisplayedDoB(), DEPENDANT_DOB_DAY + " " + DEPENDANT_DOB_FULL_MONTH + " " + DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER, checkDependantsPage.getDisplayedIHSNumber());
                break;
        }
    }

    @When("^I change the dependant's (.*) and continue$")
    public void iChangeTheDependantSAnswerAndContinue(String answer) {
        switch (answer) {
            case "name":
                dependantDetailsPage.enterNameAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME, UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                dependantDetailsPage.enterDateOfBirthAndSubmit(UPDATED_DEPENDANT_DOB_DAY, UPDATED_DEPENDANT_DOB_MONTH, UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                dependantDetailsPage.enterIHSandSubmit(UPDATED_DEPENDANT_IHS_NUMBER);
                break;
        }
    }

    @And("^my dependant's (.*) is changed$")
    public void myDependantSAnswerIsChanged(String answer) {
        switch (answer) {
            case "name":
                Assert.assertEquals(checkDependantsPage.getDisplayedName(), UPDATED_DEPENDANT_GIVEN_NAME + " " + UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                Assert.assertEquals(checkDependantsPage.getDisplayedDoB(), UPDATED_DEPENDANT_DOB_DAY + " " + UPDATED_DEPENDANT_DOB_FULL_MONTH + " " + UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                Assert.assertEquals(UPDATED_DEPENDANT_IHS_NUMBER, checkDependantsPage.getDisplayedIHSNumber());
                break;
        }
    }

    @When("^I change the dependant's answer as (.*)$")
    public void iChangeTheDependantSAnswerAsInvalid(String invalid) {
        switch (invalid) {
            case "blank name":
                dependantDetailsPage.enterNameAndSubmit("", UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "invalid name":
                dependantDetailsPage.enterNameAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME, UPDATED_DEPENDANT_FAMILY_NAME + "123");
                break;
            case "blank date of birth":
                dependantDetailsPage.enterDateOfBirthAndSubmit("", "", "");
                break;
            case "invalid date of birth":
                dependantDetailsPage.enterDateOfBirthAndSubmit("dd", "mm", "yyyy");
                break;
            case "blank IHS number":
                dependantDetailsPage.enterIHSandSubmit("");
                break;
            case "invalid IHS number":
                dependantDetailsPage.enterIHSandSubmit("1ihsc12345678");
                break;
            case "invalid range IHS number":
                dependantDetailsPage.enterIHSandSubmit("ihsc1234561");
                break;
        }
    }

    @When("^I (.*) have another dependant to add$")
    public void iDependantOptionHaveAnotherDependantToAdd(String dependantMoreOption) {
        switch (dependantMoreOption) {
            case "do":
                checkDependantsPage.selectYesRadioButtonAndClickContinue();
                break;
            case "do not":
                checkDependantsPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                checkDependantsPage.continueButton();
                break;
        }
    }

    @When("^I enter dependant details$")
    public void iEnterDependantDetails() {
        dependantDetailsPage.enterDependantDetailsAndSubmit(DEPENDANT_GIVEN_NAME, DEPENDANT_FAMILY_NAME, DEPENDANT_DOB_DAY, DEPENDANT_DOB_MONTH, DEPENDANT_DOB_YEAR, DEPENDANT_IHS_NUMBER);
    }
}


