package com.nhsbsa.ihs.stepdefs;

public class Constants {

    public static final String GIVEN_NAME = "Given Name";
    public static final String FAMILY_NAME = "Family Name";
    public static final String DOB_DAY = "01";
    public static final String DOB_MONTH = "01";
    public static final String DOB_FULL_MONTH = "January";
    public static final String DOB_YEAR = "1988";
    public static final String NINO_NUMBER = "SN123456C";
    public static final String IHS_NUMBER = "IHS123456789";
    public static final String EMAIL_ADDRESS = "ihs-testing@nhsbsa.nhs.uk";
    public static final String PHONE_NUMBER = "07123456789";
    public static final String CLAIM_START_DAY = "07";
    public static final String CLAIM_START_MONTH = "06";
    public static final String CLAIM_START_YEAR = "2020";

    public static final String UPDATED_GIVEN_NAME = "Archie";
    public static final String UPDATED_FAMILY_NAME = "A D'Russell";
    public static final String UPDATED_DOB_DAY = "31";
    public static final String UPDATED_DOB_MONTH = "12";
    public static final String UPDATED_FULL_DOB_MONTH = "December";
    public static final String UPDATED_DOB_YEAR = "1998";
    public static final String UPDATED_IHS_NUMBER = "IHSC223456788";
    public static final String UPDATED_NINO_NUMBER = "SN123456A";
    public static final String UPDATED_EMAIL_ADDRESS = "test@test.com";
    public static final String UPDATED_PHONE_NUMBER = "07814787492";
    public static final String UPDATED_CLAIM_START_DAY = "01";
    public static final String UPDATED_CLAIM_START_MONTH = "01";
    public static final String UPDATED_CLAIM_START_YEAR = "2021";

    public static final String DEPENDANT_GIVEN_NAME = "Mary";
    public static final String DEPENDANT_FAMILY_NAME = "O'hara";
    public static final String DEPENDANT_DOB_DAY = "04";
    public static final String DEPENDANT_DOB_MONTH = "11";
    public static final String DEPENDANT_DOB_FULL_MONTH = "November";
    public static final String DEPENDANT_DOB_YEAR = "1980";
    public static final String DEPENDANT_IHS_NUMBER = "IHS567876534";

    public static final String UPDATED_DEPENDANT_GIVEN_NAME = "Updated Mary";
    public static final String UPDATED_DEPENDANT_FAMILY_NAME = "Updated O'hara";
    public static final String UPDATED_DEPENDANT_DOB_DAY = "06";
    public static final String UPDATED_DEPENDANT_DOB_MONTH = "10";
    public static final String UPDATED_DEPENDANT_DOB_FULL_MONTH = "October";
    public static final String UPDATED_DEPENDANT_DOB_YEAR = "2000";
    public static final String UPDATED_DEPENDANT_IHS_NUMBER = "IHSC567877734";

    public static String FILE_UPLOAD_FILEPATH = System.getProperty("user.dir") + "/sample_uploads/";
    public static String MULTIPLE_FILES = "Sample_1.jpg,sample_2.png";

    public static final String EMPLOYER_NAME_WITH_TITLE_AND_SETTING = "Testing Services";
    public static final String JOB_TITLE_WITH_SETTING = "Tester";
    public static final String JOB_SETTING = "Hospital";
    public static final String EMPLOYER_NAME_WITH_TITLE = "MICROBIOLOGY DEPARTMENT";
    public static final String JOB_TITLE = "medical director";
    public static final String EMPLOYER_NAME_WITH_TITLE_MANDATORY = "NHS BUSINESS SERVICES AUTHORITY";
    public static final String JOB_TITLE_MANDATORY = "specialty doctor";
    public static final String UPDATED_EMPLOYER_NAME_WITH_TITLE_AND_SETTING = "HEATON HOUSE";
    public static final String UPDATED_JOB_TITLE_WITH_SETTING = "business analyst";
    public static final String UPDATED_JOB_SETTING = "GP practice";
    public static final String UPDATED_EMPLOYER_NAME_WITH_TITLE = "INFINITE JOY";
    public static final String UPDATED_JOB_TITLE_WITHOUT_SETTING = "nurse manager";
    public static final String UPDATED_EMPLOYER_NAME = "NORTH EAST AND YORKSHIRE COMMISSIONING REGION";

    public static final String EXTRA_INFORMATION = "extra information";
    public static final String UPDATED_EXTRA_INFORMATION = "updated extra information";

    public static final String START_PAGE_URL = "/apply-immigration-health-surcharge-refund/healthcare-setting";
    public static final String START_PAGE_TITLE = "Get an immigration health surcharge refund if you work in health and care: If you do not have a Tier 2 visa or you work in another health or care role - GOV.UK";

    public static final String NAME_PAGE_URL = "/health/claim/name";
    public static final String DATE_OF_BIRTH_PAGE_URL = "/health/claim/date-of-birth";
    public static final String NINO_PAGE_URL = "/health/claim/national-insurance-number";
    public static final String IHS_NUMBER_PAGE_URL = "/health/claim/immigration-health-surcharge-number";
    public static final String EMAIL_ADDRESS_PAGE_URL = "/health/claim/email";
    public static final String PHONE_NUMBER_PAGE_URL = "/health/claim/phone-number";
    public static final String DEPENDANT_QUESTION_PAGE_URL = "/health/claim/dependant-question";
    public static final String DEPENDANT_DETAILS_PAGE_URL = "/health/claim/dependant-details";
    public static final String CHECK_DEPENDANTS_PAGE_URL = "/health/claim/check-dependants";
    public static final String CLAIM_START_DATE_PAGE_URL = "/health/claim/claim-start-date";
    public static final String SUBSCRIPTION_PAGE_URL = "/health/claim/subscribe-for-reminder";
    public static final String EMPLOYER_NAME_PAGE_URL = "/health/claim/add-employer";
    public static final String JOB_TITLE_PAGE_URL = "/health/claim/job-title";
    public static final String JOB_SETTING_PAGE_URL = "/health/claim/job-setting";
    public static final String CHECK_EMPLOYMENTS_PAGE_URL = "/health/claim/check-employment";
    public static final String UPLOAD_EVIDENCE_PAGE_URL = "/health/claim/upload-evidence";
    public static final String EVIDENCE_UPLOADED_PAGE_URL = "/health/claim/attachments";
    public static final String EXTRA_INFORMATION_PAGE_URL = "/health/claim/extra-information";
    public static final String EXTRA_EVIDENCE_UPLOADED_PAGE_URL = "/health/claim/extra-evidence-uploaded";
    public static final String CHECK_YOUR_ANSWERS_PAGE_URL = "/health/claim/check-your-answers";
    public static final String DECLARATION_PAGE_URL = "/health/claim/declaration";
    public static final String APPLICATION_COMPLETED_PAGE_URL = "/health/claim/done";
    public static final String COOKIES_POLICY_PAGE_URL = "/health/help/cookies";
    public static final String ACCESSIBILITY_STATEMENT_PAGE_URL = "/health/help/accessibility-statement";
    public static final String CONTACT_US_PAGE_URL = "/health/help/contact";
    public static final String TERMS_CONDITIONS_PAGE_URL = "/health/help/terms-conditions";
    public static final String PRIVACY_NOTICE_PAGE_URL = "/health/help/privacy-notice";
    public static final String CURRENTLY_NOT_ELIGIBLE_PAGE_URL = "/health/claim/currently-not-eligible";

    public static final String NAME_PAGE_TITLE = "What's the applicant's name? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String DATE_OF_BIRTH_PAGE_TITLE = "What's the applicant's date of birth? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String NINO_PAGE_TITLE = "What's the applicant's National Insurance number? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String IHS_NUMBER_PAGE_TITLE = "What's the IHS reference number for the IHS payment the applicant is applying for a reimbursement for? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String EMAIL_ADDRESS_PAGE_TITLE = "What's the applicant's email address? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String PHONE_NUMBER_PAGE_TITLE = "What's the applicant's phone number? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String DEPENDANT_QUESTION_PAGE_TITLE = "Do you want to add any dependants who have paid an immigration health surcharge? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String DEPENDANT_DETAILS_PAGE_TITLE = "What are the applicant's dependant details? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String CHECK_DEPENDANTS_PAGE_TITLE = "Check dependant details - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String CHECK_DEPENDANTS_ERROR_PAGE_TITLE = "Error: Check dependant details - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String CLAIM_START_DATE_PAGE_TITLE = "What's the starting date of the 6-month employment period the applicant is claiming for? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String SUBSCRIPTION_PAGE_TITLE = "Would the applicant like to subscribe to get a reminder email when they may be able to reapply? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String EMPLOYER_NAME_PAGE_TITLE = "What is the name of the applicant's employer? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String JOB_TITLE_PAGE_TITLE = "What's the applicant's job title? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String JOB_SETTING_PAGE_TITLE = "What's the applicant's job setting? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String CHECK_EMPLOYMENTS_PAGE_TITLE = "Check employment details - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String ERROR_CHECK_EMPLOYMENTS_PAGE_TITLE = "Error: Check employment details - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String UPLOAD_EVIDENCE_PAGE_TITLE = "Upload evidence - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String EVIDENCE_UPLOADED_PAGE_TITLE = "Evidence uploaded - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String EVIDENCE_UPLOADED_ERROR_PAGE_TITLE = "Error: Evidence uploaded - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String EXTRA_INFORMATION_PAGE_TITLE = "Would you like to add extra information about gaps in the applicant's employment or working arrangements? (optional) - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String EXTRA_EVIDENCE_UPLOADED_PAGE_TITLE = "Extra evidence uploaded - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String CHECK_YOUR_ANSWERS_PAGE_TITLE = "Check your answers before sending your application - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String DECLARATION_PAGE_TITLE = "Declaration - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String APPLICATION_COMPLETED_PAGE_TITLE = "Application complete - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String COOKIES_POLICY_PAGE_TITLE = "Details about cookies for Apply for your immigration health surcharge reimbursement - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String ACCESSIBILITY_STATEMENT_PAGE_TITLE = "Accessibility statement for Apply for your immigration health surcharge reimbursement - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String CONTACT_US_PAGE_TITLE = "Contact us - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String TERMS_CONDITIONS_PAGE_TITLE = "Terms and conditions - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String PRIVACY_NOTICE_PAGE_TITLE = "Privacy notice - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String CURRENTLY_NOT_ELIGIBLE_PAGE_TITLE = "The applicant is not currently eligible to start a new claim - Apply for your immigration health surcharge reimbursement - GOV.UK";

    public static final String GOV_UK_PAGE = "Welcome to GOV.UK";
    public static final String HELP_PAGE = "Help using GOV.UK - Help Pages - GOV.UK";
    public static final String OPEN_GOVERNMENT_LICENCE_PAGE = "Open Government Licence";
    public static final String CROWN_COPYRIGHT_PAGE = "Crown copyright - Re-using PSI";
    public static final String GOV_UK_HEALTH_FEEDBACK_PAGE_TITLE = "Give feedback on Get an immigration health surcharge refund if you work in health and care - GOV.UK";
    public static final String GOV_UK_HEALTH_FEEDBACK_PAGE_URL = "https://www.gov.uk/done/apply-immigration-health-surcharge-refund";
    public static final String PAY_FOR_UK_HEALTHCARE_PAGE = "Pay for UK healthcare as part of your immigration application: Overview - GOV.UK";
    public static final String SNAP_SURVEY_PREVIEW_PAGE = "Survey Preview - Snap Surveys";
    public static final String HEALTH_SNAP_SURVEY_PAGE = "IHS Health and Social Care Digital Feedback";
    public static final String CONTACT_UK_VISAS_AND_IMMIGRATION_PAGE_TITLE = "Contact UK Visas and Immigration for help - GOV.UK";
    public static final String CONTACT_UK_VISAS_AND_IMMIGRATION_PAGE_URL = "https://www.gov.uk/contact-ukvi-inside-outside-uk";
}