package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;

import static com.nhsbsa.ihs.stepdefs.Constants.*;


public class CheckYourAnswersStepDef {
    private WebDriver driver;
    private CheckYourAnswersPage checkYourAnswersPage;
    private CommonPage commonPage;
    private Page page;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private NationalInsuranceNumberPage nationalInsuranceNumberPage;
    private IHSNumberPage ihsNumberPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private DependantQuesPage dependantQuesPage;
    private DependantDetailsPage dependantDetailsPage;
    private ClaimStartDatePage claimStartDatePage;
    private SubscriptionPage subscriptionPage;
    private EmployerNamePage employerNamePage;
    private JobTitlePage jobTitlePage;
    private JobSettingPage jobSettingPage;
    private CheckEmploymentsPage checkEmploymentsPage;
    private UploadFilePage uploadFilePage;
    private UploadedFilesPage uploadedFilesPage;
    private ExtraInformationPage extraInformationPage;
    private CheckDependantsPage checkDependantsPage;
    private ExtraEvidenceUploadedPage extraEvidenceUploadedPage;


    public CheckYourAnswersStepDef() {
        driver = Config.getDriver();
        checkYourAnswersPage = new CheckYourAnswersPage(driver);
        commonPage = new CommonPage(driver);
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        namePage = new NamePage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        nationalInsuranceNumberPage = new NationalInsuranceNumberPage(driver);
        page = new Page(driver);
        emailPage = new EmailPage(driver);
        dependantQuesPage = new DependantQuesPage(driver);
        dependantDetailsPage = new DependantDetailsPage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        claimStartDatePage = new ClaimStartDatePage(driver);
        subscriptionPage = new SubscriptionPage(driver);
        employerNamePage = new EmployerNamePage(driver);
        jobTitlePage = new JobTitlePage(driver);
        jobSettingPage = new JobSettingPage(driver);
        checkEmploymentsPage = new CheckEmploymentsPage(driver);
        uploadFilePage = new UploadFilePage(driver);
        uploadedFilesPage = new UploadedFilesPage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
        extraInformationPage = new ExtraInformationPage(driver);
        extraEvidenceUploadedPage = new ExtraEvidenceUploadedPage(driver);
    }

    @When("^I select (.*) on Check Your Answers page$")
    public void iSelectChangeLinkOnCheckYourAnswersPage(String changeLink) {
        switch (changeLink) {
            case "Change Name link":
                checkYourAnswersPage.claimNameChangeLink();
                break;
            case "Change Date of birth link":
                checkYourAnswersPage.claimDOBChangeLink();
                break;
            case "Change NI number link":
                checkYourAnswersPage.claimNINOChangeLink();
                break;
            case "Change IHS number link":
                checkYourAnswersPage.claimIHSNumberChangeLink();
                break;
            case "Change Phone number link":
                checkYourAnswersPage.claimPhoneNumberChangeLink();
                break;
            case "Change Email address link":
                checkYourAnswersPage.claimEmailChangeLink();
                break;
            case "Change Claim start date link":
                checkYourAnswersPage.claimStartDateChangeLink();
                break;
            case "Change Reminder Email link":
                checkYourAnswersPage.reminderEmailChangeLink();
                break;
            case "Change Employer link":
                checkYourAnswersPage.employerChangeLink();
                break;
            case "Change Employer 2 link":
                checkYourAnswersPage.employerChangeLink2();
                break;
            case "Change Employer 3 link":
                checkYourAnswersPage.employerChangeLink3();
                break;
            case "Change Job Title link":
                checkYourAnswersPage.jobTitleChangeLink();
                break;
            case "Change Job Title 2 link":
                checkYourAnswersPage.jobTitleChangeLink2();
                break;
            case "Change Job Title 3 link":
                checkYourAnswersPage.jobTitleChangeLink3();
                break;
            case "Change Job Setting link":
                checkYourAnswersPage.jobSettingChangeLink();
                break;
            case "Change Job Setting 2 link":
                checkYourAnswersPage.jobSettingChangeLink2();
                break;
            case "Change Dependant 1 link":
                checkYourAnswersPage.claimDependant1ChangeLink();
                break;
            case "Change Dependant 2 link":
                checkYourAnswersPage.claimDependant2ChangeLink();
                break;
            case "Change Dependant Question link":
                checkYourAnswersPage.claimDependentQuestionChangeLink();
                break;
            case "Change Files Added link":
                checkYourAnswersPage.claimFileAttachedChangeLink();
                break;
            case "Change Extra Information Link":
                checkYourAnswersPage.extraInformationChangeLink();
                break;
            case "Change Extra Evidence Uploaded Link":
                checkYourAnswersPage.extraEvidenceChangeLink();
                break;
        }
    }

    @When("^I continue without changing my answer on (.*)$")
    public void iContinueWithoutChangingMyAnswerOnScreen(String screen) throws InterruptedException {
        switch (screen) {
            case "Name screen":
                namePage.continueButton();
                break;
            case "Date of birth screen":
                dateOfBirthPage.continueButton();
                break;
            case "NI Number screen":
                nationalInsuranceNumberPage.continueButton();
                break;
            case "IHS Number screen":
                ihsNumberPage.continueButton();
                break;
            case "Phone number screen":
                mobileNumberPage.continueButton();
                break;
            case "Email Address screen":
                emailPage.continueButton();
                break;
            case "Dependant details screen":
                dependantDetailsPage.continueButton();
                break;
            case "Check dependants screen":
                checkDependantsPage.continueButton();
                break;
            case "Dependant question screen":
                dependantQuesPage.continueButton();
                break;
            case "Claim start date screen":
                claimStartDatePage.continueButton();
                break;
            case "Subscriptions screen":
                subscriptionPage.continueButton();
                break;
            case "Employer Name screen":
                employerNamePage.continueButton();
                break;
            case "Job Title screen":
                jobTitlePage.continueButton();
                break;
            case "Job Setting screen":
                jobSettingPage.continueButton();
                break;
            case "Check Employment screen":
                checkEmploymentsPage.continueButton();
                break;
            case "Evidence Uploaded screen":
                uploadedFilesPage.continueButton();
                break;
            case "Extra Information screen":
                extraInformationPage.continueButton();
                break;
            case "Upload Evidence screen":
                uploadFilePage.continueButton();
                break;
            case "Extra Evidence Uploaded screen":
                extraEvidenceUploadedPage.selectNoRadioButtonAndContinue();
                break;
            case "Check Your Answers screen":
                checkYourAnswersPage.continueButton();
                break;
        }
    }

    @And("^My answer remains same as on (.*)$")
    public void myAnswerRemainsSameAsOnScreen(String screen) {
        switch (screen) {
            case "Name screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantName(), GIVEN_NAME + " " + FAMILY_NAME);
                break;
            case "Date of birth screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantDOB(), DOB_DAY + " " + DOB_FULL_MONTH + " " + DOB_YEAR);
                break;
            case "NI Number screen":
                Assert.assertEquals(NINO_NUMBER, checkYourAnswersPage.getApplicantNINONumber());
                break;
            case "IHS Number screen":
                Assert.assertEquals(IHS_NUMBER, checkYourAnswersPage.getApplicantIHSNumber());
                break;
            case "Email Address screen":
                Assert.assertEquals(EMAIL_ADDRESS, checkYourAnswersPage.getApplicantEmail());
                break;
            case "Phone number screen":
                Assert.assertEquals("-", checkYourAnswersPage.getApplicantPhoneNumber());
                break;
            case "Claim start date screen":
                Assert.assertTrue(checkYourAnswersPage.getApplicantClaimStartingDate().contains(CLAIM_START_DAY));
                Assert.assertTrue(checkYourAnswersPage.getApplicantClaimStartingDate().contains(CLAIM_START_YEAR));
                break;
            case "Subscriptions screen":
                Assert.assertEquals("Yes", checkYourAnswersPage.getApplicantReminderEmail());
                break;
            case "Employer Name screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantEmployerDetails(), EMPLOYER_NAME_WITH_TITLE_AND_SETTING + " " + JOB_TITLE_WITH_SETTING + " " + JOB_SETTING);
                break;
            case "Dependant question screen":
                Assert.assertEquals("No", checkYourAnswersPage.getDependantQues());
                break;
            case "Evidence Uploaded screen":
                Assert.assertTrue(checkYourAnswersPage.getUploadedFile1().contains("payslip4"));
                break;
            case "Extra Information screen":
                Assert.assertEquals(EXTRA_INFORMATION, checkYourAnswersPage.getExtraInformationDisplayed());
                break;
            case "Upload Evidence screen":
                Assert.assertTrue(checkYourAnswersPage.getUploadedFiles().containsAll(Arrays.asList("Sample_1.jpg", "sample_2.png")));
                break;
            case "Extra Evidence Uploaded screen":
                Assert.assertTrue(checkYourAnswersPage.getExtraEvidenceUploaded().containsAll(Arrays.asList("Sample_1.jpg")));
                break;
        }
    }

    @And("^My claimant's (.*) is pre-populated$")
    public void myClaimantSAnswerIsPrePopulated(String answer) {
        switch (answer) {
            case "Name":
                Assert.assertEquals(GIVEN_NAME, namePage.getEnteredGivenName());
                Assert.assertEquals(FAMILY_NAME, namePage.getEnteredFamilyName());
                break;
            case "Date of birth":
                Assert.assertEquals(DOB_DAY, dateOfBirthPage.getEnteredDay());
                Assert.assertEquals(DOB_MONTH, dateOfBirthPage.getEnteredMonth());
                Assert.assertEquals(DOB_YEAR, dateOfBirthPage.getEnteredYear());
                break;
            case "IHS Number":
                Assert.assertEquals(IHS_NUMBER, ihsNumberPage.getEnteredIHSNumber());
                break;
            case "NI Number":
                Assert.assertEquals(NINO_NUMBER, nationalInsuranceNumberPage.getEnteredNINO());
                break;
            case "Email":
                Assert.assertEquals(EMAIL_ADDRESS, emailPage.getEnteredEmail());
                break;
            case "Phone Number":
                Assert.assertEquals("", mobileNumberPage.getEnteredPhoneNumber());
                break;
            case "Claim start date":
                Assert.assertEquals(claimStartDatePage.getEnteredDate(), CLAIM_START_DAY + CLAIM_START_MONTH + CLAIM_START_YEAR);
                break;
            case "Subscription":
                Assert.assertTrue(subscriptionPage.isYesRadioButtonSelected());
                break;
            case "Employer Name with title and setting":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE_AND_SETTING, employerNamePage.getEnteredEmployerName());
                break;
            case "Job title with setting":
                Assert.assertEquals(JOB_TITLE_WITH_SETTING, jobTitlePage.getEnteredJobTitle());
                break;
            case "Job setting":
                Assert.assertTrue(jobSettingPage.getEnteredJobSetting().equalsIgnoreCase(JOB_SETTING));
                break;
            case "Employer Name with title":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE, employerNamePage.getEnteredEmployerName());
                break;
            case "Employer Name":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE_MANDATORY, employerNamePage.getEnteredEmployerName());
                break;
            case "Evidence details":
                Assert.assertTrue(uploadedFilesPage.isNoRadioButtonSelected());
                Assert.assertTrue(uploadedFilesPage.getUploadedFile().contains("payslip4"));
                break;
            case "Extra Information":
                Assert.assertEquals(EXTRA_INFORMATION, extraInformationPage.getEnteredExtraInformation());
                break;
            case "Multi File":
                Assert.assertTrue(uploadFilePage.isFilesAddedHeadingDisplayed());
                Assert.assertTrue(uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(MULTIPLE_FILES));
                break;
            case "Extra Evidence":
                Assert.assertTrue(extraEvidenceUploadedPage.isNoRadioButtonSelected());
                Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains("Sample_1.jpg"));
                break;
            case "Extra Evidences":
                Assert.assertTrue(extraEvidenceUploadedPage.isNoRadioButtonSelected());
                Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains("New-payslip_No1.png"));
                Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains("New-payslip_No2.png"));
                Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains("New-payslip_No0.png"));
                break;
        }
    }

    @When("^I change my (.*) and continue$")
    public void iChangeMyAnswerAndContinue(String answer) {
        switch (answer) {
            case "Name":
                namePage.enterNameAndSubmit(UPDATED_GIVEN_NAME, UPDATED_FAMILY_NAME);
                break;
            case "Date of birth":
                dateOfBirthPage.enterDateOfBirthAndSubmit(UPDATED_DOB_DAY, UPDATED_DOB_MONTH, UPDATED_DOB_YEAR);
                break;
            case "NI Number":
                nationalInsuranceNumberPage.enterNINOAndSubmit(UPDATED_NINO_NUMBER);
                break;
            case "IHS Number":
                ihsNumberPage.enterIHSNumberAndSubmit(UPDATED_IHS_NUMBER);
                break;
            case "Email":
                emailPage.enterEmailAndSubmit(UPDATED_EMAIL_ADDRESS);
                break;
            case "Phone Number":
                mobileNumberPage.enterMobileNumberAndSubmit(UPDATED_PHONE_NUMBER);
                break;
            case "Dependant Details":
                dependantQuesPage.selectYesRadioButtonAndClickContinue();
                break;
            case "Claim start date":
                claimStartDatePage.enterClaimStartDateAndSubmit(UPDATED_CLAIM_START_DAY, UPDATED_CLAIM_START_MONTH, UPDATED_CLAIM_START_YEAR);
                break;
            case "Subscription":
                subscriptionPage.selectNoRadioButtonAndClickContinue();
                break;
            case "Employer Name":
            case "Employer2":
                employerNamePage.enterEmployerAndSubmit(UPDATED_EMPLOYER_NAME);
                break;
            case "Employer to Employer with Title":
                employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE);
                break;
            case "Employer to Employer with Title and Setting":
                employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
                break;
            case "Employer1":
                employerNamePage.enterEmployerAndSubmit("108 DENTAL SURGERY");
                break;
            case "Employer3":
                employerNamePage.enterEmployerAndSubmit("BOOTS UK LIMITED");
                break;
            case "Evidence details":
                uploadedFilesPage.selectYesRadioButtonAndContinue();
                break;
            case "Extra Information":
                extraInformationPage.enterExtraInformationAndContinue(UPDATED_EXTRA_INFORMATION);
                break;
            case "Extra Evidence":
                extraEvidenceUploadedPage.selectYesRadioButtonAndContinue();
                break;
        }
    }

    @And("^I see the updated (.*) on Check Your Answers screen$")
    public void iSeeTheUpdatedAnswersOnCheckYourAnswersScreen(String answer) {
        switch (answer) {
            case "Name":
                Assert.assertEquals(checkYourAnswersPage.getApplicantName(), UPDATED_GIVEN_NAME + " " + UPDATED_FAMILY_NAME);
                break;
            case "Date of birth":
                Assert.assertEquals(checkYourAnswersPage.getApplicantDOB(), UPDATED_DOB_DAY + " " + UPDATED_FULL_DOB_MONTH + " " + UPDATED_DOB_YEAR);
                break;
            case "NI Number":
                Assert.assertEquals(UPDATED_NINO_NUMBER, checkYourAnswersPage.getApplicantNINONumber());
                break;
            case "IHS Number":
                Assert.assertEquals(UPDATED_IHS_NUMBER, checkYourAnswersPage.getApplicantIHSNumber());
                break;
            case "Email":
                Assert.assertEquals(UPDATED_EMAIL_ADDRESS, checkYourAnswersPage.getApplicantEmail());
                break;
            case "Phone Number":
                Assert.assertEquals(UPDATED_PHONE_NUMBER, checkYourAnswersPage.getApplicantPhoneNumber());
                break;
            case "Dependant Name":
                Assert.assertEquals(checkYourAnswersPage.getDependantName(), UPDATED_DEPENDANT_GIVEN_NAME + " " + UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "Dependant Date of Birth":
                Assert.assertEquals(checkYourAnswersPage.getDependantDob(), UPDATED_DEPENDANT_DOB_DAY + " " + UPDATED_DEPENDANT_DOB_FULL_MONTH + " " + UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "Dependant IHS number":
                Assert.assertEquals(UPDATED_DEPENDANT_IHS_NUMBER, checkYourAnswersPage.getDependantIHSNumber());
                break;
            case "Claim start date":
                Assert.assertTrue(checkYourAnswersPage.getApplicantClaimStartingDate().contains(UPDATED_CLAIM_START_DAY));
                Assert.assertTrue(checkYourAnswersPage.getApplicantClaimStartingDate().contains(UPDATED_CLAIM_START_YEAR));
                break;
            case "Subscription":
                Assert.assertEquals("No", checkYourAnswersPage.getApplicantReminderEmail());
                break;
            case "Employer Name":
                Assert.assertEquals(UPDATED_EMPLOYER_NAME, checkYourAnswersPage.getEmployerName());
                break;
            case "Employer Name with Title":
                Assert.assertEquals(checkYourAnswersPage.getApplicantEmployerDetails(), EMPLOYER_NAME_WITH_TITLE + " " + JOB_TITLE);
                break;
            case "Employer Name with Title and Setting":
                Assert.assertEquals(checkYourAnswersPage.getApplicantEmployerDetails(), EMPLOYER_NAME_WITH_TITLE_AND_SETTING + " " + JOB_TITLE_WITH_SETTING + " " + UPDATED_JOB_SETTING);
                break;
            case "Employer and newly added employer":
                Assert.assertEquals(checkYourAnswersPage.getApplicantEmployerDetails(), UPDATED_EMPLOYER_NAME + " " + JOB_TITLE_MANDATORY + " " + "JOIN HANDS CARE" + " " + "social care manager");
                break;
            case "Employer1":
                Assert.assertTrue(checkYourAnswersPage.getApplicantEmployerDetails().contains("108 DENTAL SURGERY" + " " + JOB_TITLE));
                break;
            case "Employer3":
                Assert.assertTrue(checkYourAnswersPage.getApplicantEmployerDetails().contains("BOOTS UK LIMITED" + " " + JOB_TITLE_WITH_SETTING + " " + UPDATED_JOB_SETTING));
                break;
            case "Employer2":
                Assert.assertTrue(checkYourAnswersPage.getApplicantEmployerDetails().contains("JOIN HANDS CARE" + " " + JOB_TITLE_WITH_SETTING + " " + UPDATED_JOB_SETTING));
                break;
            case "Employer":
                Assert.assertEquals(checkYourAnswersPage.getApplicantEmployerDetails(), "JOIN HANDS CARE" + " " + "social care manager");
                break;
            case "Employers":
                Assert.assertEquals(checkYourAnswersPage.getApplicantEmployerDetails(), EMPLOYER_NAME_WITH_TITLE_MANDATORY + " " + JOB_TITLE_MANDATORY + " " + UPDATED_EMPLOYER_NAME + " " + JOB_TITLE);
                break;
            case "multiple employers":
                Assert.assertEquals(checkYourAnswersPage.getApplicantEmployerDetails(), "108 DENTAL SURGERY" + " " + JOB_TITLE_WITH_SETTING + " " + UPDATED_JOB_SETTING + " " + UPDATED_EMPLOYER_NAME + " " + JOB_TITLE_WITH_SETTING + " " + JOB_SETTING + " " + EMPLOYER_NAME_WITH_TITLE + " " + JOB_TITLE);
                break;
            case "Evidence details":
                Assert.assertTrue(checkYourAnswersPage.getUploadedFile1().contains("payslip4"));
                Assert.assertTrue(checkYourAnswersPage.getUploadedFile2().contains("Payslip-June"));
                break;
            case "Extra Information":
                Assert.assertEquals(UPDATED_EXTRA_INFORMATION, checkYourAnswersPage.getExtraInformationDisplayed());
                break;
            case "Multi File":
                Assert.assertTrue(checkYourAnswersPage.getUploadedFiles().containsAll(Arrays.asList("Sample_1.jpg", "sample_2.png", "sample_3.pdf", "Sample_4.bmp")));
                break;
            case "Files":
                Assert.assertTrue(checkYourAnswersPage.getUploadedFiles().containsAll(Arrays.asList("123456.pdf", "Payslip_July.bmp")));
                break;
            case "Evidence":
                Assert.assertTrue(checkYourAnswersPage.getUploadedFile1().contains("Payslip-June"));
                break;
            case "Extra Evidences":
                Assert.assertTrue(checkYourAnswersPage.getExtraEvidenceUploaded().containsAll(Arrays.asList("Sample_1.jpg", "Payslip-June.png")));
                break;
            case "Extra Evidence":
                Assert.assertTrue(checkYourAnswersPage.getExtraEvidenceUploaded().contains("Payslip-June.png"));
                break;
        }
    }

    @And("^I change my (.*) and do not continue$")
    public void iChangeMyAnswerAndDoNotContinue(String answer) {
        switch (answer) {
            case "Name":
                namePage.enterName(UPDATED_GIVEN_NAME, UPDATED_FAMILY_NAME);
                break;
            case "Date of birth":
                dateOfBirthPage.enterDateOfBirth(UPDATED_DOB_DAY, UPDATED_DOB_MONTH, UPDATED_DOB_YEAR);
                break;
            case "NI Number":
                nationalInsuranceNumberPage.enterNINO(UPDATED_NINO_NUMBER);
                break;
            case "IHS Number":
                ihsNumberPage.enterIHSNumber(UPDATED_IHS_NUMBER);
                break;
            case "Email":
                emailPage.enterEmail(UPDATED_EMAIL_ADDRESS);
                break;
            case "Phone Number":
                mobileNumberPage.enterMobileNumber(UPDATED_PHONE_NUMBER);
                break;
            case "Claim start date":
                claimStartDatePage.enterClaimStartDate(UPDATED_CLAIM_START_DAY, UPDATED_CLAIM_START_MONTH, UPDATED_CLAIM_START_YEAR);
                break;
            case "Subscription":
                subscriptionPage.selectNoRadioButton();
                break;
            case "Employer Name with title and setting":
                employerNamePage.enterEmployerName("Test");
                break;
            case "Extra Information":
                extraInformationPage.enterExtraInformation(UPDATED_EXTRA_INFORMATION);
                break;
        }
    }

    @When("^I change the answer to (.*)$")
    public void iChangeTheAnswerToInvalidAnswer(String invalidAnswer) {
        switch (invalidAnswer) {
            case "Blank Given Name":
                namePage.enterNameAndSubmit("", UPDATED_FAMILY_NAME);
                break;
            case "Blank Family Name":
                namePage.enterNameAndSubmit(UPDATED_GIVEN_NAME, "");
                break;
            case "Invalid Given Name":
                namePage.enterNameAndSubmit("(first name)", UPDATED_FAMILY_NAME);
                break;
            case "Blank Date of birth":
                dateOfBirthPage.enterDateOfBirthAndSubmit("", "", "");
                break;
            case "Invalid Date of birth":
                dateOfBirthPage.enterDateOfBirthAndSubmit("32", "13", UPDATED_DOB_YEAR);
                break;
            case "Blank IHS Number":
                ihsNumberPage.enterIHSNumberAndSubmit("");
                break;
            case "Invalid IHS Number":
                ihsNumberPage.enterIHSNumberAndSubmit("abc12345656abc");
                break;
            case "Invalid range IHS Number":
                ihsNumberPage.enterIHSNumberAndSubmit("ihsc1234567");
                break;
            case "Blank NI Number":
                nationalInsuranceNumberPage.enterNINOAndSubmit("");
                break;
            case "Invalid NI Number":
                nationalInsuranceNumberPage.enterNINOAndSubmit("abc12345656ab");
                break;
            case "Blank Email":
                emailPage.enterEmailAndSubmit("");
                break;
            case "Invalid Email":
                emailPage.enterEmailAndSubmit("testemail");
                break;
            case "Invalid Phone Number":
                mobileNumberPage.enterMobileNumberAndSubmit("*7653456789");
                break;
            case "Blank Claim Start Date":
                claimStartDatePage.enterClaimStartDateAndSubmit("", "", "");
                break;
            case "Invalid Claim Start Date":
                claimStartDatePage.enterClaimStartDateAndSubmit("31", "02", "2021");
                break;
            case "Blank Employer Name":
                employerNamePage.enterEmployerAndSubmit("");
                break;
            case "Invalid Employer Name":
                employerNamePage.enterEmployerAndSubmit("ab");
                break;
            case "Invalid Extra Information":
                extraInformationPage.enterExtraInformationAndContinue("a#bc");
                break;
        }
    }

    @When("^I am navigated to the (.*) and continue$")
    public void iAmNavigateToScreenAndContinue(String screen) {
        switch (screen) {
            case "Job Title screen-enter Job title":
                jobTitlePage.enterJobTitle(JOB_TITLE);
                break;
            case "Job Title screen-enter Job title with setting":
                jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
                break;
            case "Job Setting screen-select GP":
                jobSettingPage.selectGPPracticeAndContinue();
                break;
            case "Check Employment screen-select No":
                checkEmploymentsPage.selectNoAndContinue();
                break;
            case "Check Employment screen-select yes":
                checkEmploymentsPage.selectYesAndContinue();
                break;
        }
    }

    @And("^My dependant answer is pre-populated$")
    public void myDependantAnswerIsPrePopulated() {
        Assert.assertTrue(dependantQuesPage.isNoRadioButtonSelected());
    }

    @And("^I see No value under dependant section for dependants$")
    public void ISeeNoValueUnderDependantSectionForDependants() {
        Assert.assertEquals("No", checkYourAnswersPage.getDependantQues());
    }

    @When("^I add a dependant and continue$")
    public void iAddADependantAndContinue() {
        dependantDetailsPage.enterDependantDetailsAndSubmit(DEPENDANT_GIVEN_NAME, DEPENDANT_FAMILY_NAME, DEPENDANT_DOB_DAY, DEPENDANT_DOB_MONTH, DEPENDANT_DOB_YEAR, DEPENDANT_IHS_NUMBER);
    }

    @And("^My dependant (.*) remains same as on Dependant details screen$")
    public void myDependantAnswerRemainsSameAsOnDependantDetailsScreen(String answer) {
        switch (answer) {
            case "name":
                Assert.assertEquals(checkYourAnswersPage.getDependantName(), DEPENDANT_GIVEN_NAME + " " + DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                Assert.assertEquals(checkYourAnswersPage.getDependantDob(), DEPENDANT_DOB_DAY + " " + DEPENDANT_DOB_FULL_MONTH + " " + DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER, checkYourAnswersPage.getDependantIHSNumber());
                break;
        }
    }

    @When("^I add another dependant and continue$")
    public void iAddAnotherDependantAndContinue() {
        checkDependantsPage.selectYesRadioButtonAndClickContinue();
        dependantDetailsPage.enterDependantDetailsAndSubmit(DEPENDANT_GIVEN_NAME, DEPENDANT_FAMILY_NAME, DEPENDANT_DOB_DAY, DEPENDANT_DOB_MONTH, DEPENDANT_DOB_YEAR, DEPENDANT_IHS_NUMBER);
    }

    @When("^I change the dependant (.*) and continue$")
    public void iChangeTheDependantAnswerAndContinue(String answer) {
        switch (answer) {
            case "name":
                dependantDetailsPage.enterNameAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME, UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                dependantDetailsPage.enterDateOfBirthAndSubmit(UPDATED_DEPENDANT_DOB_DAY, UPDATED_DEPENDANT_DOB_MONTH, UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                dependantDetailsPage.enterIHSandSubmit(UPDATED_DEPENDANT_IHS_NUMBER);
                break;
        }
    }

    @And("^I will see updated dependant (.*) on Check your answers page$")
    public void iWillSeeUpdatedDependantAnswerOnCheckYourAnswersPage(String answer) {
        switch (answer) {
            case "name":
                Assert.assertEquals(checkYourAnswersPage.getDependantName(), DEPENDANT_GIVEN_NAME + " " + DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                Assert.assertEquals(checkYourAnswersPage.getDependantDob(), DEPENDANT_DOB_DAY + " " + DEPENDANT_DOB_FULL_MONTH + " " + DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER, checkYourAnswersPage.getDependantIHSNumber());
                break;
            case "name 2":
                Assert.assertEquals(checkYourAnswersPage.getDependantName2(), DEPENDANT_GIVEN_NAME + " " + DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth 2":
                Assert.assertEquals(checkYourAnswersPage.getDependantDob2(), DEPENDANT_DOB_DAY + " " + DEPENDANT_DOB_FULL_MONTH + " " + DEPENDANT_DOB_YEAR);
                break;
            case "IHS number 2":
                Assert.assertEquals(DEPENDANT_IHS_NUMBER, checkYourAnswersPage.getDependantIHSNumber2());
                break;
            case "updated name 2":
                Assert.assertEquals(checkYourAnswersPage.getDependantName2(), UPDATED_DEPENDANT_GIVEN_NAME + " " + UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "updated date of birth 2":
                Assert.assertEquals(checkYourAnswersPage.getDependantDob2(), UPDATED_DEPENDANT_DOB_DAY + " " + UPDATED_DEPENDANT_DOB_FULL_MONTH + " " + UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "updated IHS number 2":
                Assert.assertEquals(UPDATED_DEPENDANT_IHS_NUMBER, checkYourAnswersPage.getDependantIHSNumber2());
                break;
        }
    }

    @And("^I will see (.*) is deleted on Check your answers page$")
    public void iWillSeeDependantIsDeletedOnCheckYourAnswersPage(String dependant) {
        switch (dependant) {
            case "Dependant 1":
                Assert.assertFalse(checkYourAnswersPage.isDependant1Visible());
                break;
            case "Dependant 2":
                Assert.assertFalse(checkYourAnswersPage.isDependant2Visible());
                break;
        }
    }

    @And("^I will see (.*) files under (.*) section$")
    public void iWillSeeCountFilesUnderSectionSection(int count, String section) {
        switch (section) {
            case "Files Added":
                Assert.assertEquals(checkYourAnswersPage.getFilesAddedCount(), count);
                break;
            case "Extra Evidence":
                Assert.assertEquals(checkYourAnswersPage.getExtraEvidenceCount(), count);
                break;
        }
    }
}
