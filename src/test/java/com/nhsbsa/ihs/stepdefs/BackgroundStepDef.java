package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import com.nhsbsa.ihs.utilities.ConfigReader;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.And;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class BackgroundStepDef {

    private WebDriver driver;
    private String baseUrl;
    private String validationUrl;
    private Page page;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private NationalInsuranceNumberPage nationalInsuranceNumberPage;
    private IHSNumberPage ihsNumberPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private DependantQuesPage dependantQuesPage;
    private DependantDetailsPage dependantDetailsPage;
    private CheckDependantsPage checkDependantsPage;
    private ClaimStartDatePage claimStartDatePage;
    private SubscriptionPage subscriptionPage;
    private EmployerNamePage employerNamePage;
    private JobTitlePage jobTitlePage;
    private JobSettingPage jobSettingPage;
    private CheckEmploymentsPage checkEmploymentsPage;
    private UploadFilePage uploadFilePage;
    private UploadedFilesPage uploadedFilesPage;
    private ExtraInformationPage extraInformationPage;
    private ExtraEvidenceUploadedPage extraEvidenceUploadedPage;

    public BackgroundStepDef() {
        driver = Config.getDriver();
        baseUrl = ConfigReader.getEnvironment();
        mobileNumberPage = new MobileNumberPage(driver);
        namePage = new NamePage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        nationalInsuranceNumberPage = new NationalInsuranceNumberPage(driver);
        page = new Page(driver);
        emailPage = new EmailPage(driver);
        dependantQuesPage = new DependantQuesPage(driver);
        dependantDetailsPage = new DependantDetailsPage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
        claimStartDatePage = new ClaimStartDatePage(driver);
        subscriptionPage = new SubscriptionPage(driver);
        employerNamePage = new EmployerNamePage(driver);
        jobTitlePage = new JobTitlePage(driver);
        jobSettingPage = new JobSettingPage(driver);
        checkEmploymentsPage = new CheckEmploymentsPage(driver);
        uploadFilePage = new UploadFilePage(driver);
        uploadedFilesPage = new UploadedFilesPage(driver);
        extraInformationPage = new ExtraInformationPage(driver);
        extraEvidenceUploadedPage = new ExtraEvidenceUploadedPage(driver);
    }

    @Given("^I launch the IHS Health claim application$")
    public void iLaunchTheIHSHealthClaimApplication() {
        driver.manage().deleteAllCookies();
        new Page(driver).navigateToUrl(baseUrl);
    }

    @And("^I submit the page with validation date (.*)$")
    public void iSubmitThePageWithValidationDateValidationDate(String validationDate) {
        validationUrl = ConfigReader.getValidationUrl(validationDate);
        driver.get(validationUrl);
    }

    @And("^My details are captured until Date of birth screen$")
    public void myDetailsAreCapturedUntilDateOfBirthScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
    }

    @And("^My details are captured until NI Number screen$")
    public void myDetailsAreCapturedUntilNINumberScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
    }

    @And("^My details are captured until IHS Number screen$")
    public void myDetailsAreCapturedUntilIHSNumberScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
    }

    @And("^My details are captured until Email Address screen$")
    public void myDetailsAreCapturedUntilEmailAddressScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
    }

    @And("^My details are captured until Phone Number screen$")
    public void myDetailsAreCapturedUntilPhoneNumberScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
    }

    @And("^My details are captured until Dependant Question screen$")
    public void myDetailsAreCapturedUntilDependantQuestionScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
    }

    @And("^My details are captured until Dependant Details screen$")
    public void myDetailsAreCapturedUntilDependantDetailsScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Check Dependants screen$")
    public void myDetailsAreCapturedUntilCheckDependantsScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectYesRadioButtonAndClickContinue();
        dependantDetailsPage.enterDependantDetailsAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME,DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR,DEPENDANT_IHS_NUMBER);
    }

    @And("^My details are captured until Claim start date screen$")
    public void myDetailsAreCapturedUntilClaimStartDateScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Subscription screen$")
    public void myDetailsAreCapturedUntilSubscriptionScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
    }

    @And("^My details are captured until Employer Name screen$")
    public void myDetailsAreCapturedUntilEmployerNameScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME, FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY, DOB_MONTH, DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Job Title screen$")
    public void myDetailsAreCapturedUntilJobTitleScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit("ABICA LIMITED");
    }

    @And("^My details are captured until Job Setting screen$")
    public void myDetailsAreCapturedUntilJobSettingScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit("ABICA LIMITED");
        jobTitlePage.enterJobTitle("Family Therapist");
    }

    @And("^My details are captured until Check Employment Details screen with Employer Job Title and Settings$")
    public void myDetailsAreCapturedUntilCheckEmploymentScreenWithEmployerJobTitleAndSettings() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
        jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
        jobSettingPage.selectHospitalAndContinue();
    }

    @And("^My details are captured until Check Employment Details screen with Employer Name and Job Title$")
    public void myDetailsAreCapturedUntilCheckEmploymentScreenWithEmployerNameAndJobTitle() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE);
        jobTitlePage.enterJobTitle(JOB_TITLE);
    }

    @And("^My details are captured until Check Employment Details screen with Employer Name and mandatory Job Title$")
    public void myDetailsAreCapturedUntilCheckEmploymentScreenWithEmployerNameAndMandatoryJobTitle() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_MANDATORY);
        jobTitlePage.enterJobTitle(JOB_TITLE_MANDATORY);
    }

    @And("^My details are captured until Upload Evidence screen$")
    public void myDetailsAreCapturedUntilUploadEvidenceScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit("Test Employer");
        jobTitlePage.enterJobTitle("Test Job Title");
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectNoAndContinue();
    }

    @And("^My details are captured until Extra Information screen$")
    public void myDetailsAreCapturedUntilExtraInformationScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit("Test Employer");
        jobTitlePage.enterJobTitle("Test Job Title");
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
        uploadedFilesPage.selectNoRadioButtonAndContinue();
    }

    @And("^My details are captured until Check your answers screen$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
        jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
        uploadedFilesPage.selectNoRadioButtonAndContinue();
        extraInformationPage.enterExtraInformationAndContinue(EXTRA_INFORMATION);
    }

    @And("My details are captured until Check your answers screen with Dependant")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenWithDependant() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        dependantQuesPage.selectYesRadioButtonAndClickContinue();
        dependantDetailsPage.enterDependantDetailsAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME,DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR,DEPENDANT_IHS_NUMBER);
        checkDependantsPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
        jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
        uploadedFilesPage.selectNoRadioButtonAndContinue();
        extraInformationPage.enterExtraInformationAndContinue(EXTRA_INFORMATION);
    }

    @And("My details are captured until Check your answers screen with Dependant - Multi File")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenWithDependantMultiFile() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        dependantQuesPage.selectYesRadioButtonAndClickContinue();
        dependantDetailsPage.enterDependantDetailsAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME,DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR,DEPENDANT_IHS_NUMBER);
        checkDependantsPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
        jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.uploadMultipleFiles(MULTIPLE_FILES);
        uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(MULTIPLE_FILES);
        uploadFilePage.continueButton();
        extraInformationPage.enterExtraInformationAndContinue(EXTRA_INFORMATION);
    }

    @And("^My details are captured until Check Your Answer screen with multiple dependants$")
    public void myDetailsAreCapturedUntilCheckYourAnswerScreenWithMultipleDependants() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        dependantQuesPage.selectYesRadioButtonAndClickContinue();
        dependantDetailsPage.enterDependantDetailsAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME,DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR,DEPENDANT_IHS_NUMBER);
        checkDependantsPage.selectYesRadioButtonAndClickContinue();
        dependantDetailsPage.enterDependantDetailsAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME,DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR,DEPENDANT_IHS_NUMBER);
        checkDependantsPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
        jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.uploadMultipleFiles(MULTIPLE_FILES);
        uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(MULTIPLE_FILES);
        uploadFilePage.continueButton();
        extraInformationPage.enterExtraInformationAndContinue(EXTRA_INFORMATION);
    }

    @And("^My details are captured until Check your answers screen - Employer with mandatory Title$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenEmployerWithMandatoryTitle() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_MANDATORY);
        jobTitlePage.enterJobTitle(JOB_TITLE_MANDATORY);
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.uploadMultipleFiles(MULTIPLE_FILES);
        uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(MULTIPLE_FILES);
        uploadFilePage.continueButton();
        extraInformationPage.enterExtraInformationAndContinue(EXTRA_INFORMATION);
    }

    @And("^My details are captured until Check your answers screen - Multiple Employers$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenMultipleEmployers() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_MANDATORY);
        jobTitlePage.enterJobTitle(JOB_TITLE_MANDATORY);
        checkEmploymentsPage.selectYesAndContinue();
        employerNamePage.enterEmployerAndSubmit("JOIN HANDS CARE");
        jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectYesAndContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE);
        jobTitlePage.enterJobTitle(JOB_TITLE);
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.uploadMultipleFiles(MULTIPLE_FILES);
        uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(MULTIPLE_FILES);
        uploadFilePage.continueButton();
        extraInformationPage.enterExtraInformationAndContinue(EXTRA_INFORMATION);
    }

    @And("^My details are captured until Check your answers screen - MultiFile$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenMultiFile() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
        jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.uploadMultipleFiles(MULTIPLE_FILES);
        uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(MULTIPLE_FILES);
        uploadFilePage.continueButton();
        extraInformationPage.enterExtraInformationAndContinue(EXTRA_INFORMATION);
    }

    @And("^My details are captured until Extra Information screen - MultiFile$")
    public void myDetailsAreCapturedUntilExtraInformationScreenMultiFile() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
        jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.uploadMultipleFiles(MULTIPLE_FILES);
        uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(MULTIPLE_FILES);
        uploadFilePage.continueButton();
    }

    @And("^My details are captured until Check your answers screen with extra evidence$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenWithExtraEvidence() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
        jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
        uploadedFilesPage.selectNoRadioButtonAndContinue();
        extraInformationPage.enterExtraInformation(EXTRA_INFORMATION);
        extraInformationPage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        extraEvidenceUploadedPage.selectNoRadioButtonAndContinue();
    }
    @And("^My details are captured until Check your answers screen with extra evidence - MultiFile$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenWithExtraEvidenceMultiFile() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        nationalInsuranceNumberPage.enterNINOAndSubmit(NINO_NUMBER);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.continueButton();
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        claimStartDatePage.enterClaimStartDateAndSubmit(CLAIM_START_DAY,CLAIM_START_MONTH,CLAIM_START_YEAR);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
        jobTitlePage.enterJobTitle(JOB_TITLE_WITH_SETTING);
        jobSettingPage.selectHospitalAndContinue();
        checkEmploymentsPage.selectNoAndContinue();
        uploadFilePage.uploadMultipleFiles(MULTIPLE_FILES);
        uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(MULTIPLE_FILES);
        uploadFilePage.continueButton();
        extraInformationPage.enterExtraInformation(EXTRA_INFORMATION);
        extraInformationPage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        extraEvidenceUploadedPage.selectNoRadioButtonAndContinue();
    }
}
