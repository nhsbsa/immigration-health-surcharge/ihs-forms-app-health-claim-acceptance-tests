package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

import static com.nhsbsa.ihs.stepdefs.Constants.*;
import static org.hamcrest.CoreMatchers.is;

public class ClaimDetailsStepDef {
    private WebDriver driver;
    private CommonPage commonPage;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private NationalInsuranceNumberPage ninoPage;
    private IHSNumberPage ihsNumberPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private ClaimStartDatePage claimStartDatePage;
    private SubscriptionPage subscriptionPage;
    private EmployerNamePage employerNamePage;
    private JobTitlePage jobTitlePage;
    private CheckEmploymentsPage checkEmploymentsPage;
    private UploadFilePage uploadFilePage;
    private UploadedFilesPage uploadedFilesPage;
    private ExtraInformationPage extraInformationPage;
    private CheckYourAnswersPage checkYourAnswersPage;

    public ClaimDetailsStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ninoPage = new NationalInsuranceNumberPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        claimStartDatePage = new ClaimStartDatePage(driver);
        subscriptionPage = new SubscriptionPage(driver);
        employerNamePage = new EmployerNamePage(driver);
        jobTitlePage = new JobTitlePage(driver);
        checkEmploymentsPage = new CheckEmploymentsPage(driver);
        uploadFilePage = new UploadFilePage(driver);
        uploadedFilesPage = new UploadedFilesPage(driver);
        extraInformationPage = new ExtraInformationPage(driver);
        checkYourAnswersPage = new CheckYourAnswersPage(driver);
    }

    @And("^My firstname is (.*) and surname is (.*)$")
    public void myFirstnameIsGivenNameAndSurnameIsFamilyName(String GivenName, String FamilyName) {
        namePage.enterNameAndSubmit(GivenName, FamilyName);
    }

    @When("^I attempt to enter (.*) characters name$")
    public void iAttemptToEnterOverLimitCharactersName(int count) {
        namePage.enterOverLimitText(count);
    }

    @Then("^I am restricted to enter (.*) characters name$")
    public void iAmRestrictedToEnterMaximumCharactersName(int maximum) {
        Assert.assertEquals(namePage.getEnteredTextCount(), maximum);
        namePage.continueButton();
    }

    @And("^My date of birth is (.*) (.*) (.*)$")
    public void myDateOfBirthIsDayMonthYear(String Day, String Month, String Year) {
        dateOfBirthPage.enterDateOfBirthAndSubmit(Day, Month, Year);
    }

    @And("^My national insurance number is (.*)$")
    public void myNationalInsuranceNumberIsNINumber(String NINumber) {
        ninoPage.enterNINOAndSubmit(NINumber);
    }

    @When("^My IHS number is (.*)$")
    public void myIHSNumberIsIHSNumber(String IHSNumber) {
        ihsNumberPage.enterIHSNumberAndSubmit(IHSNumber);
    }

    @When("^I attempt to enter (.*) characters IHS number$")
    public void iAttemptToEnterOverLimitCharactersIHSNumber(int count) {
        ihsNumberPage.enterOverLimitIHSNumber(count);
    }

    @Then("^I am restricted to enter (.*) characters IHS number$")
    public void iAmRestrictedToEnterMaximumCharactersIHSNumber(int maximum) {
        Assert.assertEquals(ihsNumberPage.getEnteredTextCount(), maximum);
        ihsNumberPage.continueButton();
    }

    @When("^My Email address is (.*)$")
    public void myEmailAddressIsEmail(String Email) {
        emailPage.enterEmailAndSubmit(Email);
    }

    @When("^My Phone number is (.*)$")
    public void myPhoneNumberIsPhoneNumber(String PhoneNumber) {
        mobileNumberPage.enterMobileNumberAndSubmit(PhoneNumber);
    }

    @When("^I attempt to enter (.*) characters phone number$")
    public void iAttemptToEnterOverLimitCharactersPhoneNumber(int count) {
        mobileNumberPage.enterOverLimitPhoneNumber(count);
    }

    @Then("^I am restricted to enter (.*) characters phone number$")
    public void iAmRestrictedToEnterMaximumCharactersPhoneNumber(int maximum) {
        Assert.assertEquals(mobileNumberPage.getEnteredTextCount(), maximum);
        mobileNumberPage.continueButton();
    }

    @When("^My claim start date is (.*) (.*) (.*)$")
    public void myClaimStartDateIsDayMonthYear(String Day, String Month, String Year) {
        claimStartDatePage.enterClaimStartDateAndSubmit(Day, Month, Year);
    }

    @When("^I (.*) choose to subscribe$")
    public void iOptionChooseToSubscribe(String subscribeOption) {
        switch (subscribeOption) {
            case "do":
                subscriptionPage.selectYesRadioButtonAndClickContinue();
                break;
            case "do not":
                subscriptionPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                subscriptionPage.continueButton();
                break;
        }
    }

    @When("I confirm the answers")
    public void iConfirmTheAnswers() throws InterruptedException {
        checkYourAnswersPage.continueButton();
    }

    @When("^I confirm the declaration$")
    public void iConfirmTheDeclaration() throws InterruptedException {
        checkYourAnswersPage.continueButton();
        commonPage.acceptAndSend();
    }

    @And("I submit the claim application")
    public void iSubmitTheClaimApplication() {
        commonPage.acceptAndSend();
    }

    @Then("^My claim is submitted successfully$")
    public void myClaimIsSubmittedSuccessfully() {
        Assert.assertEquals(APPLICATION_COMPLETED_PAGE_TITLE, commonPage.getCurrentPageTitle());
        Assert.assertTrue(commonPage.getCurrentURL().contains(APPLICATION_COMPLETED_PAGE_URL));
        Assert.assertTrue(commonPage.getReferenceNumber().contains("BSA"));
    }

    @When("^I enter claim start on (.*) (.*) (.*)$")
    public void iEnterClaimStartOnDayMonthAndYear(String day, String month, String year) throws Throwable {
        claimStartDatePage.enterClaimStartDateAndSubmit(day, month, year);
        subscriptionPage.selectYesRadioButtonAndClickContinue();
        employerNamePage.enterEmployerAndSubmit(EMPLOYER_NAME_WITH_TITLE_MANDATORY);
        jobTitlePage.enterJobTitle(JOB_TITLE_MANDATORY);
        checkEmploymentsPage.selectNoAndContinue();
    }

    @Then("^I will see claim end on (.*) in Evidence screen$")
    public void iWillSeeClaimEndOnOutputInEvidenceScreen(String output) throws Throwable {
        Assert.assertTrue(uploadFilePage.getClaimPeriods().contains(output));
        uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
        uploadedFilesPage.selectNoRadioButtonAndContinue();
        extraInformationPage.enterExtraInformationAndContinue("The expected claim end date is " + output);
        checkYourAnswersPage.continueButton();
    }

    @Then("^I will see claim end on (.*) in Evidence screen - MultiFile$")
    public void iWillSeeClaimEndOnOutputInEvidenceScreenMultiFile(String output) throws Throwable {
        Assert.assertTrue(uploadFilePage.getClaimPeriods().contains(output));
        uploadFilePage.uploadMultipleFiles(MULTIPLE_FILES);
        uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(MULTIPLE_FILES);
        uploadFilePage.continueButton();
        extraInformationPage.enterExtraInformationAndContinue("The expected claim end date is " + output);
        checkYourAnswersPage.continueButton();
    }

    @And("^I will see claim end on (.*) in Confirmation screen$")
    public void iWillSeeClaimEndOnOutputInConfirmationScreen(String output) {
        commonPage.acceptAndSend();
        Assert.assertTrue(commonPage.getClaimPeriods().contains(output));
    }

    @Then("^I validate the output$")
    public void iValidateTheOutput() {
        WebDriverWait driverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        try {
            driverWait.until(ExpectedConditions.titleIs(APPLICATION_COMPLETED_PAGE_TITLE));
            Assert.assertEquals(APPLICATION_COMPLETED_PAGE_TITLE, commonPage.getCurrentPageTitle());
            ((JavascriptExecutor)driver).executeScript("sauce:job-result=passed");
        }
        catch(AssertionError e) {
            Assert.assertThat(e.getMessage(),is("My Assertion error"));
            ((JavascriptExecutor)driver).executeScript("sauce:job-result=failed");
        }
    }
}