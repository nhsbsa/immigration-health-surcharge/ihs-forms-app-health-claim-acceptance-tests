package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.ExtraInformationPage;
import com.nhsbsa.ihs.pageobjects.ExtraEvidenceUploadedPage;
import com.nhsbsa.ihs.pageobjects.UploadFilePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.io.File;

import static com.nhsbsa.ihs.stepdefs.Constants.FILE_UPLOAD_FILEPATH;

public class ExtraInformationStepDef {

    private WebDriver driver;
    private ExtraInformationPage extraInformationPage;
    private ExtraEvidenceUploadedPage extraEvidenceUploadedPage;

    private UploadFilePage uploadFilePage;

    public ExtraInformationStepDef() {
        driver = Config.getDriver();
        extraInformationPage = new ExtraInformationPage(driver);
        extraEvidenceUploadedPage = new ExtraEvidenceUploadedPage(driver);
        uploadFilePage = new UploadFilePage(driver);

    }

    @When("^My entered extra information is ([\\s\\S]*)$")
    public void myEnteredExtraInformationIsInformation(String information) {
        extraInformationPage.enterExtraInformation(information);
    }
    @When("^My extra information is ([\\s\\S]*)$")
    public void myExtraInformationIsInformation(String information) {
        extraInformationPage.enterExtraInformationAndContinue(information);
    }

    @And("^I submit the extra information with (.*) characters$")
    public void iSubmitTheExtraInformationWithCountCharacters(int count) {
        extraInformationPage.enterOverLimitExtraInformationText(count);
    }

    @And("^I will verify (.*) characters in the extra information$")
    public void IWillVerifyTheCountCharactersInTheExtraInformation(int count) {
        Assert.assertEquals(extraInformationPage.getEnteredTextCount(), count);
        extraInformationPage.continueButton();
    }
    @And("^I (.*) upload the file on Extra Information screen$")
    public void IUploadFileUploadTheFileOnExtraInformationScreen(String uploadFile) {
        switch (uploadFile) {
            case "do":
                extraInformationPage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
                break;
            case "do not":
                extraInformationPage.continueButton();
                break;
        }
    }

    @When("^I upload a file (.*) of format (.*)$")
    public void iUploadAFileFileNameOfFormatFileFormat(String fileName, String fileFormat) {
        extraInformationPage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload a file with name (.*)(.*)$")
    public void iUploadAFileWithNameFileNameFileFormat(String fileName, String fileFormat) {
        extraInformationPage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload a file (.*)(.*) of size (.*)$")
    public void iUploadAFileFileNameFileFormatOfSizeFileSize(String fileName, String fileFormat, String fileSize) {
        extraInformationPage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("I upload (.*) (.*) extra files to my claim application$")
    public void iUploadCountDifferentExtraFilesToMyClaimApplication(int count, String files) {
        switch (files){
            case "different":
                for (int i = 0; i < (count - 1); i++) {
                    File newFile = new File(FILE_UPLOAD_FILEPATH + "New-payslip_No" + (i + 1) + ".png");
                    boolean convertFile = new File(FILE_UPLOAD_FILEPATH + "New-payslip_No" + i + ".png").renameTo(newFile);
                    extraInformationPage.chooseFileAndContinue(String.valueOf(newFile));
                    extraEvidenceUploadedPage.selectYesRadioButtonAndContinue();
                }
                File revertFile = new File(FILE_UPLOAD_FILEPATH + "New-payslip_No0" + ".png");
                boolean convertFile = new File(FILE_UPLOAD_FILEPATH + "New-payslip_No2" + ".png").renameTo(revertFile);
                extraInformationPage.chooseFileAndContinue(String.valueOf(revertFile));
                break;
            case "same":
                for (int i = 0; i < (count - 1); i++) {
                    extraInformationPage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "Large_size_jpg_file" + ".jpg");
                    extraEvidenceUploadedPage.selectYesRadioButtonAndContinue();
                }
                extraInformationPage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "Sample-jpg-image-1-9mb" + ".jpeg");
        }
    }

    @And("^I view the extra evidence (.*) is uploaded successfully$")
    public void iViewTheExtraEvidenceFileNameIsUploadedSuccessfully(String fileName) {
        Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains(fileName));
    }

    @And("^I (.*) want to upload another extra evidence$")
    public void iAddMoreEvidenceWantToUploadAnotherExtraEvidence(String addMoreEvidence) {
        switch (addMoreEvidence) {
            case "do":
                extraEvidenceUploadedPage.selectYesRadioButtonAndContinue();
                break;
            case "do not":
                extraEvidenceUploadedPage.selectNoRadioButtonAndContinue();
                break;
            case "":
                extraEvidenceUploadedPage.continueButton();
                break;
        }
    }

    @When("^I upload a valid extra evidence$")
    public void iUploadAValidExtraEvidence() {
        extraInformationPage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
    }

    @And("^I select (.*) on Extra Evidence Uploaded screen$")
    public void iSelectDeleteLinkOnExtraEvidenceUploadedScreen(String deleteLink) {
        switch (deleteLink) {
            case "Delete File 1 link":
                extraEvidenceUploadedPage.deleteEvidence1Link();
                break;
            case "Delete File 2 link":
                extraEvidenceUploadedPage.deleteEvidence2Link();
                break;
        }
    }

    @And("^I will see delete success message for the file (.*)(.*)$")
    public void iWillSeeDeleteSuccessMessageTextForTheFileFileNameFileFormat(String fileName, String fileFormat) {
        Assert.assertEquals("File named " + fileName + fileFormat + " has been deleted", extraEvidenceUploadedPage.getDeleteUploadedFileSuccessLocator());
    }

    @And("^I will see delete success message on Extra Information screen for file (.*)(.*)$")
    public void iWillSeeDeleteSuccessMessageOnExtraInformationScreenForFileFileNameFileFormat(String fileName, String fileFormat) {
        Assert.assertEquals("File named " + fileName + fileFormat + " has been deleted", extraInformationPage.getDeleteUploadedFileSuccessLocator());
    }

    @And("^I will see extra files are added having (.*)$")
    public void iWillSeeExtraFilesAreAddedHavingFilename(String fileName) {
        switch (fileName) {
            case "different filename":
                Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains("New-payslip_No1.png"));
                Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains("New-payslip_No2.png"));
                Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains("New-payslip_No0.png"));
                break;
            case "same filename":
                Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains("Large_size_jpg_file.jpg"));
                Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains("Large_size_jpg_file.jpg"));
                Assert.assertTrue(extraEvidenceUploadedPage.getUploadedFile().contains("Sample-jpg-image-1-9mb.jpeg"));
                break;
        }
    }
}