package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class CommonStepDef {

    private WebDriver driver;
    private CommonPage commonPage;

    public CommonStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
    }

    @When("^I select the (.*)$")
    public void iSelectTheHyperlink(String hyperlink) {
        switch (hyperlink) {
            case "Service Name link":
                driver.manage().deleteAllCookies();
                commonPage.serviceNameLink();
                break;
            case "GOV.UK link":
                commonPage.govUKLink();
                break;
            case "Back link":
                commonPage.backLink();
                break;
            case "Contact Us link":
                commonPage.navigateToContactUsLink();
                break;
            case "More Info link":
                commonPage.navigateToMoreInfo();
                break;
            case "Privacy Info link":
                commonPage.declarationPrivacy();
                break;
            case "End of service survey link":
                commonPage.endOfServiceSurvey();
                break;
            case "Help link":
                commonPage.navigateToHelp();
                break;
            case "Cookies link":
                commonPage.navigateToCookies();
                break;
            case "Accessibility Statement link":
                commonPage.navigateToAccessibilityStatement();
                break;
            case "Contact link":
                commonPage.navigateToContactUs();
                break;
            case "Terms and Conditions link":
                commonPage.navigateToTermsConditions();
                break;
            case "Privacy link":
                commonPage.navigateToPrivacyNotice();
                break;
            case "Open Government Licence link":
                commonPage.navigateToOpenLicence();
                break;
            case "Crown Copyright link":
                commonPage.navigateToCopyrightLogo();
                break;
            case "Accept cookies button":
                commonPage.acceptAnalyticsCookies();
                break;
            case "Reject cookies button":
                commonPage.rejectAnalyticsCookies();
                break;
            case "View cookies link":
                commonPage.navigateToViewCookies();
                break;
            case "Change accept cookie settings link":
                commonPage.acceptAnalyticsCookies();
                commonPage.navigateToAcceptCookieSettings();
                driver.manage().deleteAllCookies();
                break;
            case "Change reject cookie settings link":
                commonPage.rejectAnalyticsCookies();
                commonPage.navigateToRejectCookieSettings();
                driver.manage().deleteAllCookies();
                break;
            case "Hide accept message button":
                commonPage.hideAcceptCookieBanner();
                break;
            case "Hide reject message button":
                commonPage.hideRejectCookieBanner();
                break;
            case "Feedback link":
                commonPage.navigateToSurveyPreview();
                break;
            case "Applicant Visas and Immigration link":
                commonPage.navigateToVisasAndImmigrationLink();
                break;
            case "Dependant Visas and Immigration link":
                commonPage.clickApplicantLostThisNumberDropdown();
                commonPage.navigateToVisasAndImmigrationLink();
                break;
        }
    }

    @When("^I see the cookies banner is displayed$")
    public void iSeeTheCookiesBannerIsDisplayed() {
        Assert.assertTrue(commonPage.isCookieBannerDisplayed());
    }

    @Then("^I see the cookies banner is hidden$")
    public void iSeeTheCookiesBannerIsHidden() {
        Assert.assertFalse(commonPage.isCookieBannerDisplayed());
        driver.manage().deleteAllCookies();
    }

    @And("^I start the survey$")
    public void iStartTheSurvey() {
        commonPage.navigateToSnapSurvey();
    }
}
