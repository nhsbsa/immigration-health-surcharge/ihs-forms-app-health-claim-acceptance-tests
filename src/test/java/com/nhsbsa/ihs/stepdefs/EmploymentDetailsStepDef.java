package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.util.*;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class EmploymentDetailsStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private EmployerNamePage employerNamePage;
    private JobTitlePage jobTitlePage;
    private JobSettingPage jobSettingPage;
    private CheckEmploymentsPage checkEmploymentsPage;
    private SubscriptionPage subscriptionPage;

    public EmploymentDetailsStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        employerNamePage = new EmployerNamePage(driver);
        jobTitlePage = new JobTitlePage(driver);
        jobSettingPage = new JobSettingPage(driver);
        checkEmploymentsPage = new CheckEmploymentsPage(driver);
        subscriptionPage = new SubscriptionPage(driver);
    }

    @When("^My employer name is (.*)$")
    public void myEmployerNameIsEmployerName(String employerName) throws InterruptedException {
        employerNamePage.enterEmployerName(employerName);
        employerNamePage.continueButton();
    }

    @When("^I enter (.*) and select (.*) from the list$")
    public void iEnterEmpAndSelectEmployerNameFromTheList(String emp, String employerName) throws InterruptedException {
        employerNamePage.selectEmployerFromList(emp, employerName);
    }

    @When("^I enter (.*) characters in employer name$")
    public void iEnterCharactersInEmployerName(int count) {
        employerNamePage.enterEmployerOverLimitText(count);
    }

    @Then("^I am restricted to enter (.*) characters employer name$")
    public void iAmRestrictedToEnterMaximumCharactersEmployerName(int maximum) {
        Assert.assertEquals(employerNamePage.getEnteredEmployerNameLength(), maximum);
        employerNamePage.continueButton();
    }

    @When("^My job title is (.*)$")
    public void myJobTitleIsJobTitle(String jobTitle) {
        jobTitlePage.enterJobTitle(jobTitle);
    }

    @When("^I enter (.*) characters in job title$")
    public void iEnterCharactersInJobTitle(int count) {
        jobTitlePage.enterJobTitleOverLimitText(count);
    }

    @Then("^I am restricted to enter (.*) characters job title$")
    public void iAmRestrictedToEnterMaximumCharactersJobTitle(int maximum) {
        Assert.assertEquals(jobTitlePage.getEnteredJobTitleLength(), maximum);
        jobTitlePage.continueButton();
    }

    @When("^I enter (.*) and select (.*) from the job title list$")
    public void iEnterJobAndSelectJobTitleFromTheJobTitleList(String job, String jobTitle) throws InterruptedException {
        jobTitlePage.selectJobTitleFromList(job, jobTitle);
    }

    @When("^My job setting is (.*)$")
    public void myJobSettingIs(String jobSetting) {
        switch (jobSetting) {
            case "Hospital":
                jobSettingPage.selectHospitalAndContinue();
                break;
            case "GP Practice":
                jobSettingPage.selectGPPracticeAndContinue();
                break;
            case "Care Home":
                jobSettingPage.selectCareHomeAndContinue();
                break;
            case "Community Healthcare Setting":
                jobSettingPage.selectCommunityHealthcareAndContinue();
                break;
            case "":
                jobSettingPage.continueButton();
                break;
            default:
                throw new RuntimeException("This is a runtime exception");
        }
    }

    @When("^My other job setting is (.*)$")
    public void myOtherJobTypeIsOtherJobSetting(String otherJobSetting) {
        jobSettingPage.selectOther();
        jobSettingPage.enterOtherJobTypeAndContinue(otherJobSetting);
    }

    @When("^I enter (.*) characters in job setting$")
    public void iEnterCharactersInJobSetting(int count) {
        jobSettingPage.selectOther();
        jobSettingPage.enterJobSettingOverLimitText(count);
    }

    @Then("^I am restricted to enter (.*) characters job setting$")
    public void iAmRestrictedToEnterMaximumCharactersJobSetting(int maximum) {
        Assert.assertEquals(jobSettingPage.getEnteredOtherJobSetting(), maximum);
        jobSettingPage.continueButton();
    }

    @When("^I select (.*) on Check Employments screen$")
    public void iSelectHyperlinkOnCheckEmploymentsScreen(String hyperlink) {
        switch (hyperlink) {
            case "Change Employer Name link":
                checkEmploymentsPage.changeEmployerName();
                break;
            case "Change Job Title link":
                checkEmploymentsPage.changeJobTitle();
                break;
            case "Change Job Setting link":
                checkEmploymentsPage.changeJobSetting();
                break;
            case "Delete Employer1 link":
                checkEmploymentsPage.deleteEmployer1();
                break;
            case "Delete Employer2 link":
                checkEmploymentsPage.deleteEmployer2();
                break;
            case "Service Name link":
                driver.manage().deleteCookieNamed("SESSION");
                commonPage.serviceNameLink();
                break;
            case "GOV.UK link":
                commonPage.govUKLink();
                break;
        }
    }

    @And("^I (.*) have more employers to add$")
    public void iEmploymentOptionHaveMoreEmployersToAdd(String employmentOption) {
        switch (employmentOption) {
            case "Do":
                checkEmploymentsPage.selectYesAndContinue();
                break;
            case "Do not":
                checkEmploymentsPage.selectNoAndContinue();
                break;
            case "":
                checkEmploymentsPage.continueButton();
                break;
        }
    }

    @And("^I select (.*) in Check Employment Details screen$")
    public void iSelectChangeLinkInCheckEmploymentDetailScreen(String changeLink) {
        switch (changeLink) {
            case "Change Employer Name link":
                checkEmploymentsPage.changeEmployerName();
                break;
            case "Change Job Title link":
                checkEmploymentsPage.changeJobTitle();
                break;
            case "Change Job Setting link":
                checkEmploymentsPage.changeJobSetting();
                break;
            case "Change Employer Name link2":
                checkEmploymentsPage.changeEmployerName2();
                break;
        }
    }

    @And("^My employment's (.*) is pre-populated$")
    public void myEmploymentDetailIsPrePopulated(String detail) {
        switch (detail) {
            case "employer name with title and setting":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE_AND_SETTING, employerNamePage.getEnteredEmployerName());
                break;
            case "job title with setting":
                Assert.assertEquals(JOB_TITLE_WITH_SETTING, jobTitlePage.getEnteredJobTitle());
                break;
            case "job setting":
                Assert.assertTrue(jobSettingPage.getEnteredJobSetting().equalsIgnoreCase(JOB_SETTING));
                break;
            case "subscription":
                Assert.assertTrue(subscriptionPage.isYesRadioButtonSelected());
                break;
            case "employer name with title":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE, employerNamePage.getEnteredEmployerName());
                break;
            case "job title":
                Assert.assertEquals(JOB_TITLE, jobTitlePage.getEnteredJobTitle());
                break;
            case "employer name":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE_MANDATORY, employerNamePage.getEnteredEmployerName());
                break;
        }
    }

    @When("^I do not change the employment's (.*) and continue$")
    public void iDoNotChangeTheEmploymentDetailsAndContinue(String detail) {
        switch (detail) {
            case "employer name with title and setting":
            case "employer name with title":
            case "employer name":
                employerNamePage.continueButton();
                break;
            case "job title with setting":
            case "job title":
                jobTitlePage.continueButton();
                break;
            case "job setting":
                jobSettingPage.continueButton();
                break;
        }
    }

    @And("^My employment's (.*) is not changed$")
    public void myEmploymentDetailIsNotChanged(String detail) {
        switch (detail) {
            case "employer name with title and setting":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE_AND_SETTING, checkEmploymentsPage.getDisplayedEmployerName());
                break;
            case "job title with setting":
                Assert.assertEquals(JOB_TITLE_WITH_SETTING, checkEmploymentsPage.getDisplayedJobTitle());
                break;
            case "job setting":
                Assert.assertEquals(JOB_SETTING, checkEmploymentsPage.getDisplayedJobSetting());
                break;
            case "employer name with title":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE, checkEmploymentsPage.getDisplayedEmployerName());
                break;
            case "job title":
                Assert.assertEquals(JOB_TITLE, checkEmploymentsPage.getDisplayedJobTitle());
                break;
            case "employer name":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE_MANDATORY, checkEmploymentsPage.getDisplayedEmployerName());
                break;
        }
    }

    @When("^I change the employment's (.*) to another employer with Job Title and Job Setting and continue$")
    public void iChangeTheEmploymentDetailWithTitleAndSettingAndContinue(String detail) {
        switch (detail) {
            case "employer name with title and setting":
            case "employer name with title":
            case "employer name":
                employerNamePage.enterEmployerAndSubmit(UPDATED_EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
                checkEmploymentsPage.changeJobTitle();
                jobTitlePage.enterJobTitle(UPDATED_JOB_TITLE_WITH_SETTING);
                jobSettingPage.selectGPPracticeAndContinue();
                break;
            case "job title with setting":
            case "job title":
                jobTitlePage.enterJobTitle(UPDATED_JOB_TITLE_WITH_SETTING);
                jobSettingPage.selectGPPracticeAndContinue();
                break;
            case "job setting":
                jobSettingPage.selectGPPracticeAndContinue();
                break;
        }
    }

    @When("^I change the employment's (.*) to employer with Job title and continue$")
    public void iChangeTheEmploymentDetailWithTitleAndContinue(String answer) {
        switch (answer) {
            case "employer name with title and setting":
            case "employer name with title":
            case "employer name":
                employerNamePage.enterEmployerAndSubmit(UPDATED_EMPLOYER_NAME_WITH_TITLE);
                checkEmploymentsPage.changeJobTitle();
                jobTitlePage.enterJobTitle(UPDATED_JOB_TITLE_WITHOUT_SETTING);
                break;
            case "job title with setting":
            case "job title":
                jobTitlePage.enterJobTitle(UPDATED_JOB_TITLE_WITHOUT_SETTING);
                break;
        }
    }

    @And("^My employment's (.*) is changed to another employer with Job Title and Job Setting$")
    public void myEmploymentDetailIsChangedWithJobTitleAndSetting(String detail) {
        switch (detail) {
            case "employer name with title and setting":
            case "employer name with title":
            case "employer name":
                Assert.assertEquals(UPDATED_EMPLOYER_NAME_WITH_TITLE_AND_SETTING, checkEmploymentsPage.getDisplayedEmployerName());
                Assert.assertEquals(UPDATED_JOB_TITLE_WITH_SETTING, checkEmploymentsPage.getDisplayedJobTitle());
                Assert.assertEquals(UPDATED_JOB_SETTING, checkEmploymentsPage.getDisplayedJobSetting());
                break;
            case "job title with setting":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE_AND_SETTING, checkEmploymentsPage.getDisplayedEmployerName());
                Assert.assertEquals(UPDATED_JOB_TITLE_WITH_SETTING, checkEmploymentsPage.getDisplayedJobTitle());
                Assert.assertEquals(UPDATED_JOB_SETTING, checkEmploymentsPage.getDisplayedJobSetting());
                break;
            case "job setting":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE_AND_SETTING, checkEmploymentsPage.getDisplayedEmployerName());
                Assert.assertEquals(JOB_TITLE_WITH_SETTING, checkEmploymentsPage.getDisplayedJobTitle());
                Assert.assertEquals(UPDATED_JOB_SETTING, checkEmploymentsPage.getDisplayedJobSetting());
                break;
            case "job title":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE, checkEmploymentsPage.getDisplayedEmployerName());
                Assert.assertEquals(UPDATED_JOB_TITLE_WITH_SETTING, checkEmploymentsPage.getDisplayedJobTitle());
                Assert.assertEquals(UPDATED_JOB_SETTING, checkEmploymentsPage.getDisplayedJobSetting());
                break;
        }
    }

    @And("^My employment's (.*) is changed to employer with Job title$")
    public void myEmploymentDetailIsChangedWithJobTitle(String detail) {
        switch (detail) {
            case "employer name with title and setting":
            case "employer name with title":
            case "employer name":
                Assert.assertEquals(UPDATED_EMPLOYER_NAME_WITH_TITLE, checkEmploymentsPage.getDisplayedEmployerName());
                Assert.assertEquals(UPDATED_JOB_TITLE_WITHOUT_SETTING, checkEmploymentsPage.getDisplayedJobTitle());
                Assert.assertTrue(checkEmploymentsPage.isJobSettingNotDisplayed());
                break;
            case "job title with setting":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE_AND_SETTING, checkEmploymentsPage.getDisplayedEmployerName());
                Assert.assertEquals(UPDATED_JOB_TITLE_WITHOUT_SETTING, checkEmploymentsPage.getDisplayedJobTitle());
                Assert.assertTrue(checkEmploymentsPage.isJobSettingNotDisplayed());
                break;
            case "job title":
                Assert.assertEquals(EMPLOYER_NAME_WITH_TITLE, checkEmploymentsPage.getDisplayedEmployerName());
                Assert.assertEquals(UPDATED_JOB_TITLE_WITHOUT_SETTING, checkEmploymentsPage.getDisplayedJobTitle());
                Assert.assertTrue(checkEmploymentsPage.isJobSettingNotDisplayed());
                break;
        }
    }

    @When("^I change the employment detail's as (.*)$")
    public void iChangeTheEmploymentDetailSAsInvalid(String invalid) {
        switch (invalid) {
            case "blank employer name":
                employerNamePage.enterEmployerAndSubmit("");
                break;
            case "invalid employer name":
                employerNamePage.enterEmployerAndSubmit("ab");
                break;
            case "blank job title":
                jobTitlePage.enterJobTitle("");
                break;
            case "invalid job title":
                jobTitlePage.enterJobTitle("12");
                break;
            case "blank job setting":
                jobSettingPage.selectOther();
                jobSettingPage.enterOtherJobTypeAndContinue("");
                break;
            case "invalid job setting":
                jobSettingPage.selectOther();
                jobSettingPage.enterOtherJobTypeAndContinue("ok");
                break;
        }
    }

    @When("^I update the (.*) and continue$")
    public void iUpdateTheDetailAndContinue(String detail) {
        switch (detail) {
            case "subscription":
                subscriptionPage.selectNoRadioButtonAndClickContinue();
                break;
            case "employer name with title and setting":
                employerNamePage.enterEmployerAndSubmit(UPDATED_EMPLOYER_NAME_WITH_TITLE_AND_SETTING);
                break;
            case "job title with setting":
                jobTitlePage.enterJobTitle(UPDATED_JOB_TITLE_WITH_SETTING);
                jobSettingPage.continueButton();
                break;
            case "job setting":
                jobSettingPage.selectGPPracticeAndContinue();
                break;
            case "employer name":
                employerNamePage.enterEmployerAndSubmit(UPDATED_EMPLOYER_NAME);
                break;
            case "job title":
                jobTitlePage.enterJobTitle(UPDATED_JOB_TITLE_WITHOUT_SETTING);
                break;
        }
    }

    @And("^My employment's (.*) is updated$")
    public void myEmploymentDetailIsUpdated(String detail) {
        switch (detail) {
            case "employer name with title and setting":
                Assert.assertEquals(UPDATED_EMPLOYER_NAME_WITH_TITLE_AND_SETTING, checkEmploymentsPage.getDisplayedEmployerName());
                break;
            case "job title with setting":
                Assert.assertEquals(UPDATED_JOB_TITLE_WITH_SETTING, checkEmploymentsPage.getDisplayedJobTitle());
                break;
            case "job setting":
                Assert.assertEquals(UPDATED_JOB_SETTING, checkEmploymentsPage.getDisplayedJobSetting());
                break;
            case "employer name":
                Assert.assertEquals(UPDATED_EMPLOYER_NAME, checkEmploymentsPage.getDisplayedEmployerName());
                break;
            case "job title":
                Assert.assertEquals(UPDATED_JOB_TITLE_WITHOUT_SETTING, checkEmploymentsPage.getDisplayedJobTitle());
                break;
        }
    }

    @When("^I add (.*) employer to my claim application$")
    public void iAddEmployerToMyClaimApplication(int count) {
        HashMap<String, String> employmentDetails = new HashMap<>();
        employmentDetails.put("ACHE CLINIC", "student school nurse");
        employmentDetails.put("14 MANOR ROAD (CARE HOME)", "student school nurse");
        employmentDetails.put("ALLINGTON NHS TRUST", "student school nurse");
        employmentDetails.put("AGE UK BOLTON", "clinical assistant");
        employmentDetails.put("A1 PHARMACY", "pre-reg pharmacist");
        employmentDetails.put("BRETBY BUSINESS PARK", "student school nurse");
        employmentDetails.put("ASHFORD HOSPITAL NHS TRUST", "student school nurse");
        employmentDetails.put("JOIN HANDS CARE", "social care manager");
        employmentDetails.put("Testing Services", "test");
        employmentDetails.put("ASPIRE: LIFESKILLS", "student school nurse");
        int i = 0;
        for (Map.Entry<String, String> entry : employmentDetails.entrySet()) {
            if (i < count) {
                employerNamePage.enterEmployerAndSubmit(entry.getKey());
                if (commonPage.getCurrentPageTitle().contains("job title")) {
                    jobTitlePage.enterJobTitle(entry.getValue());
                }
                if (commonPage.getCurrentPageTitle().contains("job setting")) {
                    jobSettingPage.selectGPPracticeAndContinue();
                }
                i++;
                if (i < count) {
                    checkEmploymentsPage.selectYesAndContinue();
                }
            }
        }
    }

    @When("^I will be navigated to the (.*)")
    public void iWillBeNavigatedToNextScreen(String screen) {
        switch (screen) {
            case "Job Title screen":
                Assert.assertTrue(commonPage.getCurrentURL().contains(JOB_TITLE_PAGE_URL));
                jobTitlePage.continueButton();
                if (commonPage.getCurrentPageTitle().contains("job setting"))
                    jobSettingPage.continueButton();
                break;
            case "Job Setting screen":
                Assert.assertTrue(commonPage.getCurrentURL().contains(JOB_SETTING_PAGE_URL));
                jobSettingPage.continueButton();
                break;
            case "Check Employment screen":
                Assert.assertEquals(CHECK_EMPLOYMENTS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_EMPLOYMENTS_PAGE_URL));
                break;
            case "Check your answers screen":
                Assert.assertEquals(CHECK_YOUR_ANSWERS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_YOUR_ANSWERS_PAGE_URL));
                break;
        }
    }
}