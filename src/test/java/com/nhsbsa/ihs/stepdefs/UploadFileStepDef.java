package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.UploadFilePage;
import com.nhsbsa.ihs.pageobjects.UploadedFilesPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

import static com.nhsbsa.ihs.stepdefs.Constants.FILE_UPLOAD_FILEPATH;
import static com.nhsbsa.ihs.stepdefs.Constants.MULTIPLE_FILES;

public class UploadFileStepDef {

    private WebDriver driver;
    private UploadFilePage uploadFilePage;
    private UploadedFilesPage uploadedFilesPage;

    public UploadFileStepDef() {
        driver = Config.getDriver();
        uploadFilePage = new UploadFilePage(driver);
        uploadedFilesPage = new UploadedFilesPage(driver);
    }

    @When("^I (.*) upload the file$")
    public void iUploadFileUploadTheFile(String uploadFile) {
        switch (uploadFile) {
            case "do":
                uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
                break;
            case "do not":
                uploadFilePage.continueButton();
                break;
        }
    }


    @When("^I upload the file (.*) of format (.*)$")
    public void iUploadTheFileFileNameOfFormatFileFormat(String fileName, String fileFormat) {
        uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the file with name (.*)(.*)$")
    public void iUploadTheFileWithNameFileNameFileFormat(String fileName, String fileFormat) {
        uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the file (.*)(.*) of size (.*)$")
    public void iUploadTheFileFile_NameFile_FormatOfSizeFileSize(String fileName, String fileFormat, String fileSize) {
        uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload a valid file in Upload Evidence screen$")
    public void iUploadAValidFileInUploadEvidenceScreen() {
        uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
    }

    @When("^I (.*) want to add another evidence$")
    public void iAddMoreEvidenceWantToAddAnotherEvidence(String addMoreEvidence) {
        switch (addMoreEvidence) {
            case "do":
                uploadedFilesPage.selectYesRadioButtonAndContinue();
                break;
            case "do not":
                uploadedFilesPage.selectNoRadioButtonAndContinue();
                break;
            case "":
                uploadedFilesPage.continueButton();
                break;
        }
    }

    @When("^I add (.*) new files to my claim application$")
    public void iAddCountNewFilesToMyClaimApplication(int count) {
        for (int i = 0; i < (count-1); i++) {
            File newFile = new File(FILE_UPLOAD_FILEPATH + "New-payslip_No" + (i+1) + ".png");
            boolean convertFile = new File(FILE_UPLOAD_FILEPATH + "New-payslip_No" + i + ".png").renameTo(newFile);
            uploadFilePage.chooseFileAndContinue(String.valueOf(newFile));
            uploadedFilesPage.selectYesRadioButtonAndContinue();
        }
        File revertFile = new File(FILE_UPLOAD_FILEPATH + "New-payslip_No0" + ".png");
        boolean convertFile = new File(FILE_UPLOAD_FILEPATH + "New-payslip_No74" + ".png").renameTo(revertFile);
        uploadFilePage.chooseFileAndContinue(String.valueOf(revertFile));
    }

    @When("^I add (.*) same files to my claim application$")
    public void iAddCountSameFilesToMyClaimApplication(int count) {
        for (int i = 0; i < (count-1); i++) {
            uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "Large_size_jpg_file" + ".jpg");
            uploadedFilesPage.selectYesRadioButtonAndContinue();
        }
        uploadFilePage.chooseFileAndContinue(FILE_UPLOAD_FILEPATH + "Sample-jpg-image-1-9mb" + ".jpeg");
    }

    @And("^I view the uploaded evidence (.*)$")
    public void iViewTheUploadedEvidenceFileName(String fileName) {
        Assert.assertTrue(uploadedFilesPage.getUploadedFile().contains(fileName));
    }

    @And("^I select (.*) in Evidence uploaded screen$")
    public void iSelectDeleteLinkInScreen(String deleteLink) {
        switch (deleteLink) {
            case "Delete File 1 link":
                uploadedFilesPage.deleteEvidence1Link();
                break;
            case "Delete File 2 link":
                uploadedFilesPage.deleteEvidence2Link();
                break;

        }
    }

    @And("^I will see delete success text for file (.*) of format (.*)$")
    public void iWillSeeDeleteSuccessTextForFileFileNameOfFormatFileFormat(String fileName, String fileFormat) {
        Assert.assertEquals("File named " + fileName + fileFormat + " has been deleted", uploadedFilesPage.getDeleteUploadedFileSuccessLocator());
    }

    @When("^I (.*) upload multiple file$")
    public void iUploadMultiFilesUploadTheFile(String uploadFile) {
        switch (uploadFile) {
            case "do":
                uploadFilePage.uploadMultipleFiles(MULTIPLE_FILES);
                uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(MULTIPLE_FILES);
                uploadFilePage.continueButton();
                break;
            case "do not":
                uploadFilePage.continueButton();
                break;
        }
    }

    @When("^I upload multiple files (.*)$")
    public void iUploadMultipleFiles(String files) {
        uploadFilePage.uploadMultipleFiles(files);
    }

    @And("^I will see upload success message for the files (.*)$")
    public void iWillSeeUploadSuccessMessageForFiles(String files) {
        Assert.assertTrue(uploadFilePage.isFilesAddedHeadingDisplayed());
        Assert.assertTrue(uploadFilePage.isMultiFileUploadSuccessMessagesDisplayed(files));
    }

    @And("^I will see upload success message for the new files$")
    public void iWillSeeUploadSuccessMessageForNewFiles() {
        Assert.assertTrue(uploadFilePage.isMultiFileUploadSuccessMessagesDisplayedFromCheckAnswer());
    }

    @When("I click on continue")
    public void iClickOnContinue() {
        uploadFilePage.continueButton();
    }

    @When("^I add (.*) multi files to my claim application$")
    public void iAddCountMultiFilesToMyClaimApplication(int count) {
        for (int i = 1; i <= 7; i++) {
            uploadFilePage.uploadMultipleFiles("Payslip-June.png,My Payslip_1.pdf,Payslip July.bmp,123456.pdf,1-payslip_May Name.jpg,Sample_1.jpg,sample_2.png,sample_3.pdf,Sample_4.bmp,Sample_5.jpeg");
            uploadFilePage.isMultiFileUploadSuccessMessageCountCorrect(10 * i);
        }
        if (count == 75) {
            uploadFilePage.uploadMultipleFiles("Payslip-June.png,My Payslip_1.pdf,Payslip July.bmp,123456.pdf,1-payslip_May Name.jpg");
            uploadFilePage.isMultiFileUploadSuccessMessageCountCorrect(75);
        }
        else
            uploadFilePage.uploadMultipleFiles("Payslip-June.png,My Payslip_1.pdf,Payslip July.bmp,123456.pdf,1-payslip_May Name.jpg,Sample_1.jpg");
    }

    @And("I upload multiple payslips in SauceLabs$")
    public void iUploadMultiplePayslipsInSauceLabsFiles() throws IOException {
        uploadFilePage.uploadMultiFileSauceLabs();
    }

    @And("^I click delete link for (.*) on Upload Evidence screen$")
    public void iSelectDeleteLink(String fileName) {
        uploadFilePage.deleteFile(fileName);
    }

    @And("^I click delete link on Upload Evidence screen$")
    public void iSelectDeleteLink() {
        uploadFilePage.deleteFile();
    }

    @And("^I will see delete success message for file (.*)$")
    public void iWillSeeDeleteSuccessMessageForFile(String fileName) {
        Assert.assertEquals("File named " + fileName + " has been deleted", uploadFilePage.getDeleteFileSuccessMessage(fileName));
    }

    @And("^I click Delete all unsuccessful files link on Upload Evidence screen$")
    public void iSelectDeleteAllUnsuccessfulFilesLink() {
        uploadFilePage.deleteAllUnsuccessfulFiles();
    }

    @And("^I will see delete success message$")
    public void iWillSeeDeleteSuccessMessage() {
        Assert.assertEquals("All unsuccessful files have been deleted", uploadFilePage.getDeleteFileSuccessMessage());
    }

    @And("^I will not see any unsuccessful files in the Files added section$")
    public void iWillNotSeeAnyUnsuccessfulFiles() {
        Assert.assertTrue(uploadFilePage.isUnsuccessfulFileDisplayed());
    }

    @And("^I click Clear error message link on Upload Evidence screen$")
    public void iClickClearErrorMessageLink() {
        uploadFilePage.clearErrorMessage();
    }

    @And("^I will see File Upload in progress$")
    public void iWillSeeFileUploadProgress() {
        Assert.assertTrue(uploadFilePage.isFilesAddedHeadingDisplayed());
        Assert.assertTrue(uploadFilePage.multiFileUploadProgressCount(2));
    }

    @And("^I will not see the delete success message$")
    public void iWillNotSeeDeleteSuccessMessage() {
        Assert.assertTrue(uploadFilePage.isDeleteSuccessMessageDisplayed());
    }

    @When("^I upload (.*) new multi files to my claim application")
    public void iUploadCountNewMultiFilesToMyClaimApplication(int count) {
        if (count == 75) {
            uploadFilePage.uploadNewMultipleFiles("PayslipDecember copy 1.jpg,PayslipDecember copy 2.jpg,PayslipDecember copy 3.jpg,PayslipDecember copy 4.jpg,PayslipDecember copy 5.jpg,PayslipDecember copy 6.jpg,PayslipDecember copy 7.jpg,PayslipDecember copy 8.jpg,PayslipDecember copy 9.jpg,PayslipDecember copy 10.jpg,PayslipDecember copy 11.jpg,PayslipDecember copy 12.jpg,PayslipDecember copy 13.jpg,PayslipDecember copy 14.jpg,PayslipDecember copy 15.jpg,PayslipDecember copy 16.jpg,PayslipDecember copy 17.jpg,PayslipDecember copy 18.jpg,PayslipDecember copy 19.jpg,PayslipDecember copy 20.jpg,PayslipDecember copy 21.jpg,PayslipDecember copy 22.jpg,PayslipDecember copy 23.jpg,PayslipDecember copy 24.jpg,PayslipDecember copy 25.jpg,PayslipDecember copy 26.jpg,PayslipDecember copy 27.jpg,PayslipDecember copy 28.jpg,PayslipDecember copy 29.jpg,PayslipDecember copy 30.jpg,PayslipDecember copy 31.jpg,PayslipDecember copy 32.jpg,PayslipDecember copy 33.jpg,PayslipDecember copy 34.jpg,PayslipDecember copy 35.jpg,PayslipDecember copy 36.jpg,PayslipDecember copy 37.jpg,PayslipDecember copy 38.jpg,PayslipDecember copy 39.jpg,PayslipDecember copy 40.jpg,PayslipDecember copy 41.jpg,PayslipDecember copy 42.jpg,PayslipDecember copy 43.jpg,PayslipDecember copy 44.jpg,PayslipDecember copy 45.jpg,PayslipDecember copy 46.jpg,PayslipDecember copy 47.jpg,PayslipDecember copy 48.jpg,PayslipDecember copy 49.jpg,PayslipDecember copy 50.jpg,PayslipDecember copy 51.jpg,PayslipDecember copy 52.jpg,PayslipDecember copy 53.jpg,PayslipDecember copy 54.jpg,PayslipDecember copy 55.jpg,PayslipDecember copy 56.jpg,PayslipDecember copy 57.jpg,PayslipDecember copy 58.jpg,PayslipDecember copy 59.jpg,PayslipDecember copy 60.jpg,PayslipDecember copy 61.jpg,PayslipDecember copy 62.jpg,PayslipDecember copy 63.jpg,PayslipDecember copy 64.jpg,PayslipDecember copy 65.png,PayslipDecember copy 66.png,PayslipDecember copy 67.png,PayslipDecember copy 68.jpg,PayslipDecember copy 69.jpg,PayslipDecember copy 70.jpg,PayslipDecember copy 71.pdf,PayslipDecember copy 72.jpg,PayslipDecember copy 73.pdf,PayslipDecember copy 74.jpg,PayslipDecember copy 75.pdf");
            uploadFilePage.isMultiFileUploadSuccessMessageCountCorrect(count);
        }
        else if (count == 76) {
            uploadFilePage.uploadNewMultipleFiles("PayslipDecember copy 1.jpg,PayslipDecember copy 2.jpg,PayslipDecember copy 3.jpg,PayslipDecember copy 4.jpg,PayslipDecember copy 5.jpg,PayslipDecember copy 6.jpg,PayslipDecember copy 7.jpg,PayslipDecember copy 8.jpg,PayslipDecember copy 9.jpg,PayslipDecember copy 10.jpg,PayslipDecember copy 11.jpg,PayslipDecember copy 12.jpg,PayslipDecember copy 13.jpg,PayslipDecember copy 14.jpg,PayslipDecember copy 15.jpg,PayslipDecember copy 16.jpg,PayslipDecember copy 17.jpg,PayslipDecember copy 18.jpg,PayslipDecember copy 19.jpg,PayslipDecember copy 20.jpg,PayslipDecember copy 21.jpg,PayslipDecember copy 22.jpg,PayslipDecember copy 23.jpg,PayslipDecember copy 24.jpg,PayslipDecember copy 25.jpg,PayslipDecember copy 26.jpg,PayslipDecember copy 27.jpg,PayslipDecember copy 28.jpg,PayslipDecember copy 29.jpg,PayslipDecember copy 30.jpg,PayslipDecember copy 31.jpg,PayslipDecember copy 32.jpg,PayslipDecember copy 33.jpg,PayslipDecember copy 34.jpg,PayslipDecember copy 35.jpg,PayslipDecember copy 36.jpg,PayslipDecember copy 37.jpg,PayslipDecember copy 38.jpg,PayslipDecember copy 39.jpg,PayslipDecember copy 40.jpg,PayslipDecember copy 41.jpg,PayslipDecember copy 42.jpg,PayslipDecember copy 43.jpg,PayslipDecember copy 44.jpg,PayslipDecember copy 45.jpg,PayslipDecember copy 46.jpg,PayslipDecember copy 47.jpg,PayslipDecember copy 48.jpg,PayslipDecember copy 49.jpg,PayslipDecember copy 50.jpg,PayslipDecember copy 51.jpg,PayslipDecember copy 52.jpg,PayslipDecember copy 53.jpg,PayslipDecember copy 54.jpg,PayslipDecember copy 55.jpg,PayslipDecember copy 56.jpg,PayslipDecember copy 57.jpg,PayslipDecember copy 58.jpg,PayslipDecember copy 59.jpg,PayslipDecember copy 60.jpg,PayslipDecember copy 61.jpg,PayslipDecember copy 62.jpg,PayslipDecember copy 63.jpg,PayslipDecember copy 64.jpg,PayslipDecember copy 65.png,PayslipDecember copy 66.png,PayslipDecember copy 67.png,PayslipDecember copy 68.jpg,PayslipDecember copy 69.jpg,PayslipDecember copy 70.jpg,PayslipDecember copy 71.pdf,PayslipDecember copy 72.jpg,PayslipDecember copy 73.pdf,PayslipDecember copy 74.jpg,PayslipDecember copy 75.pdf,PayslipDecember copy 76.pdf");
            uploadFilePage.isMultiFileUploadSuccessMessageCountCorrect(75);
        }
    }
}