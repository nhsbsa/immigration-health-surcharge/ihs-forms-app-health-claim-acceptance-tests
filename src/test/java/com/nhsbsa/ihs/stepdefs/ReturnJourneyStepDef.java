package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import com.nhsbsa.ihs.utilities.ConfigReader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;
import static io.restassured.RestAssured.given;

public class ReturnJourneyStepDef {

    String NINumber = "SZ593913C";
    String IHSNumber = "IHS511860244";
    String bsaReference = "BSA4594752";
    private WebDriver driver;
    private NationalInsuranceNumberPage ninoPage;
    private IHSNumberPage ihsNumberPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private DependantQuesPage dependantQuesPage;
    private ClaimStartDatePage claimStartDatePage;
    private CommonPage commonPage;
    private NotEligiblePage notEligiblePage;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;

    public ReturnJourneyStepDef() {
        driver = Config.getDriver();
        ninoPage = new NationalInsuranceNumberPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        dependantQuesPage = new DependantQuesPage(driver);
        claimStartDatePage = new ClaimStartDatePage(driver);
        commonPage = new CommonPage(driver);
        notEligiblePage = new NotEligiblePage(driver);
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
    }

    @Given("^My previous claim details are (.*) and (.*)$")
    public void myPreviousClaimDetails(String StartDate, String EndDate) {
        String url = ConfigReader.getApiUrl();
        String requestBody = "{\"" +
                "bsaReimbursementReference\":\"" + bsaReference + "\"," +
                "\"nationalInsuranceNumber\":\"" + NINumber + "\"," +
                "\"immigrationHealthSurchargeNumber\":\"" + IHSNumber + "\"," +
                "\"dateOfBirth\":\"1989-05-16\"," +
                "\"givenName\":\"GivenName\"," +
                "\"familyName\":\"FamilyName\"," +
                "\"startDate\":\"" + StartDate + "\"," +
                "\"endDate\":\"" + EndDate + "\"" +
                "}";

        given().log().all().when().
                header("Content-Type", "application/json").
                header("x-api-key", ConfigReader.getApiKey()).
                body(requestBody).
                put(url + "claims").
                then().log().all().statusCode(200);
    }

    @And("^I enter my first name and family name")
    public void iEnterMyFirstNameAndFamilyName() {
        namePage.enterNameAndSubmit("GivenName", "FamilyName");
    }

    @And("^I enter my date of birth$")
    public void iEnterMyDateOfBirth() {
        dateOfBirthPage.enterDateOfBirthAndSubmit("16", "05", "1989");
    }

    @And("^I enter my National Insurance Number$")
    public void iEnterMyNationalInsuranceNumber() {
        ninoPage.enterNINOAndSubmit(NINumber);
    }

    @When("^I enter my IHS number$")
    public void iEnterMyIHSNumber() {
        ihsNumberPage.enterIHSNumberAndSubmit(IHSNumber);
    }

    @And("^I will see claim dates (.*) and (.*) of my previous claim$")
    public void iWillSeeClaimDatesStartDateAndEndDateOfMyPreviousClaim(String StartDate, String EndDate) {
        Assert.assertTrue(notEligiblePage.getPreviousClaimDates().contains(StartDate));
        Assert.assertTrue(notEligiblePage.getPreviousClaimDates().contains(EndDate));
    }

    @And("^I will see earliest date I can submit the new claim is (.*)$")
    public void iWillSeeEarliestDateICanSubmitTheNewClaimIsEarliestDate(String EarliestDate) {
        Assert.assertTrue(notEligiblePage.getEarliestDate().contains(EarliestDate));
    }

    @Then("^I will see previous claim dates (.*) and (.*) on the claim start date screen$")
    public void iWillSeePreviousClaimDatesStartDateAndEndDateOnTheClaimStartDateScreen(String startDate, String endDate) {
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        dependantQuesPage.selectNoRadioButtonAndClickContinue();
        Assert.assertTrue(claimStartDatePage.getPreviousClaimDate().contains(startDate));
        Assert.assertTrue(claimStartDatePage.getPreviousClaimDate().contains(endDate));
    }

    @And("^I will see earliest date I can start the new claim is (.*)$")
    public void iWillSeeEarliestDateICanStartTheNewClaimIsEarliestDate(String EarliestDate) {
        Assert.assertTrue(claimStartDatePage.getEarliestDate().contains(EarliestDate));
    }

    @And("^I will see earliest date my new claim period can start is (.*)$")
    public void iWillSeeEarliestDateMyNewClaimPeriodCanStartIsEarliestDate(String earliestStartDate) {
        Assert.assertTrue(claimStartDatePage.getEarliestStartDate().contains(earliestStartDate));
    }

    @When("^I enter claim start date (.*), (.*), (.*)$")
    public void iEnterClaimStartDateDayMonthYear(String day, String month, String year) throws InterruptedException {
        claimStartDatePage.enterClaimStartDateAndSubmit(day, month, year);
    }
}
