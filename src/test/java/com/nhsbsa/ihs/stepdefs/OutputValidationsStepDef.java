package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.util.Arrays;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class OutputValidationsStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private NationalInsuranceNumberPage ninoPage;
    private IHSNumberPage ihsNumberPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private ClaimStartDatePage claimStartDatePage;
    private SubscriptionPage subscriptionPage;
    private DependantQuesPage dependantQuesPage;
    private DependantDetailsPage dependantDetailsPage;
    private CheckDependantsPage checkDependantsPage;
    private EmployerNamePage employerNamePage;
    private JobTitlePage jobTitlePage;
    private JobSettingPage jobSettingPage;
    private CheckEmploymentsPage checkEmploymentsPage;
    private UploadFilePage uploadFilePage;
    private UploadedFilesPage uploadedFilesPage;
    private ExtraInformationPage extraInformationPage;
    private ExtraEvidenceUploadedPage extraEvidenceUploadedPage;
    private CheckYourAnswersPage checkYourAnswersPage;

    public OutputValidationsStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ninoPage = new NationalInsuranceNumberPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        claimStartDatePage = new ClaimStartDatePage(driver);
        subscriptionPage = new SubscriptionPage(driver);
        dependantQuesPage = new DependantQuesPage(driver);
        dependantDetailsPage = new DependantDetailsPage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
        employerNamePage = new EmployerNamePage(driver);
        jobTitlePage = new JobTitlePage(driver);
        jobSettingPage = new JobSettingPage(driver);
        checkEmploymentsPage = new CheckEmploymentsPage(driver);
        uploadFilePage = new UploadFilePage(driver);
        uploadedFilesPage = new UploadedFilesPage(driver);
        extraInformationPage = new ExtraInformationPage(driver);
        extraEvidenceUploadedPage = new ExtraEvidenceUploadedPage(driver);
        checkYourAnswersPage = new CheckYourAnswersPage(driver);
    }

    @Then("^I will see the (.*)$")
    public void iWillSeeTheOutput(String output) {
        switch (output) {
            case "Start screen":
                Assert.assertEquals(START_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains( START_PAGE_URL));
                break;
            case "Name screen":
                Assert.assertEquals(NAME_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(NAME_PAGE_URL));
                break;
            case "Date of birth screen":
                Assert.assertEquals(DATE_OF_BIRTH_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DATE_OF_BIRTH_PAGE_URL));
                break;
            case "NI Number screen":
                Assert.assertEquals(NINO_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(NINO_PAGE_URL));
                break;
            case "IHS Number screen":
                Assert.assertEquals(IHS_NUMBER_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(IHS_NUMBER_PAGE_URL));
                break;
            case "Email address screen":
                Assert.assertEquals(EMAIL_ADDRESS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(EMAIL_ADDRESS_PAGE_URL));
                break;
            case "Phone number screen":
                Assert.assertEquals(PHONE_NUMBER_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(PHONE_NUMBER_PAGE_URL));
                break;
            case "Dependant question screen":
                Assert.assertEquals(DEPENDANT_QUESTION_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_QUESTION_PAGE_URL));
                break;
            case "Dependant details screen":
                Assert.assertEquals(DEPENDANT_DETAILS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_DETAILS_PAGE_URL));
                break;
            case "Check dependants screen":
                Assert.assertEquals(CHECK_DEPENDANTS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_DEPENDANTS_PAGE_URL));
                break;
            case "Check dependants error screen":
                Assert.assertEquals(CHECK_DEPENDANTS_ERROR_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_DEPENDANTS_PAGE_URL));
                break;
            case "Claim start date screen":
                Assert.assertEquals(CLAIM_START_DATE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CLAIM_START_DATE_PAGE_URL));
                break;
            case "Subscriptions screen":
                Assert.assertEquals(SUBSCRIPTION_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(SUBSCRIPTION_PAGE_URL));
                break;
            case "Employer Name screen":
                Assert.assertEquals(EMPLOYER_NAME_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(EMPLOYER_NAME_PAGE_URL));
                break;
            case "Job Title screen":
                Assert.assertEquals(JOB_TITLE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(JOB_TITLE_PAGE_URL));
                break;
            case "Job Setting screen":
                Assert.assertEquals(JOB_SETTING_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(JOB_SETTING_PAGE_URL));
                break;
            case "Check Employment screen":
                Assert.assertEquals(CHECK_EMPLOYMENTS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_EMPLOYMENTS_PAGE_URL));
                break;
            case "Error Check Employment screen":
                Assert.assertEquals(ERROR_CHECK_EMPLOYMENTS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_EMPLOYMENTS_PAGE_URL));
                break;
            case "Upload Evidence screen":
                Assert.assertEquals(UPLOAD_EVIDENCE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(UPLOAD_EVIDENCE_PAGE_URL));
                break;
            case "Evidence Uploaded screen":
                Assert.assertEquals(EVIDENCE_UPLOADED_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(EVIDENCE_UPLOADED_PAGE_URL));
                break;
            case "Evidence uploaded error screen":
                Assert.assertEquals(EVIDENCE_UPLOADED_ERROR_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(EVIDENCE_UPLOADED_PAGE_URL));
                break;
            case "Extra Information screen":
                Assert.assertEquals(EXTRA_INFORMATION_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(EXTRA_INFORMATION_PAGE_URL));
                break;
            case "Extra Evidence Uploaded screen":
                Assert.assertEquals(EXTRA_EVIDENCE_UPLOADED_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(EXTRA_EVIDENCE_UPLOADED_PAGE_URL));
                break;
            case "Check your answers screen":
                Assert.assertEquals(CHECK_YOUR_ANSWERS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_YOUR_ANSWERS_PAGE_URL));
                break;
            case "Declaration screen":
                Assert.assertEquals(DECLARATION_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(DECLARATION_PAGE_URL));
                break;
            case "Application Complete screen":
                Assert.assertEquals(APPLICATION_COMPLETED_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(APPLICATION_COMPLETED_PAGE_URL));
                break;
            case "GOV UK screen":
                Assert.assertEquals(GOV_UK_PAGE, commonPage.getCurrentPageTitle());
                break;
            case "Help screen":
                Assert.assertEquals(HELP_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Pay for UK healthcare screen":
                Assert.assertEquals(PAY_FOR_UK_HEALTHCARE_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Gov UK Health Feedback screen":
                Assert.assertEquals(GOV_UK_HEALTH_FEEDBACK_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.endOfServiceSurvey();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(GOV_UK_HEALTH_FEEDBACK_PAGE_URL));
                break;
            case "Open Government Licence screen":
                Assert.assertEquals(OPEN_GOVERNMENT_LICENCE_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Crown Copyright screen":
                Assert.assertEquals(CROWN_COPYRIGHT_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Snap survey preview":
                Assert.assertEquals(SNAP_SURVEY_PREVIEW_PAGE, commonPage.getSurveyPreviewPageTitle());
                break;
            case "IHS Health Service Survey screen":
                Assert.assertEquals(HEALTH_SNAP_SURVEY_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Contact Us screen":
                Assert.assertEquals(CONTACT_US_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToContactUs();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(CONTACT_US_PAGE_URL));
                break;
            case "Cookies Policy screen":
                Assert.assertEquals(COOKIES_POLICY_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToCookies();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(COOKIES_POLICY_PAGE_URL));
                break;
            case "Accessibility Statement screen":
                Assert.assertEquals(ACCESSIBILITY_STATEMENT_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToAccessibilityStatement();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(ACCESSIBILITY_STATEMENT_PAGE_URL));
                break;
            case "Terms and Conditions screen":
                Assert.assertEquals(TERMS_CONDITIONS_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToTermsConditions();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(TERMS_CONDITIONS_PAGE_URL));
                break;
            case "Privacy Notice screen":
                Assert.assertEquals(PRIVACY_NOTICE_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToPrivacyNotice();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(PRIVACY_NOTICE_PAGE_URL));
                break;
            case "Contact UK Visas and Immigration screen":
                Assert.assertEquals(CONTACT_UK_VISAS_AND_IMMIGRATION_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToVisasAndImmigrationLink();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(CONTACT_UK_VISAS_AND_IMMIGRATION_PAGE_URL));
                break;
            case "Cookies Policy page":
                Assert.assertEquals(COOKIES_POLICY_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Cookies accepted confirmation":
                Assert.assertTrue(commonPage.getCookiesAcceptedMessage().contains("You’ve accepted analytics cookies. You can"));
                break;
            case "Cookies rejected confirmation":
                Assert.assertTrue(commonPage.getCookiesRejectedMessage().contains("You’ve rejected analytics cookies. You can"));
                break;
            case "Blank given name error":
                Assert.assertEquals("Enter the applicant's given name", namePage.getGivenNameErrorMessage());
                break;
            case "Blank family name error":
                Assert.assertEquals("Enter the applicant's family name", namePage.getFamilyNameErrorMessage());
                break;
            case "Invalid given name error":
                Assert.assertEquals("Enter the applicant's given name using only letters, hyphens and apostrophes", namePage.getGivenNameErrorMessage());
                break;
            case "Invalid family name error":
                Assert.assertEquals("Enter the applicant's family name using only letters, hyphens and apostrophes", namePage.getFamilyNameErrorMessage());
                break;
            case "Invalid range given name error":
                Assert.assertEquals("Enter the applicant's given name using between 2 and 50 characters", namePage.getGivenNameErrorMessage());
                break;
            case "Invalid range family name error":
                Assert.assertEquals("Enter the applicant's family name using between 2 and 50 characters", namePage.getFamilyNameErrorMessage());
                break;
            case "Blank date of birth error":
                Assert.assertEquals("Enter the applicant's date of birth", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Minimum 16 years error":
                Assert.assertEquals("Enter a date of birth that is a minimum of 16 years ago", dateOfBirthPage.getMinimumYearsErrorMessage());
                break;
            case "Blank day error":
                Assert.assertEquals("Enter a day", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Blank month error":
                Assert.assertEquals("Enter a month", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Blank year error":
                Assert.assertEquals("Enter a year", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Incomplete date of birth error":
                Assert.assertEquals("Enter a complete date of birth", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid date of birth error":
                Assert.assertEquals("Enter a valid date of birth", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid format date of birth error":
                Assert.assertEquals("Enter the applicant's date of birth in the correct format, for example, 31 3 1980", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid character date of birth error":
                Assert.assertEquals("Enter a date of birth using numbers only", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid character day error":
                Assert.assertEquals("Enter a day using numbers only", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid character month error":
                Assert.assertEquals("Enter a month using numbers only", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Invalid character year error":
                Assert.assertEquals("Enter a year using numbers only", dateOfBirthPage.getDOBErrorMessage());
                break;
            case "Blank nino error":
                Assert.assertEquals("Enter the applicant's National Insurance number", ninoPage.getNINOErrorMessage());
                break;
            case "Invalid nino error":
                Assert.assertEquals("Enter the applicant's National Insurance number in the correct format", ninoPage.getNINOErrorMessage());
                break;
            case "Blank ihs number error":
                Assert.assertEquals("Enter the applicant's IHS number", ihsNumberPage.getIHSNumberErrorMessage());
                break;
            case "Invalid ihs number error":
                Assert.assertEquals("Enter an IHS number that starts with IHS or IHSC and has 9 numbers", ihsNumberPage.getIHSNumberErrorMessage());
                break;
            case "Invalid range ihs number error":
                Assert.assertEquals("Enter an IHS number that is 12 or 13 characters", ihsNumberPage.getIHSNumberErrorMessage());
                break;
            case "Blank email address error":
                Assert.assertEquals("Enter an email address that we can use to contact the applicant about the reimbursement", emailPage.getEmailErrorMessage());
                break;
            case "Invalid email address error":
                Assert.assertEquals("Enter an email address in the correct format, like name@example.com", emailPage.getEmailErrorMessage());
                break;
            case "Invalid phone number length error":
                Assert.assertEquals("Enter a UK telephone number that is 11 digits", mobileNumberPage.getMobileNumberErrorMessage());
                break;
            case "Invalid phone number format error":
                Assert.assertEquals("Enter a telephone number in the correct format", mobileNumberPage.getMobileNumberErrorMessage());
                break;
            case "Invalid selection error":
                Assert.assertEquals("Select 'Yes' if you have any dependant to add", dependantQuesPage.getErrorMessageSelectNoneOption());
                break;
            case "Blank dependant given name error":
                Assert.assertEquals("Enter the dependant's given name", dependantDetailsPage.getDependantErrorMessage());
                break;
            case "Blank dependant family name error":
                Assert.assertEquals("Enter the dependant's family name", dependantDetailsPage.getDependantErrorMessage());
                break;
            case "Invalid dependant given name error":
                Assert.assertEquals("Enter the dependant's given name using only letters, hyphens and apostrophes", dependantDetailsPage.getDependantErrorMessage());
                break;
            case "Invalid dependant family name error":
                Assert.assertEquals("Enter the dependant's family name using only letters, hyphens and apostrophes", dependantDetailsPage.getDependantErrorMessage());
                break;
            case "Invalid range dependant given name error":
                Assert.assertEquals("Enter the dependant's given name using between 2 and 50 characters", namePage.getGivenNameErrorMessage());
                break;
            case "Invalid range dependant family name error":
                Assert.assertEquals("Enter the dependant's family name using between 2 and 50 characters", namePage.getFamilyNameErrorMessage());
                break;
            case "Blank dependant date of birth error":
                Assert.assertEquals("Enter the dependant's date of birth", dependantDetailsPage.getDependantErrorMessage());
                break;
            case "Invalid format dependant date of birth error":
                Assert.assertEquals("Enter the dependant's date of birth in the correct format, for example, 31 3 1980", dependantDetailsPage.getDependantErrorMessage());
                break;
            case "Blank dependant ihs number error":
                Assert.assertEquals("Enter the dependant's IHS number", dependantDetailsPage.getDependantIHSErrorMessage());
                break;
            case "Invalid dependant ihs number error":
                Assert.assertEquals("Enter an IHS number that starts with IHS or IHSC and has 9 numbers", dependantDetailsPage.getDependantIHSErrorMessage());
                break;
            case "Invalid range dependant ihs number error":
                Assert.assertEquals("Enter an IHS number that is 12 or 13 characters", dependantDetailsPage.getDependantIHSErrorMessage());
                break;
            case "Dependant 1 deleted success message":
                Assert.assertTrue(checkDependantsPage.getDeleteSuccessMessage().contains("Dependant 1 Mary O'hara has been deleted"));
                break;
            case "Dependant 2 deleted success message":
                Assert.assertTrue(checkDependantsPage.getDeleteSuccessMessage().contains("Dependant 2 Updated Mary Updated O'hara has been deleted"));
                break;
            case "Select Yes or No error":
                Assert.assertEquals("Select 'Yes' if the applicant has a/another dependant to add", checkDependantsPage.getErrorMessageSelectNoneOption());
                break;
            case "More than 10 dependants error":
                Assert.assertEquals("Add no more than 10 dependants", checkDependantsPage.getErrorMessageMoreDepOption());
                break;
            case "Blank claim start date error":
                Assert.assertEquals("Enter a start date for the applicant's claim", claimStartDatePage.getClaimStartDateErrorMessage());
                break;
            case "Invalid claim start date format error":
                Assert.assertEquals("Enter a date in the correct format, for example, 31 3 1980", claimStartDatePage.getClaimStartDateErrorMessage());
                break;
            case "Before 31 March 2020 error":
                Assert.assertEquals("Enter a date that's on or after 31 March 2020", claimStartDatePage.getClaimStartDateErrorMessage());
                break;
            case "Six months before today error":
                Assert.assertEquals("The date entered is invalid. The applicant must enter a claim start date at least 6 months before today's date", claimStartDatePage.getClaimStartDateSixMonthsErrorMessage());
                break;
            case "Claim overlap error":
                Assert.assertEquals("Enter a start date for a new claim period that does not overlap with a previous claim date", claimStartDatePage.getClaimStartDateErrorMessage());
                break;
            case "Invalid subscription error":
                Assert.assertEquals("Select 'Yes' if the applicant would like to subscribe to get a reminder email to reapply", subscriptionPage.getErrorMessageSelectNoneOption());
                break;
            case "Blank employer name error":
                Assert.assertEquals("Enter the applicant's employer name", employerNamePage.getEmptyEmployerNameErrorMessage());
                break;
            case "Invalid employer name length error":
                Assert.assertEquals("Enter an employer name using between 3 and 255 characters", employerNamePage.getEmployerNameErrorMessage());
                break;
            case "Invalid employer name format error":
                Assert.assertEquals("Enter an employer name using only letters a to z, numbers, spaces, hyphens, underscores, full stops, forward slashes, commas, apostrophes, round brackets, square brackets, asperands (@) and ampersands (&)", employerNamePage.getEmployerNameErrorMessage());
                break;
            case "Blank job title error":
                Assert.assertEquals("Enter the applicant's job title", jobTitlePage.getEmptyJobTitleErrorMessage());
                break;
            case "Invalid job title length error":
                Assert.assertEquals("Enter a job title using between 3 and 70 characters", jobTitlePage.getJobTitleErrorMessage());
                break;
            case "Invalid job title format error":
                Assert.assertEquals("Enter a job title using only letters a to z, numbers, spaces, hyphens, forward slashes, commas and apostrophes", jobTitlePage.getJobTitleErrorMessage());
                break;
            case "Blank job setting error":
                Assert.assertEquals("Select the applicant's job setting", jobSettingPage.getEmptyJobTypeErrorMessage());
                break;
            case "Blank other job setting error":
                Assert.assertEquals("Enter the applicant's job setting", jobSettingPage.getOtherEmptyJobTypeErrorMessage());
                break;
            case "Invalid job setting length error":
                Assert.assertEquals("Enter a job setting using between 3 and 50 characters", jobSettingPage.getJobTypeErrorMessage());
                break;
            case "Invalid job setting format error":
                Assert.assertEquals("Enter a job setting using only letters a to z and spaces", jobSettingPage.getJobTypeErrorMessage());
                break;
            case "Employer deleted success message":
                Assert.assertTrue(checkEmploymentsPage.getDeleteEmployerSuccessMessage().contains("Employer 1 NHS BUSINESS SERVICES AUTHORITY has been deleted"));
                break;
            case "Employer1 deleted success message":
                Assert.assertTrue(checkEmploymentsPage.getDeleteEmployerSuccessMessage().contains("Employer 1 Testing Services has been deleted"));
                break;
            case "Employer2 deleted success message":
                Assert.assertTrue(checkEmploymentsPage.getDeleteEmployerSuccessMessage().contains("Employer 2 JOIN HANDS CARE has been deleted"));
                break;
            case "Select an option error":
                Assert.assertEquals(ERROR_CHECK_EMPLOYMENTS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertEquals("Select 'Yes' if the applicant has another employer to add", checkEmploymentsPage.getCheckEmploymentErrorMessage());
                break;
            case "More than 10 employers error":
                Assert.assertEquals("Add no more than 10 employers", checkDependantsPage.getErrorMessageMoreDepOption());
                break;
            case "No file selected error":
                Assert.assertEquals("Select a file to upload as evidence", uploadFilePage.getErrorMessage());
                break;
            case "Make a selection error":
                Assert.assertEquals("Select 'Yes' if the applicant has more evidence to add", uploadedFilesPage.getErrorMessage());
                break;
            case "More than 75 files error":
                Assert.assertEquals("You cannot upload more than 75 files. To upload another file, you'll need to delete an existing file.", uploadedFilesPage.getErrorMessage());
                break;
            case "Currently not eligible screen":
                Assert.assertEquals(CURRENTLY_NOT_ELIGIBLE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains( CURRENTLY_NOT_ELIGIBLE_PAGE_URL));
                break;
            case "No file selected error for multi file":
                Assert.assertEquals("Select a file to upload as evidence", uploadFilePage.getErrorMessageMultiFile());
                break;
            case "More than 75 files error for multi file":
                Assert.assertTrue(uploadFilePage.getErrorMessageForMaxMultiFileUpload().containsAll(Arrays.asList("You cannot upload more than 75 files. To upload another file, you'll need to delete an existing file.", "Clear error message")));
                break;
            case "Continue before evidence fully uploaded error":
                Assert.assertEquals("You must wait until each file has fully uploaded before continuing", uploadFilePage.getErrorMessageMultiFile());
                break;
            case "Submission without evidence error":
                Assert.assertEquals("You must upload evidence to be able to submit the application. Use the 'change' link to upload evidence.", checkYourAnswersPage.getErrorMessage());
                break;
            case "Invalid extra information format error":
                Assert.assertEquals("Enter extra information using only letters, numbers, spaces, hyphens, apostrophes, commas, full stops and pound signs", extraInformationPage.getErrorMessage());
                break;
            case"More than 2000 characters error":
                Assert.assertEquals("Enter extra information using no more than 2000 characters", extraInformationPage.getErrorMessage());
                break;
            case "More than 3 files error":
                Assert.assertEquals("You cannot upload more than 3 files. To upload another file, you'll need to delete an existing file.", extraEvidenceUploadedPage.getErrorMessage());
                break;
            case "Selection error for upload extra evidence":
                Assert.assertEquals("Select 'Yes' if the applicant has more evidence to add", extraEvidenceUploadedPage.getErrorMessage());
                break;
            case "No Employer added error":
                Assert.assertEquals("You must enter the applicant's employer name to be able to submit the application. Use the 'change' link to enter an employer name.", checkYourAnswersPage.getErrorMessage());
                break;
        }
    }

    @Then("^I will be displayed (.*) for (.*)(.*) file$")
    public void iiWillBeDisplayedErrorForFileNameFileFormatFile(String error, String fileName, String fileFormat) {
        switch (error){
            case "Invalid file format error":
                Assert.assertEquals(uploadFilePage.getErrorMessage(), fileName + fileFormat + " must be a jpg, bmp, png or pdf");
                break;
            case "Invalid file name error":
                Assert.assertEquals(uploadFilePage.getErrorMessage(), fileName + fileFormat + " must use only letters, numbers, spaces, hyphens and underscore");
                break;
            case "Max file size error":
                Assert.assertEquals(uploadFilePage.getErrorMessage(), fileName + fileFormat + " must be less than 2MB in size");
                break;
            case "More than 130 characters error":
                Assert.assertEquals(uploadFilePage.getErrorMessage(), fileName + fileFormat + " must be 130 characters or less");
                break;
        }
    }

    @Then("^I will be displayed (.*) summary for the files (.*)$")
    public void iWillBeDisplayedErrorSummaryForFiles(String error, String files) {
        switch (error){
            case "Invalid file format error":
                Assert.assertTrue(uploadFilePage.isErrorSummaryCorrect(files, " must be a jpg, bmp, png or pdf"));
               break;
            case "Invalid file name error":
                Assert.assertTrue(uploadFilePage.isErrorSummaryCorrect(files, " must use only letters, numbers, spaces, hyphens and underscore"));
                break;
            case "Max file size error":
                Assert.assertTrue(uploadFilePage.isErrorSummaryCorrect(files, " must be less than 2MB in size"));
                break;
            case "More than 130 characters error":
                Assert.assertTrue(uploadFilePage.isErrorSummaryCorrect(files, " must be 130 characters or less"));
                break;
        }
    }

    @And("^I will be displayed (.*) message for the files (.*)$")
    public void iWillSeeUploadErrorMessageForFiles(String error, String files) {
        switch (error){
            case "Invalid file format error":
                Assert.assertTrue(uploadFilePage.isMultiFileUploadFailureMessagesDisplayed(files, " must be a jpg, bmp, png or pdf"));
                break;
            case "Invalid file name error":
                Assert.assertTrue(uploadFilePage.isMultiFileUploadFailureMessagesDisplayed(files, " must use only letters, numbers, spaces, hyphens and underscore"));
                break;
            case "Max file size error":
                Assert.assertTrue(uploadFilePage.isMultiFileUploadFailureMessagesDisplayed(files, " must be less than 2MB in size"));
                break;
            case "More than 130 characters error":
                Assert.assertTrue(uploadFilePage.isMultiFileUploadFailureMessagesDisplayed(files, " must be 130 characters or less"));
                break;
        }
    }

    @Then("^I will be displayed (.*) for (.*)(.*) extra evidence$")
    public void iWillBeDisplayedErrorForFileNameFileFormatExtraEvidence(String error, String fileName, String fileFormat) {
        switch (error) {
            case "Invalid file format error":
                Assert.assertEquals(extraInformationPage.getErrorMessage(), fileName + fileFormat + " must be a jpg, bmp, png or pdf");
                break;
            case "Invalid file name error":
                Assert.assertEquals(extraInformationPage.getErrorMessage(), fileName + fileFormat + " must use only letters, numbers, spaces, hyphens and underscore");
                break;
            case "Max file size error":
                Assert.assertEquals(extraInformationPage.getErrorMessage(), fileName + fileFormat + " must be less than 2MB in size");
                break;
            case "More than 130 characters error":
                Assert.assertEquals(extraInformationPage.getErrorMessage(), fileName + fileFormat + " must be 130 characters or less");
                break;
            case"Invalid extra information and File not uploaded error":
                Assert.assertEquals(extraInformationPage.getErrorMessage(), "Enter extra information using only letters, numbers, spaces, hyphens, apostrophes, commas, full stops and pound signs");
                Assert.assertEquals(extraInformationPage.getInvalidFileErrorMessage(), fileName + fileFormat + " was not uploaded. Try again");
                break;
            case "Invalid extra information and Invalid file error":
                Assert.assertEquals(extraInformationPage.getErrorMessage(), "Enter extra information using only letters, numbers, spaces, hyphens, apostrophes, commas, full stops and pound signs");
                Assert.assertEquals(extraInformationPage.getInvalidFileErrorMessage(), fileName + fileFormat + " must use only letters, numbers, spaces, hyphens and underscore");
                break;
        }
    }
}